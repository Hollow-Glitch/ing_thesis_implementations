function scen__multi_P_immobile_E()
    base_fig_name = 'newGen__multiPimmobileE_';

    %% scenario setup
    t = 0;
    n_P = 6;
    r_d = 200;

    [n_P, r_d,             ...
    alpha, k, beta,        ...
    x_EI, y_EI, p_EI, v_E, ...
    v_P, p_PI]             ...
        = multi_P_fast_E__config_setup(n_P, r_d);

    %% render: setup
    hold on;
    axis equal;
    xlabel('x');
    ylabel('y');

    %% render: positions
    col_circle(x_EI, y_EI, r_d, '--k');
    plot_with_vectors(p_PI, 'r*');
    plot_with_vectors(p_PI, 'rd');
    plot_with_vectors(p_EI, 'b*');

    %% render: DB-circles
    for i = 1 : n_P
        [center, radius] = DB_circle_center_and_radius(...
            p_PI(1,i), p_PI(2,i), p_EI(1), p_EI(2), k);
        col_circle(center(1), center(2), radius, 'r');
    end

    %%  Limits for x,y which will be used for all figures so that the reducing
    %   dominance region of the evader can be seen.
    my_xlim = 1.05 .* xlim;
    my_ylim = 1.05 .* ylim;
    xlim(my_xlim);
    ylim(my_ylim);

    %% print variables
    disp('----------------------------------------');
    print_var(t);
    print_var(n_P);
    print_var(r_d);
    print_var(alpha);
    print_var(k);
    print_var(v_E);
    print_var(v_P);

    %% create figure
    fig_name = sprintf([base_fig_name, 't=%.2f'], t);
    set_fig_style_bc_squareish();
    % set_fig_style_min_squareish();
    print_current_fig(fig_name, "outputType","pdf");


    %% ============================================================
    %  === fig 2
    %  =========

    old__r_d = r_d;
    old__p_PI = p_PI;
    
    %% scenario setup
    new__r_d = 0.75 * r_d;
    t = (r_d - new__r_d) / v_P;
    r_d = new__r_d;

    [n_P, r_d,             ...
    alpha, k, beta,        ...
    x_EI, y_EI, p_EI, v_E, ...
    v_P, p_PI]             ...
        = multi_P_fast_E__config_setup(n_P, r_d);

    %% render: setup
    clf;
    hold on;
    axis equal;
    xlabel('x');
    ylabel('y');

    %% render: positions
    col_circle(x_EI, y_EI, old__r_d, '--k');
    plot_with_vectors(old__p_PI, 'r*');
    plot_with_vectors(p_PI, 'rd');
    plot_with_vectors(p_EI, 'b*');

    %% render: DB-circles
    for i = 1 : n_P
        [center, radius] = DB_circle_center_and_radius(...
            p_PI(1,i), p_PI(2,i), p_EI(1), p_EI(2), k);
        col_circle(center(1), center(2), radius, 'r');
    end

    xlim(my_xlim);
    ylim(my_ylim);

    %% print variables
    disp('----------------------------------------');
    print_var(t);
    print_var(n_P);
    print_var(r_d);
    print_var(alpha);
    print_var(k);
    print_var(v_E);
    print_var(v_P);
    
    %% create figure
    fig_name = sprintf([base_fig_name, 't=%.2f'], t);
    set_fig_style_bc_squareish();
    % set_fig_style_min_squareish();
    print_current_fig(fig_name, "outputType","pdf");


    %% ============================================================
    %  === fig 3
    %  =========

    old__r_d = r_d;
    old__p_PI = p_PI;
    
    %% scenario setup
    new__r_d = 0.5 * r_d;
    t = (r_d - new__r_d) / v_P;
    r_d = new__r_d;

    [n_P, r_d,             ...
    alpha, k, beta,        ...
    x_EI, y_EI, p_EI, v_E, ...
    v_P, p_PI]             ...
        = multi_P_fast_E__config_setup(n_P, r_d);

    %% render: setup
    clf;
    hold on;
    axis equal;
    xlabel('x');
    ylabel('y');

    %% render: positions
    col_circle(x_EI, y_EI, old__r_d, '--k');
    plot_with_vectors(old__p_PI, 'r*');
    plot_with_vectors(p_PI, 'rd');
    plot_with_vectors(p_EI, 'b*');

    %% render: DB-circles
    for i = 1 : n_P
        [center, radius] = DB_circle_center_and_radius(...
            p_PI(1,i), p_PI(2,i), p_EI(1), p_EI(2), k);
        col_circle(center(1), center(2), radius, 'r');
    end

    xlim(my_xlim);
    ylim(my_ylim);

    %% print variables
    disp('----------------------------------------');
    print_var(t);
    print_var(n_P);
    print_var(r_d);
    print_var(alpha);
    print_var(k);
    print_var(v_E);
    print_var(v_P);
    
    %% create figure
    fig_name = sprintf([base_fig_name, 't=%.2f'], t);
    set_fig_style_bc_squareish();
    % set_fig_style_min_squareish();
    print_current_fig(fig_name, "outputType","pdf");
end


