function scen__multi_P_fast_E()

    %% options
    create_figure = true;
    create_video = false;
    % figures and video at the same time can't be created
    
    base_fig_name = 'newGen__multiPfastE_';
    video_name = base_fig_name;

    direction_change_time = 5;
    predetermined_end_time = 15.1;

    %% Video: create video object
    if create_video
        vidObj = VideoWriter([video_name, '.avi']);
        open(vidObj);
    end
    
    %% scenario setup
    t = 0;
    n_P = 6;
    r_d = 200;

    [n_P, r_d,             ...
    alpha, k, beta,        ...
    x_EI, y_EI, p_EI, v_E, ...
    v_P, p_PI]             ...
        = multi_P_fast_E__config_setup(n_P, r_d);

    %% render: setup
    hold on;
    axis equal;
    xlabel('x');
    ylabel('y');

    %% render: positions
    col_circle(x_EI, y_EI, r_d, '--k');
    plot_with_vectors(p_PI, 'r*');
    plot_with_vectors(p_PI, 'rd');
    plot_with_vectors(p_EI, 'b*');

    %% render: DB-circles
    p_C = [];
    r_C = 0;
    for i = 1 : n_P
        [center, radius] = DB_circle_center_and_radius(...
            p_PI(1,i), p_PI(2,i), p_EI(1), p_EI(2), k);
        p_C = [p_C, center];
        r_C = radius;
        col_circle(center(1), center(2), radius, 'r');
    end

    %%  Limits for x,y which will be used for all figures so that the reducing
    %   dominance region of the evader can be seen.
    my_xlim = 1.05 .* xlim;
    my_ylim = 1.05 .* ylim;
    xlim(my_xlim);
    ylim(my_ylim);

    %% tangent points
    [T1, T2] = DB_circle_tangent_points(p_PI(1,1), p_PI(2,1), p_EI(1), p_EI(2), k);
%     plot_with_vectors([T1, T2], 'k*');
    plot_with_vectors(T2, 'k*');
    p_T = T1; % chosen tangent point

    
    %% calculating potential capture point
    points = circleLineIntersections(p_C(:, 2), r_C, p_EI, p_PI(:, 2));
    plot_with_vectors(points(:, 1), 'kx');

    %% calculating heading vectors: evader
    uvec__v_E = (p_T - p_EI) / norm(p_T - p_EI); % evader heading vector
    direction_already_changed = false;

    %% calculating heading vectors: pursuers
    %  strat1
%     uvec__v_P = [];
    uvec__v_P1 = uvec(p_T - p_PI(:, 1));
    uvec__v_P2 = uvec(p_T - p_PI(:, 2));
    uvec__v_P3 = uvec(p_T - p_PI(:, 3));
    uvec__v_P4 = uvec(p_T - p_PI(:, 4));
    uvec__v_P5 = uvec(p_T - p_PI(:, 5));
    uvec__v_P6 = uvec(p_T - p_PI(:, 6));
    uvec__v_P = [
        uvec__v_P1, ...
        uvec__v_P2, ...
        uvec__v_P3, ...
        uvec__v_P4, ...
        uvec__v_P5, ...
        uvec__v_P6
    ];

    %% path collections
    col__p_P = [];
    col__p_E = [];

    %% game loop
    p_P = p_PI;
    p_E = p_EI;
    time_step = 1 / 60;
    t = 0;
    isPlaying = true;
    while isPlaying
        loop_start_time = tic;
        clf;
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");
        xlim(my_xlim);
        ylim(my_ylim);

        t = t + time_step;

        % update evader
        if direction_change_time < t && ~direction_already_changed
            direction_already_changed = true;
            uvec__v_E = (p_PI(:,1) - p_E) / norm(p_PI(:,1) - p_E); % evader heading vector
        end
        p_E = p_E + v_E * time_step * uvec__v_E;

        % update pursuers
        if direction_change_time < t
            for i = 1 : n_P
                uvec__v_Pi = uvec(p_E - p_P(:, i));
                p_P(:, i) = p_P(:, i) + uvec__v_Pi * v_P * time_step;
            end
        else
            for i = 1 : n_P
                p_P(:, i) = p_P(:, i) + uvec__v_P(:, i) * v_P * time_step;
            end
        end

        % add positions to path collections
        col__p_E = [col__p_E, p_E];
        col__p_P = [col__p_P, [p_P(:, 1); p_P(:, 2); p_P(:, 3); p_P(:, 4); p_P(:, 5); p_P(:, 6)]];
        col__p_P

        % render: agent paths
        plot_with_vectors(col__p_E, 'b-');
        for i = 1 : n_P
            plot_with_vectors(col__p_P(i+(i-1):(i+1)+(i-1), :), 'r-');
        end

%         % render: pursuer line paths
%         line_p1p2l_draw(p_PI(:, 1), p_PI(:, 1) + uvec__v_P1, 100, 'k--');
%         line_p1p2l_draw(p_PI(:, 2), p_PI(:, 2) + uvec__v_P2, 100, 'k--');

        % render: positions
        col_circle(x_EI, y_EI, r_d, '--k');
        plot_with_vectors(p_PI, 'r*');
        plot_with_vectors(p_P, 'rd');
        plot_with_vectors(p_EI, 'b*');
        plot_with_vectors(p_E, 'bs');

        if t < direction_change_time
            plot_with_vectors(p_T, '*k');
        end

        % render: DB-circles
        p_C = [];
        r_C = 0;
        for i = 1 : n_P
            [center, radius] = DB_circle_center_and_radius(...
                p_P(1,i), p_P(2,i), p_E(1), p_E(2), k);
            p_C = [p_C, center];
            r_C = radius;
            col_circle(center(1), center(2), radius, 'k');
        end

        % !!! DRAW !!!
        elapsed_time = toc(loop_start_time);
        print_var(t);
        print_var(elapsed_time);
%         pause(1/60);
        drawnow;

        % create media
        if create_figure
            fig_time_1 = direction_change_time;
            fig_time_2 = predetermined_end_time-0.1;
            fig_epsilon = 0.01;
            if float_eq(t, fig_time_1, fig_epsilon) ...
            || float_eq(t, fig_time_2, fig_epsilon)
                disp("should create figure");

                fig_name = sprintf([base_fig_name, 't=%.2f'], t);
                set_fig_style_bc_squareish();
                % set_fig_style_min_squareish();
                print_current_fig(fig_name, "outputType","pdf");
            end
        end
        if create_video
            writeVideo(vidObj, getframe(gcf));
        end

        % termination condition
        for i = 1 : n_P
            if norm(p_P(:, i) - p_E) < 1
                disp('Collision detected!');
                isPlaying = false;
            end
        end

        % manual interupt
        keyboard_inputs = get(gcf, 'CurrentCharacter');
        if ~isempty(keyboard_inputs) && ismember('q', keyboard_inputs)
            keyboard_inputs
            set(gcf, 'CurrentCharacter', '@');
            disp("You pressed 'q', closing the program.");
            isPlaying = false;
        end

        % predetermined interupt
        if predetermined_end_time < t
            disp('Predetermined interupt!');
            isPlaying = false;
        end
    end

    %% Video: close video object
    if create_video
        close(vidObj);
    end

    if create_figure
        clf
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");
        xlim(my_xlim);
        ylim(my_ylim);

        plot_with_vectors(p_PI, 'r*');
        plot_with_vectors(p_P, 'rd');
        plot_with_vectors(p_EI, 'b*');
        plot_with_vectors(p_E, 'bs');

        plot_with_vectors(col__p_E, 'b-');
        for i = 1 : n_P
            plot_with_vectors(col__p_P(i+(i-1):(i+1)+(i-1), :), 'r-');
        end

        fig_name = sprintf([base_fig_name, 'without_DB_t=%.2f'], t);
        set_fig_style_bc_squareish();
        % set_fig_style_min_squareish();
        print_current_fig(fig_name, "outputType","pdf");
    end
end

function out = float_eq(a, b, epsilon)
    if abs(a - b) < epsilon
        out = true;
    else
        out = false;
    end
end

function uvec = uvec(vec)
    % uvec = uvec(vec)
    %
    % Calculates the unit vector of `vec`.
    uvec = vec / norm(vec);
end




