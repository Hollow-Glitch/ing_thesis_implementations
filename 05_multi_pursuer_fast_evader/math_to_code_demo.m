function math_to_code_demo()
    % math_to_code_demo()
    %
    % This function is demonstrating that the implementation for the
    % configuration setup is trivial based on the mathematical derivations.

    % scenario setup
    n_P = 6;
    r_d = 200;

    % calculate scenario quantities
    alpha = pi / n_P;  % angle DB-circle center and tangent point
    k = sin(alpha);    % velocity prop. const.
    beta  = 2 * alpha; % angle between two neighboring DB-circle centers

    % setup: evader
    x_EI = 0;
    y_EI = 0;
    p_EI = [x_EI; y_EI];
    v_E = 25;

    % setup pursuers
    v_P = k*v_E;
    p_PI = [];
    for pursuer_index = 1 : n_P
        angle = beta * pursuer_index;
        x_Pi = r_d * cos(angle);
        y_Pi = r_d * sin(angle);
        p_Pi = p_EI + [x_Pi; y_Pi];
        p_PI = [p_PI, p_Pi];
    end

    % print variables
    print_var(n_P);
    print_var(r_d);
    print_var(alpha);
    print_var(k);
    print_var(v_E);
    print_var(v_P);

    %% rendering
    clf;
    hold on;
    axis equal;
    
    % render: positions
    plot_with_vectors(p_PI, 'r*');
    plot_with_vectors(p_EI, 'b*');
    col_circle(x_EI, y_EI, r_d, '--k');

    % render: DB-circles
    for i = 1 : n_P
        [center, radius] = DB_circle_center_and_radius(...
            p_PI(1,i), p_PI(2,i), p_EI(1), p_EI(2), k);
        col_circle(center(1), center(2), radius, 'r');
    end
end

