function scen__multi_not_smart_P_fast_E()
    % scen__multi_not_smart_P_fast_E()
    %
    % It is set up in such a way that the evader goes straight
    % to the second pursuer, and surprisingly this creates a gap.

    %% options
    create_figure = false;
    create_video  = true;
    % figures and video at the same time can't be created

    base_fig_name = 'newGen__multiPnotSmartE_';
    video_name = base_fig_name;

    %% Video: create video object
    if create_video
        vidObj = VideoWriter([video_name, '.avi']);
        open(vidObj);
    end

    %% scenario setup
    t = 0;
    n_P = 6;
    r_d = 200;

    [n_P, r_d,             ...
    alpha, k, beta,        ...
    x_EI, y_EI, p_EI, v_E, ...
    v_P, p_PI]             ...
        = multi_P_fast_E__config_setup(n_P, r_d);

    %% render: setup
    hold on;
    axis equal;
    xlabel('x');
    ylabel('y');

    %% render: positions
    col_circle(x_EI, y_EI, r_d, '--k');
    plot_with_vectors(p_PI, 'r*');
    plot_with_vectors(p_PI, 'rd');
    plot_with_vectors(p_EI, 'b*');

    %% render: DB-circles
    p_C = [];
    r_C = 0;
    for i = 1 : n_P
        [center, radius] = DB_circle_center_and_radius(...
            p_PI(1,i), p_PI(2,i), p_EI(1), p_EI(2), k);
        p_C = [p_C, center];
        r_C = radius;
        col_circle(center(1), center(2), radius, 'r');
    end

    %%  Limits for x,y which will be used for all figures so that the reducing
    %   dominance region of the evader can be seen.
    my_xlim = 1.05 .* xlim;
    my_ylim = 1.05 .* ylim;
    xlim(my_xlim);
    ylim(my_ylim);
    
    %% calculating potential capture point
    points = circleLineIntersections(p_C(:, 2), r_C, p_EI, p_PI(:, 2));
    plot_with_vectors(points(:, 1), 'kx');

    %% calculating heading vectors: evader
    uvec__v_E = (p_PI(:, 2) - p_EI) / norm(p_PI(:, 2) - p_EI); % evader heading vector

    %% calculating heading vectors: evader
    %  strat1
    uvec__v_P = [];
    for i = 1:n_P
        uvec__v_Pi = (p_EI - p_PI(:, i)) / norm(p_PI(:, 2) - p_EI);
        uvec__v_P = [uvec__v_P, uvec__v_Pi];
    end
    

    %% game loop
    p_P = p_PI;
    p_E = p_EI;
    time_step = 1 / 60;
    t = 0;
    isPlaying = true;
    while isPlaying
        loop_start_time = tic;
        clf;
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");
        xlim(my_xlim);
        ylim(my_ylim);

        % update evader
        t = t + time_step;
        p_E = p_E + v_E * time_step * uvec__v_E;

        % update pursuers
        for i = 1 : n_P
            p_P(:, i) = p_P(:, i) + uvec__v_P(:, i) * v_P * time_step;
        end

        % render: positions
        col_circle(x_EI, y_EI, r_d, '--k');
        plot_with_vectors(p_PI, 'r*');
        plot_with_vectors(p_P, 'rd');
        plot_with_vectors(p_EI, 'b*');
        plot_with_vectors(p_E, 'bs');

        % render: DB-circles
        p_C = [];
        r_C = 0;
        for i = 1 : n_P
            [center, radius] = DB_circle_center_and_radius(...
                p_P(1,i), p_P(2,i), p_E(1), p_E(2), k);
            p_C = [p_C, center];
            r_C = radius;
            col_circle(center(1), center(2), radius, 'r');
        end

        % termination condition
        for i = 1 : n_P
            if norm(p_P(:, i) - p_E) < 0.1
                disp('Collision detected!');
                isPlaying = false;
            end
        end

        % manual interupt
        keyboard_inputs = get(gcf, 'CurrentCharacter');
        if ~isempty(keyboard_inputs) && ismember('q', keyboard_inputs)
            keyboard_inputs
            set(gcf, 'CurrentCharacter', '@');
            disp("You pressed 'q', closing the program.");
            isPlaying = false;
        end

        % create media
        if create_figure
            fig_time_1 = 2;
            fig_time_2 = 4;
            fig_epsilon = 0.01;
%             if t == fig_time_1 || t == fig_time_2
            if float_eq(t, fig_time_1, fig_epsilon) ...
            || float_eq(t, fig_time_2, fig_epsilon)
                disp("should create figure");

                fig_name = sprintf([base_fig_name, 't=%.2f'], t);
                set_fig_style_bc_squareish();
                % set_fig_style_min_squareish();
                print_current_fig(fig_name, "outputType","pdf");
            end
        end
        if create_video
            writeVideo(vidObj, getframe(gcf));
        end

        elapsed_time = toc(loop_start_time);
        print_var(t);
        print_var(elapsed_time);
%         pause(1/60);
        drawnow;
    end

    if create_video
        close(vidObj);
    end
end

function out = float_eq(a, b, epsilon)
    if abs(a - b) < epsilon
        out = true;
    else
        out = false;
    end
end