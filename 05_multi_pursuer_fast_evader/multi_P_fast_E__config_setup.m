function [n_P, r_d,              ...
          alpha, k, beta,        ...
          x_EI, y_EI, p_EI, v_E, ...
          v_P, p_PI]             ...
          = multi_P_fast_E__config_setup(n_P, r_d)
    % scenario setup
%     n_P = 6;
%     r_d = 200;

    % calculate scenario quantities
    alpha = pi / n_P;  % angle DB-circle center and tangent point
    k = sin(alpha);    % velocity prop. const.
    beta  = 2 * alpha; % angle between two neighboring DB-circle centers

    % setup: evader
    x_EI = 0;
    y_EI = 0;
    p_EI = [x_EI; y_EI];
    v_E = 25;

    % setup pursuers
    v_P = k*v_E;
    p_PI = [];
    for pursuer_index = 1 : n_P
        angle = beta * pursuer_index;
        x_Pi = r_d * cos(angle);
        y_Pi = r_d * sin(angle);
        p_Pi = p_EI + [x_Pi; y_Pi];
        p_PI = [p_PI, p_Pi];
    end
end