

%% add to path
proj = currentProject();
utils = genpath([proj.RootFolder.char, filesep, 'utilities']);
rmpath(utils);

disp(' ');
disp('If there are warnings above, you can ignore them.');
disp(' ');
disp('The following directories have been removed from to the path:');
dir_path_cell_arr = split(utils, ';');
for dir_path_index = 1:length(dir_path_cell_arr)
    cur_path = dir_path_cell_arr{dir_path_index};
    disp(cur_path);
end
disp(' ');
% disp('These will be automatically re-added the next time when you open the project.');
disp('The next time you open this project, the utilities directory');
disp('will be automatically added.');
