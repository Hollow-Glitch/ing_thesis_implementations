format compact;
% set(groot,'DefaultAxesTickLabelInterpreter','Latex');


%% add to path
proj = currentProject();
utils = genpath([proj.RootFolder.char, filesep, 'utilities']);
addpath('utilities');

disp(' ');
disp('The following directories have been added to the path:');
dir_path_cell_arr = split(utils, ';');
for dir_path_index = 1:length(dir_path_cell_arr)
    cur_path = dir_path_cell_arr{dir_path_index};
    disp(cur_path);
end
disp(' ');
disp('These will be automatically removed from the path when the project is closed.');
