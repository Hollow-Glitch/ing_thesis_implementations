function GPP_various_paths()

    %% Options
    base_name = 'general_PP_broken_evader_path';
    create_figure  = false;
    create_video   = false;


    %% scenario 1: pentagon
    angles = 0 : 2*pi/5 : 2*pi;
    x_p = 10*cos(angles);
    y_p = 10*sin(angles);
    points = [x_p;y_p];

    scen_name      = [base_name, '_pentagon'];
    fig_name       = scen_name;
    video_name     = scen_name;
    agent_radius   = 0.5;
    v_E            = 25;
    v_P            = 0.8*v_E;
    p_PI           = [0;0];
    original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video);

    
    %% scenario 2: circle
    angles = 0 : 2*pi/100 : 2*pi;
    x_p = 10*cos(angles);
    y_p = 10*sin(angles);
    points = [x_p;y_p];

    scen_name      = [base_name, '_circle'];
    fig_name       = scen_name;
    video_name     = scen_name;
    agent_radius   = 0.5;
    v_E            = 25;
    v_P            = 0.8*v_E;
    p_PI           = [0;0];
    original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video);


    %% scenario 3: figure eight
    % https://math.stackexchange.com/questions/7988/the-function-that-draws-a-figure-eight
    % start_angle = -pi/2;
    % end_angle = 3*pi/2;
    start_angle = 0;
    end_angle = 2*pi;
    angle_step = (end_angle - start_angle)/100;
    angles = start_angle : angle_step : end_angle;
    points = [10*cos(angles); 20*sin(angles).*cos(angles)];

    scen_name      = [base_name, '_figure_eight'];
    fig_name       = scen_name;
    video_name     = scen_name;
    agent_radius   = 0.5;
    v_E            = 25;
    v_P            = 0.8*v_E;
    p_PI           = [0;0];
    original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video);


    %% scenario 4: parabola
    x = 10:-20/10:-10;
    y = -x.^2 / 10 + 10;
    points = [x;y];

    scen_name      = [base_name, '_parabola'];
    fig_name       = scen_name;
    video_name     = scen_name;
    agent_radius   = 0.5;
    v_E            = 25;
    v_P            = 0.8*v_E;
    p_PI           = [0;0];
    original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video);

    %% scenario 5: pentagon
    angles = 0 : 2*pi/5 : 2*pi;
    x_p = 10*cos(angles);
    y_p = 10*sin(angles);
    points = [x_p;y_p];

    scen_name      = [base_name, '_pentagon2'];
    fig_name       = scen_name;
    video_name     = scen_name;
    agent_radius   = 0.5;
    v_E            = 25;
    v_P            = 1.05*v_E;
    p_PI           = [0;0];
    original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video);

    
    %% scenario 6: circle
    angles = 0 : 2*pi/100 : 2*pi;
    x_p = 10*cos(angles);
    y_p = 10*sin(angles);
    points = [x_p;y_p];

    scen_name      = [base_name, '_circle2'];
    fig_name       = scen_name;
    video_name     = scen_name;
    agent_radius   = 0.5;
    v_E            = 25;
    v_P            = 1.05*v_E;
    p_PI           = [0;0];
    original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video);


    %% scenario 7: figure eight
    % https://math.stackexchange.com/questions/7988/the-function-that-draws-a-figure-eight
    % start_angle = -pi/2;
    % end_angle = 3*pi/2;
    start_angle = 0;
    end_angle = 2*pi;
    angle_step = (end_angle - start_angle)/100;
    angles = start_angle : angle_step : end_angle;
    points = [10*cos(angles); 20*sin(angles).*cos(angles)];

    scen_name      = [base_name, '_figure_eight2'];
    fig_name       = scen_name;
    video_name     = scen_name;
    agent_radius   = 0.5;
    v_E            = 25;
    v_P            = 1.05*v_E;
    p_PI           = [0;0];
    original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video);


    %% scenario 8: parabola
    x = 10:-20/10:-10;
    y = -x.^2 / 10 + 10;
    points = [x;y];

    scen_name      = [base_name, '_parabola2'];
    fig_name       = scen_name;
    video_name     = scen_name;
    agent_radius   = 0.5;
    v_E            = 25;
    v_P            = 1.05*v_E;
    p_PI           = [0;0];
    original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video);
    
end

% function original_code(agent_radius, v_E, p_EI, points, v_P, p_PI, fig_name, create_figure, video_name, create_video)
function original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video)
    arguments
        agent_radius  (1,1)
        v_E           (1,1)
        % p_EI          (2,1)
        points        (2,:)
        v_P           (1,1)
        p_PI          (2,1)
        fig_name      
        create_figure 
        video_name    
        create_video  
    end
    % points ..

    % v_E ... evader's velocity
    % t_t ... travel time
    % t_T ... total travel time
    % 

    %% setup this program
%     create_figure = true;
%     create_video = true;
    
    %% Video: create video object
    if create_video
        vidObj = VideoWriter([video_name, '.avi']);
        open(vidObj);
    end

    %% general agent setup
%     agent_radius = 0.5;
    termination_distance = agent_radius;

    %% evader setup
%     v_E = 25;
%     p_EI = [10;20];  
    % p_EI will be the first position in the sequence of positions of the evader's path.

    %% pursuer setup
%     p_PI = [0;0];
    p_P = p_PI;
%     v_P = 25;
    

    %% precalculations for evader
    n = size(points, 2);
    t_t = [];  % sequence of travel times
    for i = 1:(n-1)
        p_a = points(:, i);           % p_a = p_i
        p_b = points(:, i+1);         % p_b = p_{i+1}
        t_ti = norm(p_b - p_a) / v_E; % i-th travel time

        t_t = [t_t, t_ti];            % update travel time seq.
    end

    t_T = sum(t_t);  % total travel time

    t_a = [];  % sequence of arrival times
    for j = 1:n
        t_ai = sum(t_t(1:j-1));  % calculate j-th arrival time
        t_a = [t_a, t_ai];       % update seq.
    end

    %% agent position collections for path rendering
    col__p_E = [];
    col__p_P = [];

    %% game loop
    i = 1;  % segment index
    step = 1/60;
    for t = 0 : step : t_T
        %% evader update
        if t_a(i+1) <= t
            i = i + 1;
        end
        t_ai = t_a(i);
        p_i = points(:, i);
        next__p_i = points(:, i+1);

        uvec = (next__p_i - p_i) / norm(next__p_i - p_i);

        p_E = p_i + (t - t_ai) * v_E * uvec;

        %% pursuer update
        uvecP = (p_E - p_P) / norm(p_E - p_P);
        p_P = p_P + step * v_P * uvecP;
        % the step here represents the time ammount by which we want the pursuer to change

        %% update agent position collections for path rendering
        col__p_E = [col__p_E, p_E];
        col__p_P = [col__p_P, p_P];

        %% render
        clf;
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf("$%.2f$sec", t), 'Interpreter','latex');
        plot_with_vectors(points, 'k.');
        plot_with_vectors(col__p_E, '--b');
        agent_repr(p_E, agent_radius, 'b');
        plot_with_vectors(col__p_P, '--r');
        agent_repr(p_P, agent_radius, 'r');
        xline(0);
        yline(0);

%         pause(1/60);
        pause(1/61)

        %% Video: write frame
        if create_video
            writeVideo(vidObj, getframe(gcf));
        end

        %% termination condition
        %  This must be here so that we see the state which led to
        %  termination if this would be above the render part then we would
        %  not see this state.
        if norm(p_E - p_P) < termination_distance
            break;
        end
    end

    %% Video: close video object
    if create_video
        close(vidObj);
    end

    %% figure creation
    if create_figure
        set_fig_style_bc_squareish();
        print_current_fig(fig_name);
    end
end

function point = polarPoint(theta, radius)
    [x,y] = pol2cart(theta, radius);
    point = [x;y];
end

function agent_repr(p, radius, color)
    % Used for rendering the agents on the playing field in different
    % colors.
    arguments
        p      (2,1)
        radius (1,1)
        color
    end
    x = p(1);
    y = p(2);
    r_half = radius / 2;
    x = x - r_half;
    y = y - r_half;
    pos = [x, y, radius, radius];
    rectangle('Position', pos, 'Curvature', 1, 'FaceColor', color);
end