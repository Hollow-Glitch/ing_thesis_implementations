function GPP_comparison()

    %% Options
    create_figure  = false;
    create_video   = false;

    %% without capture
    angles = 0 : 2*pi/100 : 2*pi;
    x_p = cos(angles);
    y_p = sin(angles);
    points = [x_p;y_p];

    scen_name      = 'comparison_of_1v1circle_with_GPPcircle';
    fig_name       = scen_name;
    video_name     = scen_name;
    agent_radius   = 0.05;
    v_E            = 1;
    v_P            = 0.8*v_E;
    p_PI           = [0;0];
    GPP = original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video);

    SPP = animate_and_create_fig(0.8);

    % GPP ... general pure pursuit
    % SPP ... solver pure pursuit

    GPP__p_E = GPP(1:2, :);
    GPP__p_P = GPP(3:4, :);

    SPP__p_E = SPP(1:2, :);
    SPP__p_P = SPP(3:4, :);

    clf;
    hold on;
    axis equal;
    xlabel('x');
    ylabel('y');
    xline(0);
    yline(0);
    plot(SPP__p_E(1,:), SPP__p_E(2,:), Color='#4DBEEE', LineStyle='-');
    plot(SPP__p_P(1,:), SPP__p_P(2,:), Color='#EDB120', LineStyle='-');

    plot(GPP__p_E(1,:), GPP__p_E(2,:), Color='#7E2F8E', LineStyle='--');
    plot(GPP__p_P(1,:), GPP__p_P(2,:), Color='#77AC30', LineStyle='--');
    
    agent_repr(SPP__p_E(:, end), agent_radius, 'b');
    agent_repr(SPP__p_P(:, end), agent_radius, 'r');

    xlim([-1.2, 1.2]);
    ylim([-1.2, 1.2]);

    % create: figure
    set_fig_style_bc_squareish();
    print_current_fig('comparison_of_1v1circle_with_GPPcircle__without_capture');

    %% with capture
    
    angles = 0 : 2*pi/100 : 2*pi;
    x_p = cos(angles);
    y_p = sin(angles);
    points = [x_p;y_p];

    scen_name      = 'comparison_of_1v1circle_with_GPPcircle';
    fig_name       = scen_name;
    video_name     = scen_name;
    agent_radius   = 0.05;
    v_E            = 1;
    v_P            = 1.2*v_E;
    p_PI           = [0;0];
    GPP = original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video);

    SPP = animate_and_create_fig(1.2);

    % GPP ... general pure pursuit
    % SPP ... solver pure pursuit

    GPP__p_E = GPP(1:2, :);
    GPP__p_P = GPP(3:4, :);

    SPP__p_E = SPP(1:2, :);
    SPP__p_P = SPP(3:4, :);

    clf;
    hold on;
    axis equal;
    xlabel('x');
    ylabel('y');
    xline(0);
    yline(0);
    plot(SPP__p_E(1,:), SPP__p_E(2,:), Color='#4DBEEE', LineStyle='-');
    plot(SPP__p_P(1,:), SPP__p_P(2,:), Color='#EDB120', LineStyle='-');

    plot(GPP__p_E(1,:), GPP__p_E(2,:), Color='#7E2F8E', LineStyle='--');
    plot(GPP__p_P(1,:), GPP__p_P(2,:), Color='#77AC30', LineStyle='--');

    agent_repr(SPP__p_E(:, end), agent_radius, 'b');
    agent_repr(SPP__p_P(:, end), agent_radius, 'r');

    xlim([-1.2, 1.2]);
    ylim([-1.2, 1.2]);

    % create: figure
    set_fig_style_bc_squareish();
    print_current_fig('comparison_of_1v1circle_with_GPPcircle__with_capture');
end


function agent_position_collections = animate_and_create_fig(velocityPropConst)
    % ATTENTION !!!
    % - don't change the implementation of EP_sys without also
    %   correctly updating the expressions for pTx and pTy bellow
    %   they are interdependent
    % - parametrization has not yet been done
    
    IC = [0, 0];
%     velocityPropConst = 0.8;
    EP_sys_closed = @(t, sys_vec_in) EP_sys( t, sys_vec_in, velocityPropConst );

    % solve sys
    options = odeset('RelTol',1e-5,'AbsTol',1e-5);
    [t, sys_vec_out] = ode45(EP_sys_closed, [0, 6.27], IC, options);

    % unpack Attacker sys
    pAx = sys_vec_out(:, 1);
    pAy = sys_vec_out(:, 2);
    % calculate Target
    pTx = cos(t);
    pTy = sin(t);

    %% Animation
    
    % radia used to draw and check for collision
    common_radius = 0.05;
    evader_radius  = common_radius;
    pursuer_radius = common_radius;
    
    % set axes limits to content
    every_x = cat(1, pTx, pAx);
    every_y = cat(1, pTy, pAy);
    [my_xlim, my_ylim] = utils_v2.auto_limits_for_2D(every_x, every_y, 0.1, 0.1);
    xlim(my_xlim);
    ylim(my_ylim);
    hold on;
    
    % % video
    % vidObj = VideoWriter('peaks.avi');
    % open(vidObj);

    %% agent position collections for path rendering
    col__p_E = [];
    col__p_P = [];

    %% game loop
    for i = 1:length(t)
        clf;
        hold on;
        axis equal;
        xlim(my_xlim);
        ylim(my_ylim);
        xline(0);
        yline(0);
        xlabel('x');
        ylabel('y');

        % Target
        plot(pTx(1),   pTy(1),   'bo');  % start position indicator
        plot(pTx(1:i), pTy(1:i), 'b');   % path
        circle(pTx(i), pTy(i), evader_radius, 'b');  % current position

        % Attacker
        plot(pAx(1),   pAy(1),   'ro');  % start position indicator
        plot(pAx(1:i), pAy(1:i), 'r');   % path
        circle(pAx(i), pAy(i), pursuer_radius, 'r'); % current position
        
        % % video
        % writeVideo(vidObj, getframe(gcf));

        %% update agent position collections for path rendering
        p_E = [pTx(i); pTy(i)];
        col__p_E = [col__p_E, p_E];
        p_P = [pAx(i); pAy(i)];
        col__p_P = [col__p_P, p_P];

        %% termination condition
        pA = [ pAx(i) ; pAy(i) ];
        pT = [ pTx(i) ; pTy(i) ];
        if norm(pT - pA) <= 0.05
            table(pA, pT)
            disp("m_p - p_p = ");
            disp(pT - pA);
            norm(pT - pA)
            disp("Booom");
            break
        end

        pause(0.01);
    end

%     %% figure creation
%     if create_figure
%         fig_name = sprintf('1v1_circ_path_k=%f', velocityPropConst);
%         set_fig_style_bc_squareish();
%         print_current_fig(fig_name);
%     end

    agent_position_collections = [col__p_E; col__p_P];
end


function circle(x, y, radius, color)
    r_half = radius / 2;
    x = x - r_half;
    y = y - r_half;
    pos = [x y radius radius];
    rectangle('Position', pos, 'Curvature', 1, 'FaceColor', color);
end

function sys_vec_out = EP_sys( ...
        t,                 ...
        sys_vec_in,        ...
        velocityPropConst  ...   % proportionality constant of the velocities
    )
    % % unpack
    in_pAx = sys_vec_in(1);
    in_pAy = sys_vec_in(2);
    
    % % ------------------------------------------------------------
    
    scalar_part =           ...
        velocityPropConst   ...
        /                   ... 
        sqrt( (cos(t) - in_pAx)^2 + (sin(t) - in_pAy)^2 );
    vAx = scalar_part * (cos(t) - in_pAx);
    vAy = scalar_part * (sin(t) - in_pAy);
    
    % % ------------------------------------------------------------
    % % pack
    sys_vec_out = [
            vAx;   ...
            vAy    ...
        ];
end

%%
%%
%%

% function original_code(agent_radius, v_E, p_EI, points, v_P, p_PI, fig_name, create_figure, video_name, create_video)
function agent_position_collections = original_code(agent_radius, v_E, points, v_P, p_PI, fig_name, create_figure, video_name, create_video)
    arguments
        agent_radius  (1,1)
        v_E           (1,1)
        % p_EI          (2,1)
        points        (2,:)
        v_P           (1,1)
        p_PI          (2,1)
        fig_name      
        create_figure 
        video_name    
        create_video  
    end
    % points ..

    % v_E ... evader's velocity
    % t_t ... travel time
    % t_T ... total travel time
    % 

    %% setup this program
%     create_figure = true;
%     create_video = true;
    
    %% Video: create video object
    if create_video
        vidObj = VideoWriter([video_name, '.avi']);
        open(vidObj);
    end

    %% general agent setup
%     agent_radius = 0.5;
    termination_distance = agent_radius;

    %% evader setup
%     v_E = 25;
%     p_EI = [10;20];  
    % p_EI will be the first position in the sequence of positions of the evader's path.

    %% pursuer setup
%     p_PI = [0;0];
    p_P = p_PI;
%     v_P = 25;
    

    %% precalculations for evader
%     p1 = p_EI;
%     p2 = [10;10];
%     p3 = [20;10];
%     p4 = [25;20];
%     p5 = [30;10];
%     points = [p1, p2, p3, p4, p5];
    n = size(points, 2);
    t_t = [];  % sequence of travel times
    for i = 1:(n-1)
        p_a = points(:, i);           % p_a = p_i
        p_b = points(:, i+1);         % p_b = p_{i+1}
        t_ti = norm(p_b - p_a) / v_E; % i-th travel time

        t_t = [t_t, t_ti];            % update travel time seq.
    end

    t_T = sum(t_t);  % total travel time

    t_a = [];  % sequence of arrival times
    for j = 1:n
        t_ai = sum(t_t(1:j-1));  % calculate j-th arrival time
        t_a = [t_a, t_ai];       % update seq.
    end

    %% agent position collections for path rendering
    col__p_E = [];
    col__p_P = [];

    %% game loop
    i = 1;  % segment index
    step = 1/60;
    for t = 0 : step : t_T
        %% evader update
        if t_a(i+1) <= t
            i = i + 1;
        end
        t_ai = t_a(i);
        p_i = points(:, i);
        next__p_i = points(:, i+1);

        uvec = (next__p_i - p_i) / norm(next__p_i - p_i);

        p_E = p_i + (t - t_ai) * v_E * uvec;

        %% pursuer update
        uvecP = (p_E - p_P) / norm(p_E - p_P);
        p_P = p_P + step * v_P * uvecP;
        % the step here represents the time ammount by which we want the pursuer to change

        %% update agent position collections for path rendering
        col__p_E = [col__p_E, p_E];
        col__p_P = [col__p_P, p_P];

        %% render
        clf;
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf("$%.2f$sec", t), 'Interpreter','latex');
        plot_with_vectors(points, 'k.');
%         plot_with_vectors(p_E, 'bs');
%         plot_with_vectors(p_P, 'rd');
        plot_with_vectors(col__p_E, '--b');
        agent_repr(p_E, agent_radius, 'b');
        plot_with_vectors(col__p_P, '--r');
        agent_repr(p_P, agent_radius, 'r');
%         textArroundPoint(p1, polarPoint(pi/2, 1), '$p_1$');
%         textArroundPoint(p2, polarPoint(pi/2+pi/4, 1), '$p_2$');
%         textArroundPoint(p3, polarPoint(-pi/2, 1), '$p_3$');
%         textArroundPoint(p4, polarPoint(pi/2, 1), '$p_4$');
%         textArroundPoint(p5, polarPoint(-pi/2-pi/4, 1), '$p_5$');
        xline(0);
        yline(0);

        pause(1/61)

        %% Video: write frame
        if create_video
            writeVideo(vidObj, getframe(gcf));
        end

        %% termination condition
        %  This must be here so that we see the state which led to
        %  termination if this would be above the render part then we would
        %  not see this state.
        if norm(p_E - p_P) < termination_distance
            break;
        end
    end

    %% Video: close video object
    if create_video
        close(vidObj);
    end

    %% figure creation
    if create_figure
        set_fig_style_bc_squareish();
        print_current_fig(fig_name);
    end

    agent_position_collections = [col__p_E; col__p_P];
end

function point = polarPoint(theta, radius)
    [x,y] = pol2cart(theta, radius);
    point = [x;y];
end

function agent_repr(p, radius, color)
    % Used for rendering the agents on the playing field in different
    % colors.
    arguments
        p      (2,1)
        radius (1,1)
        color
    end
    x = p(1);
    y = p(2);
    r_half = radius / 2;
    x = x - r_half;
    y = y - r_half;
    pos = [x, y, radius, radius];
    rectangle('Position', pos, 'Curvature', 1, 'FaceColor', color);
end