function PP_arising_limit_cycle()

    %% Options
    velocityPropConst = 0.8;
    just_some = true;

    %%
    if just_some
        hold on;

        clf;
        xlim([-1.5, 1.5]);
        ylim([-1.5, 1.5]);
        generate_one_plot(velocityPropConst, 0.5);
        pause(1);

        clf;
        xlim([-1.5, 1.5]);
        ylim([-1.5, 1.5]);
        generate_one_plot(velocityPropConst, 1);
        pause(1);

        clf;
        xlim([-1.5, 1.5]);
        ylim([-1.5, 1.5]);
        generate_one_plot(velocityPropConst, 1.5);
    else
        % generate "all of them"
        for attackerDistrRadius = 0.1:0.1:3
            clf;
            generate_one_plot(velocityPropConst, attackerDistrRadius);
        end
    end
end


function generate_one_plot(velocityPropConst, attackerDistrRadius)
    gen_plot(velocityPropConst, attackerDistrRadius);

    %% create: figure
    name = sprintf('1v1_2D_trying_too_hard__radius=%.4f', attackerDistrRadius);
    set_fig_style_bc_squareish();
    print_current_fig(name);
end


function gen_plot(velocityPropConst, attackerDistrRadius)
    function out = EP_sys_closed(t, sys_vec_in)
        out = EP_sys( t, sys_vec_in, velocityPropConst );
    end
    % solve sys
    options = odeset('RelTol',1e-5,'AbsTol',1e-5);
    
    
    t_beg = 0;
    t_end = 15;
    % color = 'r'; % looks nice
    % color = ''; % is informative
    color = '--r';
    hold on;
    xline(0);
    yline(0);
    number_of_attackers = 10;
    step = 2*pi / number_of_attackers;
    for angle = 0:step:(2*pi)
        x_IC = attackerDistrRadius * cos(angle);
        y_IC = attackerDistrRadius * sin(angle);
        IC = [x_IC, y_IC];
        [t, sys_vec_out] = ode45( @EP_sys_closed, [t_beg, t_end], IC, options);

        % unpack Attacker sys
        pAx = sys_vec_out(:, 1);
        pAy = sys_vec_out(:, 2);

        plot(pAx(1),   pAy(1),   'ro');  % start position indicator
        % plot(pAx, pAy, color);   % path
        N=7; %every N points change color
        for a=1:N:numel(pAx)-N
            plot(pAx(a:a+N),pAy(a:a+N), 'Color',[1-(a/numel(pAx)),0,(a/numel(pAx))]) 
        end
    end

    t = t_beg:0.1:t_end;
    pTx = cos(t);
    pTy = sin(t);

    % Target
    plot(pTx(1),   pTy(1),   'bo');  % start position indicator
    plot(pTx, pTy, 'g', 'LineWidth', 2);   % path

    xlabel('x');
    ylabel('y');
%     xlim([-3, 3]);
%     ylim([-3, 3]);
    axis equal;
end


function sys_vec_out = EP_sys( ...
        t,                 ...
        sys_vec_in,        ...
        velocityPropConst  ...   % proportionality constant of the velocities
    )
    % % unpack
    in_pAx = sys_vec_in(1);
    in_pAy = sys_vec_in(2);
    
    % % ------------------------------------------------------------
    
    scalar_part =           ...
        velocityPropConst   ...
        /                   ... 
        sqrt( (cos(t) - in_pAx)^2 + (sin(t) - in_pAy)^2 );
    vAx = scalar_part * (cos(t) - in_pAx);
    vAy = scalar_part * (sin(t) - in_pAy);
    
    % % ------------------------------------------------------------
    % % pack
    sys_vec_out = [
            vAx;   ...
            vAy    ...
        ];
end