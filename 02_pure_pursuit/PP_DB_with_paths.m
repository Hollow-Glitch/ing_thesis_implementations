
function PP_DB_with_paths()

    %% Options
    animationsOn = true;
    heading_angle_count = 40;
    matFileName = "PP_DB_capture_points.mat";

    %% setup
    % xT = 0, yT = 0;
    IC = [-1, 0];  % => xA = -1, yA = 0;
    vA1_propConst = 1.2;
    EP_sys_closed = @(t, sys_vec_in, angle) EP_sys( t, sys_vec_in, vA1_propConst, angle );

    capture_points = [];

    %% core
    clf;
    hold on;
    axis equal;
    xline(0);
    yline(0);
    xlabel('x');
    ylabel('y');
    for angle = 0:(2*pi)/heading_angle_count:2*pi
        % solve sys
        options = odeset('RelTol',1e-5,'AbsTol',1e-5);
        [t, sys_vec_out] = ode45(@(t, IN) EP_sys_closed(t, IN, angle), [0 8], IC, options);

        % unpack Attacker sys
        pAx1 = sys_vec_out(:, 1);
        pAy1 = sys_vec_out(:, 2);

        % calculate Target
        [pTx, pTy] = pT_func(t, 1, angle, 0, 0);

        % radia used to draw and check for collision
        common_radius = 0.05;
        evader_radius  = common_radius;
        pursuer_radius = common_radius;

        %% find termination time
        term_time = 0;
        term_index = 0;
        for i = 1:length(t)
            pT  = [pTx(i);  pTy(i)];
            pA1 = [pAx1(i); pAy1(i)];
            if norm(pT - pA1) <= 0.05
                table(pA1, pT)
                disp("pT - pA1 = ");
                disp(pT - pA1);
                norm(pT - pA1)
                disp("A1-Booom");
                capture_points = [capture_points, pT];
                term_time = t(i);
                term_index = i;
                break
            end
        end
        i = term_index;
        plot(pTx(1),   pTy(1),   'bo');  % start position indicator
        plot(pTx(1:i), pTy(1:i), 'b');   % path
        circle(pTx(i), pTy(i), evader_radius, 'b');  % current position
        
        plot(pAx1(1),   pAy1(1),   'ro');  % start position indicator
        plot(pAx1(1:i), pAy1(1:i), 'r');   % path
        circle(pAx1(i), pAy1(i), pursuer_radius, 'r'); % current position
    end

%     clf;
%     hold on;
%     axis equal;
%     xline(0);
%     yline(0);
%     points = getClosedPolygonFromPointSet(capture_points(1,:), capture_points(2,:));
% %     plot_with_vectors(points, 'r-');
%     plot(points(1,:), points(2,:), 'Color', '#77AC30');
%     plot_with_vectors([0;0], 'bx');
%     plot_with_vectors([-1;0], 'rx');
%     xlabel('x');
%     ylabel('y');

    %% create: figure
%     set_fig_style_bc_squareish();
    set_fig_style_min_squareish();
    print_current_fig("pure_pursuit_DB_paths.png");
end


function circle(x, y, radius, color)
    r_half = radius / 2;
    x = x - r_half;
    y = y - r_half;
    pos = [x y radius radius];
    rectangle('Position', pos, 'Curvature', 1, 'FaceColor', color);
end

% if the velocity is
%   vTX = vT_norm * cos(angle)
%   vTY = vT_norm * sin(angle)
% then the position is
function [xT, yT] = pT_func(t, vT_norm, angle, xTI, yTI)
    xT = vT_norm .* cos(angle) .* t + xTI;
    yT = vT_norm .* sin(angle) .* t + yTI;
end


%% EP_SYS
function sys_vec_out = EP_sys( ...
        t,                     ...
        sys_vec_in,            ...
        A1_velocityPropConst,  ...
        angle                  ...
    )
    % % unpack
    in_pAx1 = sys_vec_in(1);
    in_pAy1 = sys_vec_in(2);
    
    % % ------------------------------------------------------------
    [vTx, vTy] = pT_func(t, 1, angle, 0, 0);

    pA1_scalar_part =          ...
        A1_velocityPropConst   ...
        /                      ... 
        sqrt( (vTx - in_pAx1)^2 + (vTy - in_pAy1)^2 );
    vAx1 = pA1_scalar_part * (vTx - in_pAx1);
    vAy1 = pA1_scalar_part * (vTy - in_pAy1);

    
    % % ------------------------------------------------------------
    % % pack
    sys_vec_out = [
            vAx1;   ...
            vAy1;    ...
        ];
end

%%
%%
%%
%%
%%
%%
%%

function asdf()
    heading_angle_count = 20;

    evader_direction_vectors = []; % column vectors
    evader_position  = []; % column vectors
    v_E = 1;
    v_P = 1.2;
    pursuer_position = []; % column vectors
    isPlaying = [];

    for angle = 0:(2*pi)/heading_angle_count:2*pi
        uvec = get_directional_uvec(angle);
        evader_direction_vectors = [evader_direction_vectors, uvec];

        evader_position  = [evader_position, [0;0]];   % setup "every" evaders'  p_I
        pursuer_position = [pursuer_position, [-1;0]]; % setup "every" pursuers' p_I
        isPlaying = [isPlaying, true];
    end
    
    time_step = 1/100;
    isPlaying
    while ~all( isPlaying)
        isPlaying
        % update evader if still alive
        for E_i = 1:size(evader_position, 2)
            if isPlaying(E_i)
                evader_position(:, E_i) = ...
                    evader_position(:, E_i) + evader_direction_vectors(E_i)*v_E*time_step;
            end
        end

        % render: setup
        clf;
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        
        for E_i = 1:size(evader_position, 2)
            plot_with_vectors(evader_position)
        end
        
        drawnow;
    end
end

