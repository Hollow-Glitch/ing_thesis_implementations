function numerical_bouger()
    
    %% Options
    create_fig_overlay                  = true;
    create_fig_static_scenarios         = true;
    run_animation                       = true;
    create_fig_animation                = true;
    print_intermediate_animation_states = false;
    
    %%
    disp("if you modify this and it does nothing, then it is");
    disp("because probably the end time is too much and collision");
    disp("happened a long time ago, and ode45 because of the option config");
    disp("is going nuts");
    if create_fig_overlay
        bouger_and_purePursuit_overlay();
    end
    
    if create_fig_static_scenarios
        static_scenarios();
    end

    % target's heading angle and the corresponding end time
    % we got this from running the simulation and in the end 
    % the last time is the collision time
%     angle = pi/2;        velocity_prop_const = 1.2; t_end   = 2.73;
%     angle = pi/2 + pi/4; velocity_prop_const = 1.2; t_end   = 1.2;
%     angle = pi/2 + pi/4; velocity_prop_const = 2;   t_end   = 0.80;
    angle = pi/2;        velocity_prop_const = 2;   t_end   = 3.36;
    
    time_struct.t_start = 0;
    time_struct.t_step  = 0.01;
    time_struct.t_end   = t_end;
    
    vT_norm = 1;
    xTI = 5;
    yTI = 0;
    
    % assert: vA_norm == velocity_prop_const * vT_norm
    xAI = 0;
    yAI = 0;

    if run_animation
        pirate_merchant_anim(angle, xTI, yTI, xAI, yAI, ...
                    vT_norm, velocity_prop_const, print_intermediate_animation_states);
    end

    clf;
    if create_fig_animation
        gen_fig(angle, xTI, yTI, xAI, yAI, ...
                vT_norm, velocity_prop_const, time_struct);
    end
end


function bouger_and_purePursuit_overlay()
    function y = bouger_eq_n_lt_1(x, x0, vM, vP)
        % vM ... merchant velocity
        % vP ... pirate velocity
        n = vM / vP;
    %     if ~(n<1)
    %         error('for this equation, it must be that n<1')
    %     end
        term_1 = (n*x0) / (1-n^2);
        term_2 = (x0 - x) ./ 2;
        term_3 = ( ( 1-x./x0).^n   ) / (1+n);
        term_4 = ( ( 1-x./x0).^(-n)) / (1-n);
        
        y = term_1 + term_2 .* (term_3 - term_4);
    end

    function gen_fig(angle, xTI, yTI, xAI, yAI, ...
                     vT_norm, velocity_prop_const, time_struct)
        t = time_struct.t_start : time_struct.t_step : time_struct.t_end;
        [xT, yT] = pT_func(t, vT_norm, angle, xTI, yTI);

        options = odeset('RelTol',1e-4,'AbsTol',1e-7);
        [t, sys_vec_out] = ode45(@(t,sys_vec_in) ...
              pA_DE(t, sys_vec_in, velocity_prop_const, vT_norm, angle,xTI,yTI), ...
              [time_struct.t_start, time_struct.t_end], [xAI, yAI], options);
        xA = sys_vec_out(:, 1);
        yA = sys_vec_out(:, 2);

        hold on;
        axis equal;
        xline(0);
        yline(0);
        plot(xT, yT);
        plot(xT(end), yT(end), 'o');
        plot(xA, yA);
        plot(xA(end), yA(end), 'o');
        xlabel('x');
        ylabel('y');
        ylim([min([yT, yA'])-0.1, max([yT, yA'])+0.1])
        xlim([min([xT, xA'])-0.1, max([xT, xA'])+0.1]);
    end

    angle = pi/2; velocity_prop_const = 2; t_end   = 3.36;
    time_struct.t_start = 0;
    time_struct.t_step  = 0.01;
    time_struct.t_end   = t_end;
    vT_norm = 1;
    xTI = 5;
    yTI = 0;
    % assert: vA_norm == velocity_prop_const * vT_norm
    xAI = 0;
    yAI = 0;

    clf;
    hold on;
    x = 0:0.001:xTI;
    y = bouger_eq_n_lt_1(x, xTI, vT_norm, vT_norm*velocity_prop_const);

    gen_fig(angle, xTI, yTI, xAI, yAI, ...
            vT_norm, velocity_prop_const, time_struct);
        
    plot(x, y, 'r--');

    %% create: figure
    name = sprintf('bouger_pure_pursuit_overlay__angle=%f__k=%f.png', angle, velocity_prop_const);
    set_fig_style_bc_squareish();
    print_current_fig(name);
end


function static_scenarios()
    clf;

    % target's heading angle and the corresponding end time
    % we got this from running the simulation and in the end 
    % the last time is the collision time
    %
    % it seems that the time that the live simulation gives, is a little
    % incorrect, or that when I draw the figure, then because of the nature
    % of the continuos simulation, this time increases
    angle = pi/2 + pi/4; velocity_prop_const = 2;   t_end   = 0.45;
    time_struct.t_start = 0;
    time_struct.t_step  = 0.01;
    time_struct.t_end   = t_end;
    vT_norm = 1;
    xTI = 1;
    yTI = 0;
    % assert: vA_norm == velocity_prop_const * vT_norm
    xAI = 0;
    yAI = 0;
    gen_fig(angle, xTI, yTI, xAI, yAI, ...
            vT_norm, velocity_prop_const, time_struct);
    clf;

    angle = pi/2;        velocity_prop_const = 1.2; t_end   = 2.73;
    time_struct.t_start = 0;
    time_struct.t_step  = 0.01;
    time_struct.t_end   = t_end;
    vT_norm = 1;
    xTI = 1;
    yTI = 0;
    % assert: vA_norm == velocity_prop_const * vT_norm
    xAI = 0;
    yAI = 0;
    gen_fig(angle, xTI, yTI, xAI, yAI, ...
            vT_norm, velocity_prop_const, time_struct);
    clf;

    angle = pi/2 + pi/4; velocity_prop_const = 1.2; t_end   = 1.14;
    time_struct.t_start = 0;
    time_struct.t_step  = 0.01;
    time_struct.t_end   = t_end;
    vT_norm = 1;
    xTI = 1;
    yTI = 0;
    % assert: vA_norm == velocity_prop_const * vT_norm
    xAI = 0;
    yAI = 0;
    gen_fig(angle, xTI, yTI, xAI, yAI, ...
            vT_norm, velocity_prop_const, time_struct);
    clf;
    
    angle = pi/2 + pi/4; velocity_prop_const = 2; t_end   = 2.16;
    time_struct.t_start = 0;
    time_struct.t_step  = 0.01;
    time_struct.t_end   = t_end;
    vT_norm = 1;
    xTI = 5;
    yTI = 0;
    % assert: vA_norm == velocity_prop_const * vT_norm
    xAI = 0;
    yAI = 0;
    gen_fig(angle, xTI, yTI, xAI, yAI, ...
            vT_norm, velocity_prop_const, time_struct);
    clf;
    
    angle = pi/2; velocity_prop_const = 2; t_end   = 3.36;
    time_struct.t_start = 0;
    time_struct.t_step  = 0.01;
    time_struct.t_end   = t_end;
    vT_norm = 1;
    xTI = 5;
    yTI = 0;
    % assert: vA_norm == velocity_prop_const * vT_norm
    xAI = 0;
    yAI = 0;
    gen_fig(angle, xTI, yTI, xAI, yAI, ...
            vT_norm, velocity_prop_const, time_struct);
    clf;
end


function gen_fig(angle, xTI, yTI, xAI, yAI, ...
                 vT_norm, velocity_prop_const, time_struct)
    t = time_struct.t_start : time_struct.t_step : time_struct.t_end;
    [xT, yT] = pT_func(t, vT_norm, angle, xTI, yTI);
    
    options = odeset('RelTol',1e-4,'AbsTol',1e-7);
    [t, sys_vec_out] = ode45(@(t,sys_vec_in) ...
          pA_DE(t, sys_vec_in, velocity_prop_const, vT_norm, angle,xTI,yTI), ...
          [time_struct.t_start, time_struct.t_end], [xAI, yAI], options);
    xA = sys_vec_out(:, 1);
    yA = sys_vec_out(:, 2);
    
    hold on;
    axis equal;
    xline(0);
    yline(0);
    plot(xT, yT);
    plot(xT(end), yT(end), 'o');
    plot(xA, yA);
    plot(xA(end), yA(end), 'o');
    xlabel('x');
    ylabel('y');
    ylim([min([yT, yA'])-0.1, max([yT, yA'])+0.1])
    xlim([min([xT, xA'])-0.1, max([xT, xA'])+0.1]);

    %% create: figure
    name = sprintf('1v1_target_moves_on_line__angle=%f__k=%f.png', angle, velocity_prop_const);
    set_fig_style_bc_squareish();
    print_current_fig(name);
end


function pirate_merchant_anim(angle, xTI, yTI, xAI, yAI, vT_norm, velocity_prop_const, print_intermediate_animation_states)
    options = odeset('RelTol',1e-7,'AbsTol',1e-7);

    xA = xAI;
    yA = yAI;
    xT = xTI;
    yT = yTI;
    anim_step = 0.03;
    t_cur = 0;
    while norm([xA(end), yA(end)] - [xT, yT]) > 0.001
        t_prev = t_cur;
        t_cur = t_prev + anim_step;
        clf;
        hold on;
        axis equal;
        xline(0);
        yline(0);
        xlabel('x');
        ylabel('y');

        [ode45_t, sys_vec_out] = ode45(@(t,sys_vec_in) ...
            pA_DE(t, sys_vec_in, velocity_prop_const, vT_norm, angle, xTI, yTI), ...
            [t_prev, t_cur], [xAI, yAI], options);
        xA = sys_vec_out(:, 1);
        yA = sys_vec_out(:, 2);
        xAI = xA(end);
        yAI = yA(end);

        [xT, yT] = pT_func(t_cur, vT_norm, angle, xTI, yTI);
        
%         xlim([0, 1]);
%         xlim([min([xA(end), xT]), max([xA(end), xT])]);

        plot(xA(end), yA(end), 'o');
        plot(xT, yT, 'o');

        if print_intermediate_animation_states
            fprintf("[xA, yA] = [%f, %f]\n", xA(end), yA(end));
            fprintf("[xT, yT] = [%f, %f]\n", xT, yT);
            fprintf("norm([xA, yA] - [xT, yT]) = %f\n", ...
                    norm([xA(end), yA(end)] - [xT, yT]) ...
            );
            fprintf("t_cur = %f\n", t_cur);
        end

        pause(0.03);
    end
    
    fprintf('\n==========\n');
    fprintf("[xA, yA] = [%f, %f]\n", xA(end), yA(end));
    fprintf("[xT, yT] = [%f, %f]\n", xT, yT);
    fprintf("norm([xA, yA] - [xT, yT]) = %f\n", ...
            norm([xA(end), yA(end)] - [xT, yT]) ...
    );
    fprintf("angle = %f\n", angle);
    fprintf("t_cur = %f\n", t_cur);
end


function sys_vec_out = pA_DE( ...
        t,                 ...
        sys_vec_in,        ...
        velocityPropConst, ...   % proportionality constant of the velocities
        vT_norm,           ...
        angle,             ...
        xTI, yTI           ...
    )
    % % unpack
    in_pAx = sys_vec_in(1);
    in_pAy = sys_vec_in(2);
    
    % % ------------------------------------------------------------
    
    in_pA = [ in_pAx , in_pAy ];
    
    [pTx, pTy] = pT_func(t, vT_norm, angle, xTI, yTI);
    pT = [ pTx , pTy ];

    dAT = pT - in_pA;
    vA = (velocityPropConst .* vT_norm / norm(dAT)).*dAT;
    
    vAx = vA(1);
    vAy = vA(2);
    
    % % ------------------------------------------------------------
    % % pack
    sys_vec_out = [
            vAx;   ...
            vAy    ...
        ];
end


% if the velocity is
%   vTX = vT_norm * cos(angle)
%   vTY = vT_norm * sin(angle)
% then the position is
function [xT, yT] = pT_func(t, vT_norm, angle, xTI, yTI)
    xT = vT_norm .* cos(angle) .* t + xTI;
    yT = vT_norm .* sin(angle) .* t + yTI;
end