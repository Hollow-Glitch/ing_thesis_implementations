function PP_of_evader_with_circular_path()
    %% Options
    create_video   = false;
    create_figures = true;
    % ! Either activate none, or only one of the above, activating both
    % will lead to errors.

    %%
    animate_and_create_fig(0.8, create_video, create_figures);
    animate_and_create_fig(1.2, create_video, create_figures);
end


function animate_and_create_fig(velocityPropConst, create_video, create_figures)
    base_name = sprintf('1v1_circ_path_k=%f', velocityPropConst); % for fig and video
    
    IC = [0, 0];
    EP_sys_closed = @(t, sys_vec_in) EP_sys( t, sys_vec_in, velocityPropConst );

    % solve sys
    options = odeset('RelTol',1e-5,'AbsTol',1e-5);
    [t, sys_vec_out] = ode45(EP_sys_closed, [0 5], IC, options);

    % unpack Attacker sys
    pAx = sys_vec_out(:, 1);
    pAy = sys_vec_out(:, 2);

    % calculate Target
    pTx = cos(t);
    pTy = sin(t);

    %% Animation
    
    % radia used to draw and check for collision
    common_radius = 0.05;
    evader_radius  = common_radius;
    pursuer_radius = common_radius;
    
    % set axes limits to content
    every_x = cat(1, pTx, pAx);
    every_y = cat(1, pTy, pAy);
    [my_xlim, my_ylim] = utils_v2.auto_limits_for_2D(every_x, every_y, 0.1, 0.1);
    
    if create_video
        video_name = [base_name, '.avi'];
        vidObj = VideoWriter(video_name);
        open(vidObj);
    end

    for i = 1:length(t)
        clf;
        hold on;
        axis equal;
        xlim(my_xlim);
        ylim(my_ylim);
        xline(0);
        yline(0);
        xlabel('x');
        ylabel('y');

        % Target
        plot(pTx(1),   pTy(1),   'bo');  % start position indicator
        plot(pTx(1:i), pTy(1:i), 'b');   % path
        circle(pTx(i), pTy(i), evader_radius, 'b');  % current position

        % Attacker
        plot(pAx(1),   pAy(1),   'ro');  % start position indicator
        plot(pAx(1:i), pAy(1:i), 'r');   % path
        circle(pAx(i), pAy(i), pursuer_radius, 'r'); % current position
        
        % % video
        if create_video
            writeVideo(vidObj, getframe(gcf));
        end

        % collision checking
        pA = [ pAx(i) ; pAy(i) ];
        pT = [ pTx(i) ; pTy(i) ];
        if norm(pT - pA) <= 0.05
            table(pA, pT)
            disp("m_p - p_p = ");
            disp(pT - pA);
            norm(pT - pA)
            disp("Booom");
            break
        end

        pause(0.01);
    end

    %% create: figure
    if create_figures
        set_fig_style_bc_squareish();
        print_current_fig(base_name);
    end

    %% video
    if create_video
        close(vidObj);
    end
end


function sys_vec_out = EP_sys( ...
        t,                 ...
        sys_vec_in,        ...
        velocityPropConst  ...   % proportionality constant of the velocities
    )
    % % unpack
    in_pAx = sys_vec_in(1);
    in_pAy = sys_vec_in(2);
    
    % % ------------------------------------------------------------
    
    scalar_part =           ...
        velocityPropConst   ...
        /                   ... 
        sqrt( (cos(t) - in_pAx)^2 + (sin(t) - in_pAy)^2 );
    vAx = scalar_part * (cos(t) - in_pAx);
    vAy = scalar_part * (sin(t) - in_pAy);
    
    % % ------------------------------------------------------------
    % % pack
    sys_vec_out = [
            vAx;   ...
            vAy    ...
        ];
end


function circle(x, y, radius, color)
    r_half = radius / 2;
    x = x - r_half;
    y = y - r_half;
    pos = [x y radius radius];
    rectangle('Position', pos, 'Curvature', 1, 'FaceColor', color);
end