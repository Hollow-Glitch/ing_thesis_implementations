function GPP_broken_line_path()

    % v_E ... evader's velocity
    % t_t ... travel time
    % t_T ... total travel time
    % 

    %% setup this program
    create_figure = true;
    create_video = false;
    
    %% Video: create video object
    if create_video
        vidObj = VideoWriter('general_PP_broken_evader_path.avi');
        open(vidObj);
    end

    %% general agent setup
    agent_radius = 0.5;
    termination_distance = agent_radius;

    %% evader setup
    v_E = 25;
    p_EI = [10;20];  
    % p_EI will be the first position in the sequence of positions of the evader's path.

    %% pursuer setup
    p_PI = [0;0];
    p_P = p_PI;
    v_P = 25;
    

    %% precalculations for evader
%     p1 = [10;20];
    p1 = p_EI;
    p2 = [10;10];
    p3 = [20;10];
    p4 = [25;20];
    p5 = [30;10];
    points = [p1, p2, p3, p4, p5];
    n = size(points, 2);
    t_t = [];  % sequence of travel times
    for i = 1:(n-1)
        p_a = points(:, i);           % p_a = p_i
        p_b = points(:, i+1);         % p_b = p_{i+1}
        t_ti = norm(p_b - p_a) / v_E; % i-th travel time

        t_t = [t_t, t_ti];            % update travel time seq.
    end

    t_T = sum(t_t);  % total travel time

    t_a = [];  % sequence of arrival times
    for j = 1:n
        t_ai = sum(t_t(1:j-1));  % calculate j-th arrival time
        t_a = [t_a, t_ai];       % update seq.
    end

    %% agent position collections for path rendering
    col__p_E = [];
    col__p_P = [];

    %% game loop
    i = 1;  % segment index
    step = 1/60;
    for t = 0 : step : t_T
        %% evader update
        if t_a(i+1) <= t
            i = i + 1;
        end
        t_ai = t_a(i);
        p_i = points(:, i);
        next__p_i = points(:, i+1);

        uvec = (next__p_i - p_i) / norm(next__p_i - p_i);

        p_E = p_i + (t - t_ai) * v_E * uvec;

        %% pursuer update
        uvecP = (p_E - p_P) / norm(p_E - p_P);
        p_P = p_P + step * v_P * uvecP;
        % the step here represents the time ammount by which we want the pursuer to change

        %% update agent position collections for path rendering
        col__p_E = [col__p_E, p_E];
        col__p_P = [col__p_P, p_P];

        %% render
        clf;
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf("$%.2f$sec", t), 'Interpreter','latex');
        plot_with_vectors(points, 'k.');
%         plot_with_vectors(p_E, 'bs');
%         plot_with_vectors(p_P, 'rd');
        plot_with_vectors(col__p_E, '--b');
        agent_repr(p_E, agent_radius, 'b');
        plot_with_vectors(col__p_P, '--r');
        agent_repr(p_P, agent_radius, 'r');
        textArroundPoint(p1, polarPoint(pi/2, 1), '$p_1$');
        textArroundPoint(p2, polarPoint(pi/2+pi/4, 1), '$p_2$');
        textArroundPoint(p3, polarPoint(-pi/2, 1), '$p_3$');
        textArroundPoint(p4, polarPoint(pi/2, 1), '$p_4$');
        textArroundPoint(p5, polarPoint(-pi/2-pi/4, 1), '$p_5$');
        xline(0);
        yline(0);

%         pause(1/60);
        pause(1/61)

        %% Video: write frame
        if create_video
            writeVideo(vidObj, getframe(gcf));
        end

        %% termination condition
        %  This must be here so that we see the state which led to
        %  termination if this would be above the render part then we would
        %  not see this state.
        if norm(p_E - p_P) < termination_distance
            break;
        end
    end

    %% Video: close video object
    if create_video
        close(vidObj);
    end

    %% figure creation
    if create_figure
        set_fig_style_bc_squareish();
        print_current_fig("general_PP_broken_evader_path");
    end
end

function point = polarPoint(theta, radius)
    [x,y] = pol2cart(theta, radius);
    point = [x;y];
end

function agent_repr(p, radius, color)
    % Used for rendering the agents on the playing field in different
    % colors.
    arguments
        p      (2,1)
        radius (1,1)
        color
    end
    x = p(1);
    y = p(2);
    r_half = radius / 2;
    x = x - r_half;
    y = y - r_half;
    pos = [x y radius radius];
    rectangle('Position', pos, 'Curvature', 1, 'FaceColor', color);
end