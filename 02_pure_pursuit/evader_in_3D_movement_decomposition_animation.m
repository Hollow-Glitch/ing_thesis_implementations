function evader_in_3D_movement_decomposition_animation()
    t = 0:0.01:1;
    r = 2;
    pTafa = 2*pi * 1; % angular frequency azimuthal
    pTafp = 2*pi * 1; % angular frequency polar
    pTx = r .* sin( pTafp * t ) .* cos( pTafa * t );
    pTy = r .* sin( pTafp * t ) .* sin( pTafa * t );
    pTz = r .* cos( pTafp * t );
    
    every_x = [
        pTx  ;
%         pAx1 ;
%         pAx2
    ];
    every_y = [
        pTy  ;
%         pAy1 ;
%         pAy2
    ];
    every_z = [
        pTz  ;
%         pAy1 ;
%         pAy2
    ];

    [my_xlim, my_ylim, my_zlim] = ...
        utils_v2.auto_limits_for_3D(every_x, every_y, every_z, 0.1, 0.1, 0.1);

    
    for i = 1:length(t)
        clf;
        hold on;
        axis equal;
        grid;
        view( [45, 15] );
        xlim(my_xlim);
        ylim(my_ylim);
        zlim(my_zlim);
        
        plot3(pTx(1),   pTy(1),   pTz(1),   'bo');  % start position indicator
        
        % 2D "COMPONENTS"
        plot3(pTx(1:i), pTy(1:i) - pTy(1:i), pTz(1:i) - pTz(1:i), 'r');   % path
        plot3(pTx(1:i) - pTx(1:i), pTy(1:i) - pTy(1:i), pTz(1:i), 'g');   % path
        plot3(pTx(1:i) - pTx(1:i), pTy(1:i), pTz(1:i) - pTz(1:i), 'k');   % path
        % at time t
        plot3(pTx(i), 0     , 0     , 'ro');   % path
        plot3(0     , 0     , pTz(i), 'go');   % path
        plot3(0     , pTy(i), 0     , 'ko');   % path
        
        % 2D "COMPONENTS"
        plot3(pTx(1:i), pTy(1:i), pTz(1:i) - pTz(1:i), 'r');   % path
        plot3(pTx(1:i), pTy(1:i) - pTy(1:i), pTz(1:i), 'g');   % path
        plot3(pTx(1:i) - pTx(1:i), pTy(1:i), pTz(1:i), 'k');   % path
        % at time t
        plot3(pTx(i), pTy(i), pTz(i) - pTz(i), 'ro');   % path
        plot3(pTx(i), pTy(i) - pTy(i), pTz(i), 'go');   % path
        plot3(pTx(i) - pTx(i), pTy(i), pTz(i), 'ko');   % path

        % 3D stuff
        plot3(pTx(1),   pTy(1),   pTz(1),   'bo');  % start position indicator
        plot3(pTx(1:i), pTy(1:i), pTz(1:i), 'b');   % path
        plot3(pTx(i),   pTy(i),   pTz(i),   'bo');  % start position indicator
        %circle(pTx(i), pTy(i), pTz(i), evader_radius, 'b');  % current position
        
        pause(0.01);
    end
end

function old_main()
    
    IC = [-1, 0, 1, 1];
    vA1_propConst = 0.8;
    vA2_propConst = 0.8;
    EP_sys_closed = @(t, sys_vec_in) EP_sys( t, sys_vec_in, vA1_propConst, vA2_propConst );

    % solve sys
    options = odeset('RelTol',1e-7,'AbsTol',1e-7);
    [t, sys_vec_out] = ode45(EP_sys_closed, [0 3], IC, options);

    % unpack Attacker sys
    pAx1 = sys_vec_out(:, 1);
    pAy1 = sys_vec_out(:, 2);
    pAx2 = sys_vec_out(:, 3);
    pAy2 = sys_vec_out(:, 4);
    % calculate Target
    pTx = cos(t);
    pTy = sin(t);

    %% Animation
    
    % radia used to draw and check for collision
    common_radius = 0.05;
    evader_radius  = common_radius;
    pursuer_radius = common_radius;
    
    % set axes limits to content
    every_x = [
            pTx  ;
            pAx1 ;
            pAx2
        ];
    every_y = [
            pTy  ;
            pAy1 ;
            pAy2
        ];
    [my_xlim, my_ylim] = utils_v2.auto_limits_for_2D(every_x, every_y, 0.1, 0.1);
    xlim(my_xlim);
    ylim(my_ylim);
    hold on;
    
    % % video
    % vidObj = VideoWriter('peaks.avi');
    % open(vidObj);

    for i = 1:length(t)
        clf;
        hold on;
        axis equal;
        xlim(my_xlim);
        ylim(my_ylim);
        xline(0);
        yline(0);

        % Target
        plot(pTx(1),   pTy(1),   'bo');  % start position indicator
        plot(pTx(1:i), pTy(1:i), 'b');   % path
        circle(pTx(i), pTy(i), evader_radius, 'b');  % current position

        % Attacker
        plot(pAx1(1),   pAy1(1),   'ro');  % start position indicator
        plot(pAx1(1:i), pAy1(1:i), 'r');   % path
        circle(pAx1(i), pAy1(i), pursuer_radius, 'r'); % current position
        
        % Attacker 2
        plot(pAx2(1),   pAy2(1),   'ro');  % start position
        plot(pAx2(1:i), pAy2(1:i), 'r');   % path
        circle(pAx2(i), pAy2(i), pursuer_radius, 'r'); % curent position

        pT  = [pTx(i),  pTy(i)];
        pA1 = [pAx1(i), pAy1(i)];
        pA2 = [pAx2(i), pAy2(i)];
        if norm(pT - pA1) <= 0.05
            table(pA1, pT)
            disp("pT - pA1 = ");
            disp(pT - pA1);
            norm(pT - pA1)
            disp("A1-Booom");
            break
        end

        if norm(pT - pA2) <= 0.05
            table(pA2, pT)
            disp("pT - pA2 = ");
            disp(pT - pA2);
            norm(pT - pA2)
            disp("A2-Booom");
            break
        end

        pause(0.01);
    end
    
    % generate figure of end state
    fig = gcf;
    utils_v2.figure_decorators.bc_style_squareish(fig, 'width_multiplier', 1);
    % name = sprintf('pend_n=2_mass_path_ID=%s', ID);
    name = '1v1_circ_path';
    fprintf("saving figure to file with name: %s\n", name);
    utils_v2.smart_plot_print(fig, name);

    % % video
    % close(vidObj);
end

