function PP_DB_with_limacon_overlay()
    PP_DB();

    theta = 0 : 0.01 : 2*pi;

    a = 2;
    b = 2.6;

    x = (b + a .* cos(theta)) .* cos(theta) + 0.2;
    y = (b + a .* cos(theta)) .* sin(theta);

%     clf;
    axis equal;
    hold on;
    xline(0);
    yline(0);
%     plot(x, y, '-b');
    plot(x, y, Color='#7E2F8E', LineStyle='--');

    %% create: figure
    set_fig_style_bc_squareish();
    print_current_fig("pure_pursuit_DB_is_a_limacon_question_mark.png");
end

