function PP_in_3D()
    %% Options
    create_video   = false;
    create_figures = false;
    % ! Either activate none, or only one of the above, activating both
    % will lead to errors.

    %%
    animate_and_create_fig(12, create_video, create_figures);
    animate_and_create_fig(50, create_video, create_figures);
end


function animate_and_create_fig(pursuer_velocity, create_video, create_figures)
    base_name = sprintf('1v1_3D_circ_path_v_P=%f', pursuer_velocity);

    IC = [1, 1, -2];
    EP_sys_closed = @(t, sys_vec_in) EP_sys( t, sys_vec_in, pursuer_velocity );
    
    % solve sys
    options = odeset('RelTol',1e-7,'AbsTol',1e-7);
    [t, sys_vec_out] = ode45(EP_sys_closed, [0 0.8], IC, options);

    % unpack Attacker sys
    pAx = sys_vec_out(:, 1);
    pAy = sys_vec_out(:, 2);
    pAz = sys_vec_out(:, 3);
    
    % calculate Target
    r = 2;
    pTafa = 2*pi * 1; % angular frequency azimuthal
    pTafp = 2*pi * 1; % angular frequency polar
    pTx = r .* sin( pTafp * t ) .* cos( pTafa * t );
    pTy = r .* sin( pTafp * t ) .* sin( pTafa * t );
    pTz = r .* cos( pTafp * t );
    
    every_x = [pTx; pAx];
    every_y = [pTy; pAy];
    every_z = [pTz; pAz];

    [my_xlim, my_ylim, my_zlim] = ...
        utils_v2.auto_limits_for_3D(every_x, every_y, every_z, 0.1, 0.1, 0.1);

    if create_video
        video_name = [base_name, '.avi'];
        vidObj = VideoWriter(video_name);
        open(vidObj);
    end

    for i = 1:length(t)
        clf;
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        zlabel('z');
        grid;
        view( [45, 15] );
        xlim(my_xlim);
        ylim(my_ylim);
        zlim(my_zlim);
        
        % Target
        plot3(pTx(1),   pTy(1),   pTz(1),   'ks');  % start position indicator
        plot3(pTx(1:i), pTy(1:i), pTz(1:i), 'b');   % path
        plot3(pTx(i),   pTy(i),   pTz(i),   'bo');  % start position indicator
        
        % Attacker
        plot3(pAx(1),   pAy(1),   pAz(1),   'ks');  % start position indicator
        plot3(pAx(1:i), pAy(1:i), pAz(1:i), 'r');   % path
        plot3(pAx(i),   pAy(i),   pAz(i),   'ro');  % start position indicator
        
%         pause(0.01);
        drawnow;

        % % video
        if create_video
            writeVideo(vidObj, getframe(gcf));
        end

        pT  = [pTx(i);  pTy(i); pTz(i)];
        pA1 = [pAx(i); pAy(i); pAz(i)];
        if norm(pT - pA1) <= 0.22
            table(pA1, pT)
            disp("pT - pA1 = ");
            disp(pT - pA1);
            norm(pT - pA1)
            disp("A1-Booom");
%             capture_points = [capture_points, pT];
            break
        end
    end

    %% create: figure
    if create_figures
        set_fig_style_bc_squareish();
        print_current_fig(base_name);
    end

    %% video
    if create_video
        close(vidObj);
    end
end


function sys_vec_out = EP_sys( ...
        t,                 ...
        sys_vec_in,        ...
        pursuer_velocity  ...   % proportionality constant of the velocities
    )
    % % unpack
    in_pAx = sys_vec_in(1);
    in_pAy = sys_vec_in(2);
    in_pAz = sys_vec_in(3);
    
    % % ------------------------------------------------------------
    
    in_pA = [ in_pAx , in_pAy , in_pAz];
    
    r = 2;
    pTafa = 2*pi * 1; % angular frequency azimuthal
    pTafp = 2*pi * 1; % angular frequency polar
    pTx = r .* sin( pTafp * t ) .* cos( pTafa * t );
    pTy = r .* sin( pTafp * t ) .* sin( pTafa * t );
    pTz = r .* cos( pTafp * t );
    pT = [ pTx , pTy , pTz ];
%     vTx = r * ( pTafp*cos(pTafp *t)*cos(pTafa *t)    ...
%               - sin(pTafp *t)*pTafa*sin(pTafa *t) );
%     vTy = r * ( pTafp*cos(pTafp *t)*sin(pTafa *t)    ...
%               - sin(pTafp *t)*pTafa*cos(pTafa *t) );
%     vTz = -r*pTafp*sin(pTafp*t);
%     vT = [ vTx , vTy , vTz ];
    
    dAT = pT - in_pA;
    
%     scalar_part = velocityPropConst * norm(vT) / norm(dAT);

%     scalar_part = 12;
%     vA = scalar_part .* dAT;
    
    vA = pursuer_velocity .* dAT;
    
    vAx = vA(1);
    vAy = vA(2);
    vAz = vA(3);
    
    % % ------------------------------------------------------------
    % % pack
    sys_vec_out = [
            vAx;   ...
            vAy;   ...
            vAz    ...
        ];
end