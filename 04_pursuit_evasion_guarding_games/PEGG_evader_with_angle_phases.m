function PEGG_evader_with_angle_phases()

    %% options
    time_step = 1 / 100;

    % Misc. options
    render_initial_DB_circle = true;

    % Figure & video options
    create_figure = false;
    create_video  = false;
    % !!! : figures and video at the same time can't be created

    %%
    base_fig_name = 'PEGG_evader_with_angle_phases__';
    video_name = base_fig_name;

    predetermined_end_time = 20;
    capture_radius = 1;

    %% Video: create video object
    if create_video
        vidObj = VideoWriter([video_name, '.avi']);
        open(vidObj);
    end
    
    %% scenario setup
    t = 0;
    r_d = 100;
    p_EI = r_d * get_directional_uvec(0);
    p_PI = r_d * get_directional_uvec(2/3*pi);
    p_GI = r_d * get_directional_uvec(4/3*pi);
    v_E = 25;
    angular__v_E = -pi/5;

    k = 2;
    v_P = k * v_E;

    k_GP = 2;
    v_G = k * v_P;

    %% render: setup
    clf;
    hold on;
    axis equal;
    xlabel('x');
    ylabel('y');

    %% render: positions
    plot_with_vectors(p_PI, 'r*');
    plot_with_vectors(p_PI, 'rd');
    plot_with_vectors(p_EI, 'b*');
    plot_with_vectors(p_EI, 'bs');
    plot_with_vectors(p_GI, 'g*');
    plot_with_vectors(p_GI, 'gv');

    %% render: PE DB
    [c_PE, r_PE] = DB_circle_center_and_radius(...
        p_PI(1), p_PI(2), p_EI(1), p_EI(2), k);
    col_circle(c_PE(1), c_PE(2), r_PE, 'k');

    %% render: GP DB
    [c_GP, r_GP] = DB_circle_center_and_radius(...
        p_GI(1), p_GI(2), p_PI(1), p_PI(2), k_GP);
    col_circle(c_GP(1), c_GP(2), r_GP, 'k');

    %%  Limits for x,y which will be used for all figures so that the reducing
    %   dominance region of the evader can be seen.
    my_xlim = 1.05 .* xlim;
    my_ylim = 1.05 .* ylim;
    xlim(my_xlim);
    ylim(my_ylim);

    %% calculating potential capture point
%     points = circleLineIntersections(p_C(:, 2), r_C, p_EI, p_PI(:, 2));
%     plot_with_vectors(points(:, 1), 'kx');

    %% calculating heading vectors: evader
    uvec__v_E = (p_PI - p_EI) / norm(p_PI - p_EI);
%     psi_EI = anti_clockwise_angle_from_x_of_vector(uvec__v_E); % initial heading angle of E
    psi_EI = 2/3*pi;
    psi_E2 = psi_EI + 2/3*pi;
    psi_E3 = psi_E2 + 2/3*pi;

    %% calculating heading vectors: pursuers
    %  strat1
%     uvec__v_P = [];
%     uvec__v_P1 = uvec(p_T - p_PI(:, 1));

    %% Figure: create initial state figure
    if create_figure
        clf
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");
        xlim(my_xlim);
        ylim(my_ylim);

        uvec__v_E = get_directional_uvec(psi_EI);
        p_E = p_EI + v_E * time_step * uvec__v_E;
        col_circle(c_PE(1), c_PE(2), r_PE, 'k');
        p_CPE = circleLineIntersections(c_PE, r_PE, p_EI, p_E);
        p_PT = p_CPE(:, 2); % target point
        uvec__v_P = (p_PT - p_PI) / norm(p_PI - p_PT);

        plot_with_vectors(p_PI, 'r*');
        plot_with_vectors(p_PI, 'rd');
        plot_with_vectors(p_EI, 'b*');
        plot_with_vectors(p_EI, 'bs');
        plot_with_vectors(p_PT , 'k*');

        % render: heading lines
        line_p1p2l_draw(p_PI, p_PI + uvec__v_P, norm(p_PI - p_PT), 'k--');
        line_p1p2l_draw(p_EI, p_EI + uvec__v_E, norm(p_EI - p_PT), 'k--');

        fig_name = sprintf([base_fig_name, 'initial_state_t=%.2f'], t);
        set_fig_style_bc_squareish();
        % set_fig_style_min_squareish();
        print_current_fig(fig_name, "outputType","pdf");
    end

    %% path collections
    col__p_P = [];
    col__p_E = [];
    col__p_G = [];

    %% game loop
    p_P = p_PI;
    p_E = p_EI;
    p_G = p_GI;
    psi_E = psi_EI;

    t = 0;
    isPlaying = true;
    while isPlaying
        loop_start_time = tic;

        t = t + time_step;

        % update evader
        if t < 0.5
            psi_E = psi_EI;
        elseif t < 1
            psi_E = psi_E2;
        elseif t < 1.5
            psi_E = psi_E3;
        end
        psi_E = psi_E + angular__v_E * time_step;
        uvec__v_E = get_directional_uvec(psi_E);
        old__p_E = p_E;
        p_E = p_E + v_E * time_step * uvec__v_E;

        % PE DB-circle
        [c_PE, r_PE] = DB_circle_center_and_radius(...
            p_P(1), p_P(2), p_E(1), p_E(2), k);
        p_CPE = circleLineIntersections(c_PE, r_PE, old__p_E, p_E);
        % p_CGP ... potential [c]apture [p]oint of [E]vader by [P]ursuer
        p_PT = p_CPE(:, 2); % pursuer target point

        % update pursuers
        uvec__v_P = (p_PT - p_P) / norm(p_P - p_PT);
        old__p_P = p_P;
        p_P = p_P + v_P * time_step * uvec__v_P;

        % GP DB-circle
        [c_GP, r_GP] = DB_circle_center_and_radius(...
            p_G(1), p_G(2), p_P(1), p_P(2), k_GP);
        p_CGP = circleLineIntersections(c_GP, r_GP, old__p_P, p_P);
        % p_CGP ... potential [c]apture [p]oint of [P]ursuer by [G]uardian
        p_GT = p_CGP(:, 2); % guardian target point

        % update guardian
        uvec__v_G = (p_GT - p_G) / norm(p_G - p_GT);
        p_G = p_G + v_G * time_step * uvec__v_G;

        % add positions to path collections
        col__p_E = [col__p_E, p_E];
        col__p_P = [col__p_P, p_P];
        col__p_G = [col__p_G, p_G];

        % render: setup
        clf;
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");
        xlim(my_xlim);
        ylim(my_ylim);

        % render: agent paths
        plot_with_vectors(col__p_E, 'b-');
        plot_with_vectors(col__p_P, 'r-');
        plot_with_vectors(col__p_G, 'g-');

        % render: heading lines
        line_p1p2l_draw(p_P, p_P + uvec__v_P, norm(p_P - p_PT), 'k--');
        line_p1p2l_draw(p_E, p_E + uvec__v_E, norm(p_E - p_PT), 'k--');
        line_p1p2l_draw(p_G, p_G + uvec__v_G, norm(p_G - p_GT), 'k--');

        % render: positions
        plot_with_vectors(p_PI, 'r*');
        plot_with_vectors(p_P , 'rd');

        plot_with_vectors(p_EI, 'b*');
        plot_with_vectors(p_E , 'bs');
        plot_with_vectors(p_PT , 'k*');

        plot_with_vectors(p_GI, 'g*');
        plot_with_vectors(p_G , 'gv');
        plot_with_vectors(p_GT , 'k*');

        % render: DB-circles
        col_circle(c_PE(1), c_PE(2), r_PE, 'k');
        col_circle(c_GP(1), c_GP(2), r_GP, 'k');

        if render_initial_DB_circle
            [c_PE, r_PE] = DB_circle_center_and_radius(...
                p_PI(1), p_PI(2), p_EI(1), p_EI(2), k);
            col_circle(c_PE(1), c_PE(2), r_PE, 'k--');

            [c_GP, r_GP] = DB_circle_center_and_radius(...
                p_GI(1), p_GI(2), p_PI(1), p_PI(2), k_GP);
            col_circle(c_GP(1), c_GP(2), r_GP, 'k--');
        end

        % !!! DRAW !!!
        elapsed_time = toc(loop_start_time);
%         pause(1/60);
        drawnow;
%         print_var(t);
%         print_var(elapsed_time);
%         psi_E

        % create media
        if create_figure
            fig_time_1 = 1;
            fig_time_2 = 2;
            fig_time_3 = 3;
            fig_time_4 = 4;
            fig_time_5 = 5;
            fig_time_6 = 6;
            fig_epsilon = 0.001;
            if float_eq(t, fig_time_1, fig_epsilon) ...
            || float_eq(t, fig_time_2, fig_epsilon) ...
            || float_eq(t, fig_time_3, fig_epsilon) ...
            || float_eq(t, fig_time_4, fig_epsilon) ...
            || float_eq(t, fig_time_5, fig_epsilon) ...
            || float_eq(t, fig_time_6, fig_epsilon) ...
            || norm(p_P - p_E) < capture_radius
                fig_name = sprintf([base_fig_name, 't=%.2f'], t);
                set_fig_style_bc_squareish();
                % set_fig_style_min_squareish();
                print_current_fig(fig_name, "outputType","pdf");
            end
        end
        if create_video
            writeVideo(vidObj, getframe(gcf));
        end

        % termination condition
        if norm(p_P - p_E) < capture_radius
            disp('Evader captured!');
            isPlaying = false;
        end

        % termination condition
        if norm(p_G - p_P) < capture_radius
            disp('Pursuer captured!');
            isPlaying = false;
        end

        % manual interupt
        keyboard_inputs = get(gcf, 'CurrentCharacter');
        if ~isempty(keyboard_inputs) && ismember('q', keyboard_inputs)
            set(gcf, 'CurrentCharacter', '@');
            disp("You pressed 'q', closing the program.");
            isPlaying = false;
        end

        % predetermined interupt
        if predetermined_end_time < t
            disp('Predetermined interupt!');
            isPlaying = false;
        end
    end

    %% Video: close video object
    if create_video
        close(vidObj);
    end

    %% Figure: create final state figure
    if create_figure
        clf
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");
        xlim([-18.4225, 218.6564]);
        ylim([-74.7272, 112.2592]);

        plot_with_vectors(p_PI, 'r*');
        plot_with_vectors(p_P, 'rd');
        plot_with_vectors(p_EI, 'b*');
        plot_with_vectors(p_E, 'bs');

        plot_with_vectors(col__p_E, 'b-');
        plot_with_vectors(col__p_P, 'r-');

        fig_name = sprintf([base_fig_name, 'final_paths_t=%.2f'], t);
        set_fig_style_bc_squareish();
        % set_fig_style_min_squareish();
        print_current_fig(fig_name, "outputType","pdf");
    end

    % reset in case the simulation ended without manual interuption
    set(gcf, 'CurrentCharacter', '@');
end

