function [y_m, y_p] = hyperbola_cartes(a,b,x)
    % [y_m, y_p] = hyperbola(a,b,x)
    %
    % y_m  ...  -y
    % y_p  ...  +y
    %
    % preconditions:
    % x > a
    arguments
        a (1,1)
        b (1,1)
        x (1,1)
    end

    y = sqrt(               ...
        b^2 * (             ...
            (x^2)/(a^2) -1  ...
        )                   ...
    );

    y_p =  y;
    y_m = -y;
end

