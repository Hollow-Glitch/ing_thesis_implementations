function logic_out = is_point_on_hyperbola(a, b, point)
    arguments
        a (1,1)
        b (1,1)
        point (2,1)
    end

    [y_m, y_p] = hyperbola_cartes(a, b, point(1));

    if (y_m == point(2)  ||  y_p == point(2))
        logic_out = true;
    else
        logic_out = false;
    end
end
