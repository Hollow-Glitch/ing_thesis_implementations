function [point1, point2] = DB_circle_tangent_points(xAI, yAI, xTI, yTI, k)
    if k > 1
        % [p]osition of the [C]circle [A]ppolonius
        pCA = [
            (k^2 .* xTI - xAI) / (k^2-1)  ,  (k^2 .* yTI - yAI) / (k^2-1)
        ];
        pAI = [xAI,  yAI];
        pTI = [xTI,  yTI];

        % distance between the attacker's initial position and the 
        % appolonius circle's center
        d_TI_CA = norm(pCA - pAI);
        d_AI_TI = norm(pTI - pAI);

        rCA = (k .* d_AI_TI) / (1-k^2);

        theta = asin( rCA / d_TI_CA );

        theta = abs(theta);

        % hehe :) I was right, this works
        add_this_angle = anti_clockwise_angle_from_x_of_vector(pTI - pAI);

        % let's try with the polar representation of a line
    %     theta_1 = theta + pi;
    %     theta_2 = -theta + pi;
        theta_1 = theta + add_this_angle;
        theta_2 = -theta + add_this_angle;

%         l = 0:1:15;
        l = sqrt(d_TI_CA^2 - rCA^2);
        x = xAI + l .* cos(theta_1);
        y = yAI + l .* sin(theta_1);
%         plot(x,y);
        point1 = [x; y];

        x = xAI + l .* cos(theta_2);
        y = yAI + l .* sin(theta_2);
%         plot(x,y);
        point2 = [x; y];
    elseif k < 1
        % [p]osition of the [C]circle [A]ppolonius
        pCA = [
            (k^2 .* xTI - xAI) / (k^2-1)  ,  (k^2 .* yTI - yAI) / (k^2-1)
        ];
        pAI = [xAI,  yAI];
        pTI = [xTI,  yTI];

        % distance between the attacker's initial position and the 
        % appolonius circle's center
        d_TI_CA = norm(pCA - pTI);
        d_AI_TI = norm(pTI - pAI);

        rCA = (k .* d_AI_TI) / (1-k^2);

        theta = asin( rCA / d_TI_CA );

        theta = abs(theta);

        % hehe :) I was right, this works
        add_this_angle = anti_clockwise_angle_from_x_of_vector(pAI - pTI);

        % let's try with the polar representation of a line
    %     theta_1 = theta + pi;
    %     theta_2 = -theta + pi;
        theta_1 = theta + add_this_angle;
        theta_2 = -theta + add_this_angle;

%         l = 0:1:15;
        l = sqrt(d_TI_CA^2 - rCA^2);
        x = xTI + l .* cos(theta_1);
        y = yTI + l .* sin(theta_1);
%         plot(x,y);
        point1 = [x; y];

        x = xTI + l .* cos(theta_2);
        y = yTI + l .* sin(theta_2);
%         plot(x,y);
        point2 = [x; y];
    end
end
