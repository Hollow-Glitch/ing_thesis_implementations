function logic_out = is_point_above_line(a, b, point)
    % logic_out = is_point_above_line(a, b, point)
    %
    % y = ax + b
    arguments
        a      (1,1)
        b      (1,1)
        point  (2,1)
    end

    y = a * point(1) + b;

    if y < point(2)
        logic_out = true;
    else
        logic_out = false;
    end
end