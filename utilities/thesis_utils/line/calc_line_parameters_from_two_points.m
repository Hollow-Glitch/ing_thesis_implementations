function [a, b] = calc_line_parameters_from_two_points(point1, point2)
    % [a, b] = calc_line_parameters_from_two_points(point1, point2)
    % 
    % line:   y = ax + b
    arguments
        point1  (2,1)
        point2  (2,1)
    end

    % l ... left point
    % r ... right point
    if point1(1) < point2(1)
        l = point1;
        r = point2;
    else
        l = point2;
        r = point1;
    end
    % Symbols have been choosen so that no visual or symbolical collision
    % is possible.

    x1 = l(1);
    y1 = l(2);
    x2 = r(1);
    y2 = r(2);
    
    a = (y2 - y1) / (x2 - x1);
    b = (x2*y1 - x1*y2) / (x2 - x1);
end

