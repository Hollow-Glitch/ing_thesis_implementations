function logic_out = is_point_on_line(a, b, point)
    % logic_out = is_point_on_line(a, b, point)
    %
    % y = ax + b
    arguments
        a      (1,1)
        b      (1,1)
        point  (2,1)
    end

    y = a * point(1) + b;

    if ~(y < point(2) || point(2) < y)
        logic_out = true;
    else
        logic_out = false;
    end
end