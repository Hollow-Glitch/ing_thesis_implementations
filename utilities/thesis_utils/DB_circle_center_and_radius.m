function [center, radius ] = DB_circle_center_and_radius(xAI, yAI, xTI, yTI, k)
    radius = (k.*sqrt( (xAI-xTI)^2 + (yAI-yTI)^2 )) / (1-k^2);
    radius = abs(radius);
    
    centerX = (k^2 .* xTI - xAI) / (k^2-1);
    centerY = (k^2 .* yTI - yAI) / (k^2-1);
    center = [centerX; centerY];
end

