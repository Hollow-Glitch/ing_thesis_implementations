function test_of_hyperbola_validators()
    a = 2;
    b = 3;

    left_arm_width  = 2;
    right_arm_width = 2;

    arm_left_up   = [];
    arm_left_down = [];
    for x = -a-left_arm_width : 0.1 : -a
        [y_m, y_p] = hyperbola_cartes(a,b,x);
        arm_left_up   = [arm_left_up,   [x; y_p] ];
        arm_left_down = [arm_left_down, [x; y_m] ];
    end

    arm_right_up   = [];
    arm_right_down = [];
    for x = a : 0.1 : a+right_arm_width
        [y_m, y_p] = hyperbola_cartes(a,b,x);
        arm_right_up   = [arm_right_up,   [x; y_p] ];
        arm_right_down = [arm_right_down, [x; y_m] ];
    end

    hold on;
    axis equal;
    xline(0);
    yline(0);
    plot_with_vectors(arm_left_up,    'r');
    plot_with_vectors(arm_left_down,  'g');
    plot_with_vectors(arm_right_up,   'b');
    plot_with_vectors(arm_right_down, 'm');

    radius = 5;
%     points = [];
    for angle = 0:0.3:2*pi
        x = radius * cos(angle);
        y = radius * sin(angle);
        point = [x; y];
        marker = '';
        if     is_point_in_left_hyperbola_region(a,b,point)
            marker = 'L';
        elseif is_point_in_right_hyperbola_region(a,b,point)
            marker = 'R';
        elseif is_point_in_center_hyperbola_region(a,b,point)
            marker = 'C';
        elseif is_point_on_hyperbola(a,b,point)
            marker = 'O';
        end
        textArroundPoint(point,  -0.5*uvec(point), marker);
        plot_with_vectors(point, '*k');
    end
end

