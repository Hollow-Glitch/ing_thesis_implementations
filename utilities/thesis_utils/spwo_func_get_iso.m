function [xc, yc] = spwo_func_get_iso(f1,f2,vTnorm,pTI, t)
    % [xc, yc] = spwo_func_get_iso(f1,f2,vTnorm,pTI, t)
    %
    % Given an agent's velocity norm and position at some time `t`,
    % this procedure returns the isochrone curve corresponding to
    % this agent for the givem time moment in the viccinity of a simple
    % line segment obstacle.

    DEBUG = false;
    %% setup
    STEP = 0.01;
    isLeftSide = isPointOnLeftOfLine(pTI, f1, f2);
    if isLeftSide
        temp = f1;
        f1 = f2;
        f2 = temp;
    end
    
    xc = [];
    yc = [];
    
    if DEBUG
        print_var(isLeftSide);
    end
    
    %% precalculations
    % - for f1
    vec_TI_f1 = f1 - pTI;
    norm_vec_TI_f1 = norm(vec_TI_f1);
    f1_t = norm_vec_TI_f1 / vTnorm; % time it takes to get from pTI to f1

    % -for f2
    vec_TI_f2 = f2 - pTI;
    norm_vec_TI_f2 = norm(vec_TI_f2);
    f2_t = norm_vec_TI_f2 / vTnorm; % time it takes to get from pTI to f2
    
    vec_f1_f2 = f2 - f1;
    vec_f2_f1 = -vec_f1_f2;

    angle_vec_TI_F1 = anti_clockwise_angle_from_x_of_vector(vec_TI_f1);
    angle_vec_TI_F2 = anti_clockwise_angle_from_x_of_vector(vec_TI_f2);
    angle_vec_f1_f2 = anti_clockwise_angle_from_x_of_vector(vec_f1_f2);
    angle_vec_f2_f1 = anti_clockwise_angle_from_x_of_vector(vec_f2_f1);

    distanceFromWall = getPointLineDistance(f1, f2, pTI);
    sweepDistance = vTnorm * t;
    
    if DEBUG
        print_var(distanceFromWall);
        print_var(sweepDistance);
    end
    
    %% Region1
    angleStart = angle_vec_TI_F1;
    angleEnd = angle_vec_TI_F2;
    if angleStart > angleEnd
        angleEnd = angleEnd + 2*pi;
    end
    angleInterval = angleStart:STEP:angleEnd;
    
    x_region1 = t*vTnorm .* cos(angleInterval) + pTI(1);
    y_region1 = t*vTnorm .* sin(angleInterval) + pTI(2);
    
    xc = [xc, x_region1];
    yc = [yc, y_region1];
    
    %% OTHERS
    
    if  distanceFromWall >= sweepDistance
        %% Region Inner
        angleStart = angle_vec_TI_F2;
        angleEnd = angle_vec_TI_F1;
        if angleStart > angleEnd
            angleEnd = angleEnd + 2*pi;
        end
        angleInterval = angleStart:STEP:angleEnd;
        x_regionInner = t*vTnorm .* cos(angleInterval) + pTI(1);
        y_regionInner = t*vTnorm .* sin(angleInterval) + pTI(2);
        xc = [xc, x_regionInner];
        yc = [yc, y_regionInner];
    else
        %% Region Inner broken
        points = circleLineIntersections(pTI, vTnorm*t, f1, f2);
        pI1 = points(:,1);  % pI1 ... 1. intersect point
        pI2 = points(:,2);  % pI2 ... 2. intersect point
        
        if sweepDistance > norm_vec_TI_f1 && sweepDistance > norm_vec_TI_f2
            cIpoints = circleCircleIntersections(f1, (t-f1_t)*vTnorm, f2, (t-f2_t)*vTnorm);
        else
            cIpoints = [];
        end
        
        if sweepDistance < norm_vec_TI_f1
            % LEFT
            vec_pTI_to_pIntersec2 = pI1 - pTI;
            angle_vec_pTI_to_pIntersec2 = anti_clockwise_angle_from_x_of_vector(vec_pTI_to_pIntersec2);
            
            angleStart = angle_vec_pTI_to_pIntersec2;
            angleEnd = angle_vec_TI_F1;
            if angleStart > angleEnd
                angleEnd = angleEnd + 2*pi;
            end
            angleInterval = angleStart:STEP:angleEnd;
            x_regionBroken1 = t*vTnorm .* cos(angleInterval) + pTI(1);
            y_regionBroken1 = t*vTnorm .* sin(angleInterval) + pTI(2);

            xc = [x_regionBroken1, xc];   % THIS SHOULDN'T BE HERE BUT BEFORE REG.1
            yc = [y_regionBroken1, yc];
        else
            if isempty(cIpoints)
                angleStart = angle_vec_f1_f2;
                angleEnd = angle_vec_TI_F1;
            else
                cI2 = cIpoints(:, 2);
                angleEnd = angle_vec_TI_F1;
                angleStart = anti_clockwise_angle_from_x_of_vector(cI2 - f1);
            end
            if angleStart > angleEnd
                angleEnd = angleEnd + 2*pi;
            end
            angleInterval = angleStart:STEP:angleEnd;
            x_region3 = (t-f1_t)*vTnorm .* cos(angleInterval) + f1(1);
            y_region3 = (t-f1_t)*vTnorm .* sin(angleInterval) + f1(2);
            xc = [x_region3,xc];
            yc = [y_region3,yc];
            % WE WILL NEED THE CIRCLE INTERSECT POINT HERE
        end
        
        if sweepDistance < norm_vec_TI_f2
            % RIGHT
            vec_pTI_to_pIntersec2 = pI2 - pTI;
            angle_vec_pTI_to_pIntersec2 = anti_clockwise_angle_from_x_of_vector(vec_pTI_to_pIntersec2);
            
            angleStart = angle_vec_TI_F2;
            angleEnd = angle_vec_pTI_to_pIntersec2;
            if angleStart > angleEnd
                angleEnd = angleEnd + 2*pi;
            end
            angleInterval = angleStart:STEP:angleEnd;
            x_regionBroken2 = t*vTnorm .* cos(angleInterval) + pTI(1);
            y_regionBroken2 = t*vTnorm .* sin(angleInterval) + pTI(2);
            xc = [xc, x_regionBroken2];
            yc = [yc, y_regionBroken2];
        else
            if isempty(cIpoints)
                angleStart = angle_vec_TI_F2;
                angleEnd = angle_vec_f2_f1;
            else
                cI2 = cIpoints(:, 2);
                angleStart = angle_vec_TI_F2;
                angleEnd = anti_clockwise_angle_from_x_of_vector(cI2 - f2);
            end
            if angleStart > angleEnd
                angleEnd = angleEnd + 2*pi;
            end
            angleInterval = angleStart:STEP:angleEnd;
            x_region2 = (t-f2_t)*vTnorm .* cos(angleInterval) + f2(1);
            y_region2 = (t-f2_t)*vTnorm .* sin(angleInterval) + f2(2);
            xc = [xc, x_region2];
            yc = [yc, y_region2];
            % WE WILL NEED THE CIRCLE INTERSECT POINT HERE
        end

        if DEBUG
            plot_with_vectors(pI1, '*k');
            plot_with_vectors(pI2, '*k');
            textArroundPoint(pI1, [0;-0.5], '$p_{I1}$');
            textArroundPoint(pI2, [0;-0.5], '$p_{I2}$');
            plot(x_regionBroken1, y_regionBroken1, 'r');
            plot(x_regionBroken2, y_regionBroken2, 'r');
            plot_with_vectors([x_regionBroken2(1); y_regionBroken2(1)], '*r');
            textArroundPoint([x_regionBroken2(1); y_regionBroken2(1)], [0;-0.5], '$S_{tart}$');
            if ~isempty(cIpoints)
                plot_with_vectors(cIpoints, '*k');
                textArroundPoint(cI1, [0; -1], '$c_{I1}$');
                textArroundPoint(cI2, [0; -1], '$c_{I2}$');
            end
        end
        
        angleStart = anti_clockwise_angle_from_x_of_vector(f2 - pTI);
        angleEnd = anti_clockwise_angle_from_x_of_vector(f2 - f1);
        if angleStart > angleEnd
            angleEnd = angleEnd + 2*pi;
        end
        angleInterval = angleStart:STEP:angleEnd;
    end
end

















