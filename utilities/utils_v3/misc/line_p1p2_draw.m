% v1, v2 ... vertices of the line, should be column vectors
function line_p1p2_draw(v1, v2, decor)
    plot([v1(1), v2(1)], [v1(2), v2(2)], decor);
end

