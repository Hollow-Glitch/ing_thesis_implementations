function out = param_circle(xc, yc, r, theta)
    x = xc + r .* cos(theta);
    y = yc + r .* sin(theta);
    
    out = [
        x;
        y
        ];
end
