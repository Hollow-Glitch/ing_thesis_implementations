function out = arc2(xc, yc, r, angle_start, angle_end, angle_step)
    if angle_end < angle_start && angle_end == 0
        angle_end = 2.*pi;
    end
    
    % initialisation
    x = [];
    y = [];
    
    if angle_end < angle_start
        current_angle_end = 2*pi;
        for angle = angle_start : angle_step : current_angle_end
            x = [x, xc + r .* cos(angle)];
            y = [y, yc + r .* sin(angle)];
        end
        
        x_pot_last = xc + r .* cos(current_angle_end); % potential last x
        y_pot_last = yc + r .* sin(current_angle_end); % potential last y
        if x_pot_last ~= x(end) || y_pot_last ~= y(end)
            x = [x, x_pot_last];
            y = [y, y_pot_last];
        end
        
        angle_start = 0;
    end

    for angle = angle_start : angle_step : angle_end
        x = [x, xc + r .* cos(angle)];
        y = [y, yc + r .* sin(angle)];
    end
    
    % due to the step, sometimes we loose the last point, this ensures it
    x_pot_last = xc + r .* cos(angle_end); % potential last x
    y_pot_last = yc + r .* sin(angle_end); % potential last y
    if x_pot_last ~= x(end) || y_pot_last ~= y(end)
        x = [x, x_pot_last];
        y = [y, y_pot_last];
    end
    
    out = [
        x;
        y
        ];
end
