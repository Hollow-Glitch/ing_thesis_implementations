% get end coordinates of a line that starts from `p1`, 
% goes through `p2` and has length `length`
function [x,y] = line_p1p2l(p1, p2, length)
    angle = anti_clockwise_angle_from_x_of_vector(p2-p1);
    [x, y] = line_p(p1, angle, length);
end
