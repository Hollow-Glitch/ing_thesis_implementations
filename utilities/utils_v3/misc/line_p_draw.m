% draw a line that starts from `p`, goes in the direction given with
% `angle` and has length `length`, and line decoration is given with
% `decor`
function line_p_draw(p, angle, length, decor)
    [x,y] = line_p(p, angle, length);
    plot([p(1), x],[p(2), y],decor);
end
