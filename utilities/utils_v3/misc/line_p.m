% get end coordinates of a line that starts from `p`, goes in the
% direction given with `angle` and has length `length`
function [x, y] = line_p(p, angle, length)
    x = length .* cos(angle) + p(1);
    y = length .* sin(angle) + p(2);
end
