function plot_with_vectors(vec, decor)
    plot(vec(1,:), vec(2,:), decor);
end
