function stepped_col_circle(centerX, centerY, radius, edge_color, theta_step)
    theta = 0 : theta_step : 2*pi;
    out = param_circle(centerX, centerY, radius, theta);
    x = out(1,:);
    y = out(2,:);
    x = [x, x(1)];
    y = [y, y(1)];
%     plot(x,y, 'o'); % for testing
    plot(x,y, edge_color);
end
