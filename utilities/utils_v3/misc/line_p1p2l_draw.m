% draw line that starts from `p1`,goes through `p2`, has length `length`
% and decoration is given with `decor`
function line_p1p2l_draw(p1, p2, length, decor)
    [x,y] = line_p1p2l(p1,p2,length);
    plot([p1(1), x],[p1(2), y],decor);
end
