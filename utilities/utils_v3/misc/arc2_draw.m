function arc2_draw(xc, yc, r, angle_start, angle_end, angle_step, decor)
    % arc2_draw(xc, yc, r, angle_start, angle_end, angle_step, decor)
    % produces a COUNTER-CLOCKWISE arc
    arguments
        xc          (1,1) {mustBeReal(xc)}
        yc          (1,1) {mustBeReal(yc)}
        r           (1,1) {mustBeReal(r)}
        angle_start (1,1) {mustBeReal(angle_start)}
        angle_end   (1,1) {mustBeReal(angle_end)}
        angle_step  (1,1) {mustBeReal(angle_step)}
        decor.decor (1,:) {mustBeText(decor.decor)} = 'k-'
    end

    arc_out = arc2(xc, yc, r, angle_start, angle_end, angle_step);
    arc_x = arc_out(1,:);
    arc_y = arc_out(2,:);
    plot(arc_x, arc_y, decor.decor);
end
