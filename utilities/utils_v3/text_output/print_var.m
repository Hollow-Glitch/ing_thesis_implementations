% function print_var(input, decimalPlaces, precision, addType)
function print_var(input, nv)
    arguments
        input            (1,1)
        nv.decimalPlaces (1,1) {mustBeInteger(nv.decimalPlaces)} = 0
        nv.precision     (1,1) {mustBeInteger(nv.precision)}     = 4
        nv.addType       (1,1) = true
    end
    
    varName = inputname(1);
%     my_string = strcat(             ...
%         string(varName), ' = %',    ...
%         string(decimalPlaces),'.',  ...
%         string(precision), 'f\n'    ...
%         );

%     my_string  % debug
    if nv.addType
        type_string = strcat("    ", string(class(input)));
    else
        type_string = "";
    end
    if isa(input, 'double')
        my_string = strcat(             ...
            string(varName), ' = %',    ...
            string(nv.decimalPlaces),'.',  ...
            string(nv.precision), 'f',     ...
            type_string, '\n'  ...
            );
        fprintf(my_string, input);
    elseif isa(input, 'logical')
        my_string = strcat(             ...
            string(varName), ' = %s',   ...
            type_string, '\n'  ...
        );
        fprintf(my_string, string(input));
    else
        error("UNHANDLED TYPE");
    end
end

