function set_square_limits_at(point, width_height)
    x = point(1);
    y = point(2);
    xlim([x - width_height/2, x + width_height/2]);
    ylim([y - width_height/2, y + width_height/2]);
end

