function textArroundPoint(p, vec, varargin)
    center = p + vec;
    text(center(1), center(2), varargin, 'Interpreter', 'latex', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle');
end