function print_current_fig(fileName, opt)
    arguments
        fileName       {mustBeText}
        opt.outputType {mustBeMember(opt.outputType, {'pdf', 'png'})} = 'png'
    end
    % print_current_fig(fileName, opt)
    % 
    % The main point of this function that it automatically saves to
    % the image folder located at the root of the project.
    % 
    % arguments
    %     fileName       {mustBeText}
    %     opt.outputType {mustBeMember(opt.outputType, {'pdf', 'png'})} = 'png'
    % end

    
    fig = ancestor(gcf, 'figure');
    if ~isa(fig, 'matlab.ui.Figure')
        error("'go_or_fig' doesn't have an ancestor of type 'matlab.ui.Figure'");
    end
    % - I don't know what this does, or why it's needed, but it was in
    %   the ancient version, so....
    
    proj = currentProject();
    root_proj_dir = proj.RootFolder;
    imageDirectory = '/images/';

    % partialOutputPath = strcat(root_proj_dir, imageDirectory, fileName);
    % % - partial because the file extension, is added automatically
    % fprintf("Saving figure to file with name: %s...\n", partialOutputPath);
    % outputTypeMapped = mapSimplifiedParamToPrintParam(opt.outputType);
    % print(fig, partialOutputPath, outputTypeMapped,'-r300');

    % After some time, automatically adding the extension stopped working,
    % hence we have modified it so that we add the extension manually.
    outputPath = strcat(root_proj_dir, imageDirectory, fileName, '.', opt.outputType);
    fprintf("Saving figure to file with name: %s...\n", outputPath);
    outputTypeMapped = mapSimplifiedParamToPrintParam(opt.outputType);
    print(fig, outputPath, outputTypeMapped,'-r300');
end

function out = mapSimplifiedParamToPrintParam(outputType)
    if      all(outputType == 'png')
        out = '-dpng';
    elseif  all(outputType == 'pdf')
        out = '-dpdf';
    end
end