function used_handles = set_fig_style_min_squareish(opt)
    arguments
        opt.width_multiplier  (1,1) {mustBeNumeric} = 1
        opt.height_multiplier (1,1) {mustBeNumeric} = 1
    end
    % used_handles = set_fig_style_min_squareish(opt)
    %
    % arguments
    %     opt.width_multiplier  (1,1) {mustBeNumeric} = 1
    %     opt.height_multiplier (1,1) {mustBeNumeric} = 1
    % end
    % 
    % returns:
    %   used_handles = {
    %       this_fig;
    %       this_axes;
    %       this_lines };
    
    % ------------------------------------------------------------
    
    width_multiplier = opt.width_multiplier;
    height_multiplier = opt.height_multiplier;

    % ------------------------------------------------------------
    
    % pixels per centimeter
    % - matlab has a fixe 96 pixels per inch which after inch -> cm
    %   means: 37.795pixels per cm
    px_per_cm = 37.795;

    % extracting from the object tree the needed references
    go_or_fig = gcf;
    this_fig = ancestor(go_or_fig, 'figure');
    this_axes = this_fig.CurrentAxes;
    this_axes_children = this_axes.Children;
    
    % fig settings
    % - printing related settings
    this_fig.PaperUnits = 'centimeters';
    this_fig.PaperSize = [
        10 * width_multiplier
        7.5 * height_multiplier
    ];
    this_fig.PaperPositionMode = 'manual';
    this_fig.PaperPosition = [0 0 this_fig.PaperSize];
    % - monitor related settings
%     this_fig.Position = [0 0 this_fig.PaperSize .* px_per_cm];
    this_fig.Units = 'centimeters';
    this_fig.Position = [0 0 this_fig.PaperSize];
    set(gca,'LooseInset',get(gca,'TightInset'));  % ! removes unnecesarry figure whitespace
    
    % axes settings
    this_axes.LineWidth = 0.75;
    this_axes.FontSize = 11;
    
    movegui(this_fig, 'center');
    
    used_handles = {
        this_fig;
        this_axes;
        this_axes_children };
end