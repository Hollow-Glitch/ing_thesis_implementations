function points = circleLineIntersections(pC, r, f1, f2)
    arguments
        pC (2,1) {mustBeReal}
        r  (1,1) {mustBeReal, mustBePositive}
        f1 (2,1) {mustBeReal}
        f2 (2,1) {mustBeReal}
    end
    vec_f1_f2 = f2 - f1;
    unit_f1_f2 = (1/norm(vec_f1_f2)) * (vec_f1_f2);
    vec_f1_pC = pC - f1;
    dist_shadow = dot(vec_f1_pC, unit_f1_f2);
    vec_shadow = dist_shadow * unit_f1_f2;
    point_shadow = f1 + vec_shadow;
    distanceFromLine = norm(point_shadow - pC);
    
    if distanceFromLine == r
        points = point_shadow;
    elseif distanceFromLine < r
        % pI ... intersection point
        % pS ... shadow point
        dist_pI_from_pS = sqrt(r^2 - distanceFromLine^2);
        p1 = (dist_shadow - dist_pI_from_pS) * unit_f1_f2 + f1;
        p2 = (dist_shadow + dist_pI_from_pS) * unit_f1_f2 + f1;
        points = [p1, p2];
    else
        points = [];
    end
end


