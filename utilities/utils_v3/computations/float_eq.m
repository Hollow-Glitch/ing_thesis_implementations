function out = float_eq(a, b, epsilon)
    if abs(a - b) < epsilon
        out = true;
    else
        out = false;
    end
end