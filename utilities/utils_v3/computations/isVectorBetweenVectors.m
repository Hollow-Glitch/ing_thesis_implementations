function out = isVectorBetweenVectors(vecBetween, vecFirst, vecSec)
%     a1 = anti_clock_angle_between_vectors(vecFirst, vecBetween);
%     a2 = anti_clock_angle_between_vectors(vecBetween, vecSec);
% a1
% a2
%     if a1 > 0 && a2 > 0
%         out = true;
%     else
%         out = false;
%     end

    a1 = anti_clockwise_angle_from_x_of_vector(vecFirst);
    a2 = anti_clockwise_angle_from_x_of_vector(vecBetween);
    a3 = anti_clockwise_angle_from_x_of_vector(vecSec);
    if a1 < a3
        if a1 < a2 && a2 < a3
            out = true;
        else
            out = false;
        end
    else
        out = isVectorBetweenVectors(-vecBetween, vecSec, vecFirst);
    end
end

