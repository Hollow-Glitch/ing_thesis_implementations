function angle = angleShortestBetweenVectors(a,b)
%     angle = atan2(norm(cross(a,b)), dot(a,b));

    v1 = a;
    v2 = b;
%     angle = acos(dot(v1, v2) / (norm(v1) * norm(v2)));
    
    v = a;
    u = b;
    vx = v(1); vy= v(2); ux = u(1); uy = u(2);
    va = -atan2d(vy,vx);         % angle of v relative to x-axis (clockwise = +ve)
    ua = -atan2d(uy,ux);         % angle of u relative to x-axis (clockwise = +ve)
    A = va - ua;                             % angle va relative to ua
    A = A - 360*(A > 180) + 360*(A < -180);   % correction put in [-180,180]
    angle = deg2rad(A);
end

