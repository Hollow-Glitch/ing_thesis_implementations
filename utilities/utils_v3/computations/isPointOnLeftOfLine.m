function out = isPointOnLeftOfLine(point, lineStart, lineEnd)
    % out = isPointOnLeftOfLine(point, lineStart, lineEnd)
    % 
    % Checks whether is point on the left side of the line.
    % Uses the cross product, with the trick for using it on 
    % vectors, which are 2D.
    arguments
        point      (2,1) {mustBeReal(point)}
        lineStart  (2,1) {mustBeReal(lineStart)}
        lineEnd    (2,1) {mustBeReal(lineEnd)}
    end
    orientation = getPointLineRelPos(point, lineStart, lineEnd);

    if sign(orientation) > 0
        out = true;
    else
        out = false;
    end
end

