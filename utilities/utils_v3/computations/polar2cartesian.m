function cart_vec = polar2cartesian(polar_vec)
    arguments
        polar_vec  (2,1)
    end
    % cart_vec = polar2cartesian(polar_vec)
    %
    % polar_vec == [rho; theta]  
    % where  rho is the radius and theta is the angle
    %
    % cart_vec == [x; y]

    rho = polar_vec(1);
    theta = polar_vec(2);
    x = rho * cos(theta);
    y = rho * sin(theta);
    cart_vec = [x; y];
end