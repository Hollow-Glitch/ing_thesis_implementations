function shadow_point = getShadowPointOfPointOnLine(startPoint, endPoint, pointVec)
    arguments
        startPoint  (2,1)
        endPoint    (2,1)
        pointVec    (2,1)
    end
    lineVec = endPoint - startPoint;    
    lineVecUnit = (1/norm(lineVec)) * lineVec;
    caster = pointVec - startPoint;
    
    shadow_distance = dot(caster, lineVecUnit);
%     shadow_vec = dot(caster, lineVecUnit) * lineVecUnit;
    shadow_vec = shadow_distance * lineVecUnit;
    shadow_point = shadow_vec + startPoint;
end