function angle = anti_clock_angle_between_vectors(vec1, vec2)
    a1 = anti_clockwise_angle_from_x_of_vector(vec1);
    a2 = anti_clockwise_angle_from_x_of_vector(vec2);
    if a2 < a1
        angle = 2*pi - a1 + a2;
    else
        angle = a2 - a1;
    end
    
    if angle > 2*pi
        angle = angle - 2*pi;
    end
end

