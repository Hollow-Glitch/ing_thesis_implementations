function uvec = get_directional_uvec(angle)
    x = cos(angle);
    y = sin(angle);
    uvec = [x;y];
end
