function [x_out, y_out] = rotate_points(x_in, y_in, angle)
    % x_in ... array of x coordinates
    % y_in ... array of y coordinates
    % x_out ... array of rotated x coordinates
    % y_out ... array of rotated y coordinates
    R = [
        cos(angle), -sin(angle);
        sin(angle),  cos(angle)
    ];
    vec = [
        x_in;
        y_in
    ];
    rotated_vec = R * vec;
    x_out = rotated_vec(1,:);
    y_out = rotated_vec(2,:);
end
