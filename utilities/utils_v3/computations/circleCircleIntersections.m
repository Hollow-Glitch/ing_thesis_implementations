function points = circleCircleIntersections(A, rA, B, rB)
    distance = norm(A - B);
    if distance > rA + rB
        points = [];
%     elseif distance == rA + rB
%         disp("Executed");
%         points = rA*((B - A)/distance);
% % Doesn't work!
% % This case is now handled with the if statement at the end.
    elseif distance < abs(rA - rB)
        points = [];
    elseif (distance == 0) && (rA == rB)
        error("Coincident and infite points");
    else
%         https://stackoverflow.com/questions/3349125/circle-circle-intersection-points
        d = distance;
        a = (d^2 + rA^2 - rB^2)/(2*d);
        h = sqrt(rA^2 - a^2);
%         h*((A-B)/d);
        P2 = A + a*( B  - A) / d;
        x2 = P2(1);
        y2 = P2(2);
        
        x31 = x2 + h*(B(2)-A(2))/d;
        y31 = y2 - h*(B(1) - A(1)) / d;
        x32 = x2 - h*(B(2)-A(2))/d;
        y32 = y2 + h*(B(1) - A(1)) / d;
        p1 = [x31;y31];
        p2 = [x32;y32];
        points = [p1,p2];
    end

    if size(points, 2) > 1
        if eq_vecs(points(:,1), points(:,2))
            points = points(:,1);
        end
    end
end

function out = eq_vecs(vec1, vec2)
    tolerance = 0.000000001;
    vec = abs(vec1 - vec2);
    if  vec(1) < tolerance && vec(2) < tolerance
        out = true;
    else
        out = false;
    end
end

