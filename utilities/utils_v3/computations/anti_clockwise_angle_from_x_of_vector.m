function angle = anti_clockwise_angle_from_x_of_vector(vec)
    function angle = vec_angle_from_x(vec)
        x_unit = [1 0];
        angle = acos(...
                dot(vec, x_unit) / (norm(vec) * norm(x_unit) )...
            );
    end

    if vec(2) < 0
        angle = 2*pi - vec_angle_from_x(vec);
    else
        angle = vec_angle_from_x(vec);
    end
end
