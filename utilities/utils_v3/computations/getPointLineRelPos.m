function out = getPointLineRelPos(point, lineStart, lineEnd)
    % out = getPointLineOrientation(point, lineStart, lineEnd)
    % 
    % Checks whether is point on the left side of the line.
    % Uses the cross product, with the trick for using it on 
    % vectors, which are 2D.
    %
    % This is for:
    % we need to know on which side of the obstacle the player is
    % hence in order to use the cross product to determine this
    % we first need to determine the sequence of our points by which
    % we determine the orientation of the area of the triangle
    % they are describing, let this seq. be: f1 -> f2 -> pEI
    % u = f2 - f1
    % w = pEI - f2
    % | i    j    k|
    % | u_x  w_x  0|
    % | u_y  w_y  0|
    % k(u_x*w_y - w_x*u_y) = signedDoubleArea
    % we drop the unit vector k since we only need a scalar
    % if signedArea is <0 then we are on one side, if >0 on the other
    % and if =0 then pEI is just a multiple of the vector (f2-f1)
    % i.e. lies on the same line.
    % The reason for the name "signedDoubleArea" is that actually
    % determinant determines the area of the paralelogram with sides
    % u, w, and hence in order to get the area of the triangle, we would
    % need to divide it by two, i.e. signedArea = signedDoubleArea / 2;
    % area >0 ... we are on the left  side
    % area =0 ... we are on the same  line
    % area <0 ... we are on the right side
    arguments
        point      (2,1) {mustBeReal(point)}
        lineStart  (2,1) {mustBeReal(lineStart)}
        lineEnd    (2,1) {mustBeReal(lineEnd)}
    end
    p = lineStart - point;
    v = lineEnd - lineStart;

    out = p(1)*v(2) - v(1)*p(2);
end