function distance = getPointLineDistance(startPoint, endPoint, pointVec)
    arguments
        startPoint  (2,1)
        endPoint    (2,1)
        pointVec    (2,1)
    end
    lineVec = endPoint - startPoint;    
    lineVecUnit = (1/norm(lineVec)) * lineVec;
    caster = pointVec - startPoint;
    
    shadow_vec = dot(caster, lineVecUnit) * lineVecUnit;
    shadow_point = shadow_vec + startPoint;

    distance = norm(pointVec - shadow_point);
end

