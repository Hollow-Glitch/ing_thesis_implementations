function uvec = uvec(vec)
    % uvec = uvec(vec)
    %
    % Calculates the unit vector of `vec`.
    uvec = vec / norm(vec);
end