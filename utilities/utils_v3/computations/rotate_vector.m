function vec_out = rotate_vector(vec, angle)
    [x_out, y_out] = rotate_points(vec(1,:), vec(2,:), angle);
    vec_out = [
        x_out;
        y_out
    ];
end
