function mustHaveSameRowCount(a,b)
    % Test for equal size
    if ~isequal(size(a,1),size(b,1))
        eid = 'Size:notEqual';
        msg = 'RowCount of first input must equal RowCount of second input.';
        throwAsCaller(MException(eid,msg))
    end
end