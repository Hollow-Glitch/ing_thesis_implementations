function mustBeStructWith(str, fieldnames, mayNotIncludeThese)
    % mustBeStructWith(str, fieldnames, mayNotIncludeThese)
    % 
    % Example usage:
    % arguments
    %     ...
    %     line_config {mustBeStructWith(line_config, {'xL', 'yL', 'angleL'}, {'t'})}
    %     circle_config {mustBeStructWith(circle_config, {'xC', 'yC', 'vNorm'}, {'t'})}
    %     ...
    % end

    if ~(isstruct(str) && all(isfield(str, fieldnames)) && (~all(isfield(str, mayNotIncludeThese))))
        eidType = 'mustBeRealUpperTriangular:notRealUpperTriangular';
        
        field_names_as_char = [];
        for cell = fieldnames
            field_names_as_char = [field_names_as_char, char(cell),', '];
        end
        
        mayNotIncludeThese_as_char = [];
        for cell = mayNotIncludeThese
            mayNotIncludeThese_as_char = [mayNotIncludeThese_as_char, char(cell),', '];
        end

        msgType = ['\nStruct must contain the following fields: ', field_names_as_char, '; \nand may not contain the following fields: ', mayNotIncludeThese_as_char, ';'];
        throwAsCaller(MException(eidType,msgType))
    end
end