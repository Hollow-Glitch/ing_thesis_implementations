function mustBeEmptyOrHaveSameRowCount(a, b)
    % Test for equal size
    if isempty(a) || isempty(b)
        return;
    end
    
    mustHaveSameRowCount(a,b);
end
