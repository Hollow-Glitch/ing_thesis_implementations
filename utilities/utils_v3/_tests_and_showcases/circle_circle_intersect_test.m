function circle_circle_intersect_test()
%     test1();
%     test2();
%     test3();
%     test4();
    test5();
end


function test1()
    A = [1;1];
    B = [2;3];
    rA = 2;
    rB = 1;
    hold on;
    axis equal;
    xline(0);
    yline(0);
    circle(A(1), A(2), rA);
    circle(B(1), B(2), rB);
    points = circleCircleIntersections(A, rA, B, rB);
    plot_with_vectors(points, 'k*');
end

function test2()
    A = [1;1];
    B = [1;3];
    rA = 2;
    rB = 1;
    hold on;
    axis equal;
    xline(0);
    yline(0);
    circle(A(1), A(2), rA);
    circle(B(1), B(2), rB);
    points = circleCircleIntersections(A, rA, B, rB);
    plot_with_vectors(points, 'k*');
end

function test3()
    A = [1;3];
    B = [2;3];
    rA = 2;
    rB = 1;
    hold on;
    axis equal;
    xline(0);
    yline(0);
    circle(A(1), A(2), rA);
    circle(B(1), B(2), rB);
    points = circleCircleIntersections(A, rA, B, rB);
    plot_with_vectors(points, 'k*');
end

function test4()
    A = [2;3];
    B = [2;3];
    rA = 2;
    rB = 1;
    hold on;
    axis equal;
    xline(0);
    yline(0);
    circle(A(1), A(2), rA);
    circle(B(1), B(2), rB);
    points = circleCircleIntersections(A, rA, B, rB);
    plot_with_vectors(points, 'k*');
end

function test5()
    A = [4.721359549995794; 14.530850560107217];
    rA = 8.980559531591709;
    B =[15.278640450004206; -0.000000000000004];
    rB = 8.980559531591709;
    hold on;
    axis equal;
    xline(0);
    yline(0);
    circle(A(1), A(2), rA);
    circle(B(1), B(2), rB);
    points = circleCircleIntersections(A, rA, B, rB);
    points
    plot_with_vectors(points, 'k*');
end