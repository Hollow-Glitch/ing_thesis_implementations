function polyxpoly_test()
    p1 = [1;1];
    r1 = 1;
    
    p2 = [2;3];
    r2 = 2;
    
    step = 0.1;
    [x1,y1] = getCirclePoints(p1, r1, step);
    [x2,y2] = getCirclePoints(p2, r2, step);
    [xs, ys] = getSinusPoints(0:0.01:2*pi);
    [xi, yi] = polyxpoly(x1, y1, x2, y2);
    [xsi, ysi] = polyxpoly(x1, y1, xs, ys);
    
    hold on;
    axis equal;
    plot(x1, y1, '-r');
    plot(x2, y2, '-b');
    plot(xi, yi, '*k');
    plot(xs, ys, '-g');
    plot(xsi, ysi, '*k');
end

function [x,y] = getSinusPoints(x)
%     x = x;
    y = sin(x);
end

function [x,y] = getCirclePoints(p, r, step)
    theta = 0:step:2*pi;
    x = r .* cos(theta) + p(1);
    y = r .* sin(theta) + p(2);
    x = [x, x(1)];
    y = [y, y(2)];
end