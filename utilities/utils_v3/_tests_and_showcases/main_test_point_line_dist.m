function main_test_point_line_dist()
    startPoint = [1;1];
    endPoint = [3;2];
    pointVec = [-1;1];
    
    % implementation of that function
    lineVec = endPoint - startPoint;    
    lineVecUnit = (1/norm(lineVec)) * lineVec;
    caster = pointVec - startPoint;
    
    shadow_distance = dot(caster, lineVecUnit)
    shadow_vec = shadow_distance * lineVecUnit;
    shadow_point = shadow_vec + startPoint;

    distance = norm(pointVec - shadow_point);

    
    
    hold on;
    axis equal;
    plot_with_vectors([startPoint, endPoint, pointVec, shadow_point], 'kx');
    plot_with_vectors([startPoint, shadow_point], '-k');
    plot_with_vectors([shadow_point, pointVec], '-b');
    textArroundPoint(startPoint, [0; -0.1], 's');
    textArroundPoint(endPoint, [0; -0.1], 'e');
    textArroundPoint(pointVec, [0; -0.1], 'p');
    textArroundPoint(shadow_point, [0; -0.1], 'h');
end

