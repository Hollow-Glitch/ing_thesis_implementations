function circle_line_intersect_test()
%     test1();
%     test2();
%     test3();  % IF error => HAPPY
    test4();
end

function test1()
    f1 = [1;1];
    f2 = [3;1];
    pC = [2;2];
    r = 1;
    
    points = circleLineIntersections(pC, r, f1, f2);
    points
    
    hold on;
    axis equal;
    plot_with_vectors([f1,f2], '-k');
    plot_with_vectors(pC, 'kx');
    plot_with_vectors(points, 'k*');
    textArroundPoint(f1, [0;-0.1], '$f_1$');
    textArroundPoint(f2, [0;-0.1], '$f_2$');
    textArroundPoint(points(:,1), [0;-0.1], '$p_1$');
    circle(pC(1), pC(2), r);
end

function test2()
    f1 = [1;1];
    f2 = [3;1];
    pC = [2;2];
    r = 1.2;
    
    points = circleLineIntersections(pC, r, f1, f2);
    points
    
    hold on;
    axis equal;
    plot_with_vectors([f1,f2], '-k');
    plot_with_vectors(pC, 'kx');
    plot_with_vectors(points, 'k*');
    textArroundPoint(f1, [0;-0.1], '$f_1$');
    textArroundPoint(f2, [0;-0.1], '$f_2$');
    textArroundPoint(points(:,1), [0;-0.1], '$p_1$');
    textArroundPoint(points(:,2), [0;-0.1], '$p_2$');
    circle(pC(1), pC(2), r);
end

function test3()
    f1 = [1;1];
    f2 = [3;1];
    pC = [2;2];
    r = 0.8;
    
    points = circleLineIntersections(pC, r, f1, f2);
    points
    
    hold on;
    axis equal;
    plot_with_vectors([f1,f2], '-k');
    plot_with_vectors(pC, 'kx');
    plot_with_vectors(points, 'k*');
    textArroundPoint(f1, [0;-0.1], '$f_1$');
    textArroundPoint(f2, [0;-0.1], '$f_2$');
    circle(pC(1), pC(2), r);
end

function test4()
    f1 = [0;10];
    f2 = [10;0];
    
    xTI = 2;
    yTI = 0;
    pC = [xTI; yTI];
    r = 1*6;
    
    points = circleLineIntersections(pC, r, f1, f2);
    points
    
    circle(pC(1), pC(2), r);
    hold on;
    axis equal;
    plot_with_vectors([f1,f2], '-k');
    plot_with_vectors(pC, 'kx');
    plot_with_vectors(points, 'k*');
    textArroundPoint(f1, [0;-0.5], '$f_1$');
    textArroundPoint(f2, [0;-0.5], '$f_2$');
    textArroundPoint(points(:,1), [0;-0.5], '$p_1$');
    textArroundPoint(points(:,2), [0;0.5], '$p_2$');
    circle(pC(1), pC(2), r);

end