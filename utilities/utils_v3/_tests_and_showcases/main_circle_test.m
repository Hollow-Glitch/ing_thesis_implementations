function main_circle_test()
    axis equal;
    hold on;
    xline(0);
    yline(0);
    xline(1);
    xline(-1);
    yline(1);
    yline(-1);
    circle(0,0,1);
end
