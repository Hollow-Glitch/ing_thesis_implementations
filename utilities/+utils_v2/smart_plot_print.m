function smart_plot_print(go_or_fig, fn, opt)
    arguments
        go_or_fig
        fn
        opt.outputType {mustBeMember(opt.outputType, {'.pdf', '.png'})}
    end
% smart_plot_print(fig, fn)
% 
% fig        ...  figure to be saved
% fn         ...  filename to be used
% outputType ...  '.pdf', '.png', ...
%
% this function saves the 'fig' figure to the current projects
% image directory, i.e. root/images, in pdf format
    
    fig = ancestor(go_or_fig, 'figure');

    if ~ischar(fn)
        error("'fn' must be of type char array");
    end
    if ~isa(fig, 'matlab.ui.Figure')
        error("'go_or_fig' doesn't have an ancestor of type 'matlab.ui.Figure'");
    end
    
    proj = currentProject();
    
    root_proj_dir = proj.RootFolder;
    lid = '/images/'; % [l]ocal [i]mage [d]irectory
%     op = strcat(root_proj_dir, lid, fn, '.pdf'); % [o]utput [p]ath
    op = strcat(root_proj_dir, lid, fn, opt.outputType); % [o]utput [p]ath
    
    fprintf("Saving figure: \n    %s\n", op);
    
    print(fig, op,'-dpdf','-r300');
end

