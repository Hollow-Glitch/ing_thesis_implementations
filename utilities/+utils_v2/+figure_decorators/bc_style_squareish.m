function used_handles = bc_style_squareish(go_or_fig, varargin)
% used_handles = bc_style_squareish(go_or_fig)
%
% mandatory arguments:
%   go_or_fig  ...  graphics_object or figure
%
% optional arguments:
% ... apart from setting the width_multiplier to 2 some times
%     these optional arguments should not be utilised
%   width_multiplier  ...  type: numeric, scalar; default: 1
%                     ...  if you want a rectangular image, with twice the
%                          width of the default, then set only this to 2
%   height_multiplier ...  type: numeric, scalar; default: 1
% 
% returns:
%   used_handles = {
%       this_fig;
%       this_axes;
%       this_lines };

    p = inputParser();
    p.addOptional('width_multiplier', 1, @(x) isnumeric(x) && isscalar(x) );
    p.addOptional('height_multiplier', 1, @(x) isnumeric(x) && isscalar(x) );
    p.addOptional('line_width', 1.5, @(x) isnumeric(x) && isscalar(x) );
    p.parse(varargin{:});
    args = p.Results;
    width_multiplier = args.width_multiplier;
    height_multiplier = args.height_multiplier;
    line_width = args.line_width;

    % pixels per centimeter
    % - matlab has a fixe 96 pixels per inch which after inch -> cm
    %   means: 37.795pixels per cm
    px_per_cm = 37.795;

    % extracting from the object tree the needed references
    this_fig = ancestor(go_or_fig, 'figure');
    this_axes = this_fig.CurrentAxes;
    this_axes_children = this_axes.Children;
    
    % fig settings
    % - printing related settings
    this_fig.PaperUnits = 'centimeters';
    this_fig.PaperSize = [
        10 * width_multiplier
        7.5 * height_multiplier
    ];
    this_fig.PaperPositionMode = 'manual';
    this_fig.PaperPosition = [0 0 this_fig.PaperSize];
    % - monitor related settings
%     this_fig.Position = [0 0 this_fig.PaperSize .* px_per_cm];
    this_fig.Units = 'centimeters';
    this_fig.Position = [0 0 this_fig.PaperSize];
    set(gca,'LooseInset',get(gca,'TightInset'));  % ! removes unnecesarry figure whitespace
    
    % axes settings
    this_axes.LineWidth = 0.75;
    this_axes.FontSize = 11;
    
    % lines settings
    for ith_child = 1:length(this_axes_children)
        go = this_axes_children(ith_child);  % go - graphics object
        
        if isa(go, 'matlab.graphics.chart.primitive.Quiver')
            go.LineWidth = 0.5;
        end
        if isa(go, 'matlab.graphics.chart.primitive.Line')
            this_axes_children(ith_child).LineWidth = line_width;
            this_axes_children(ith_child).MarkerSize = 5;
        end
    end
    
    movegui(this_fig, 'center');
    
    
    used_handles = {
        this_fig;
        this_axes;
        this_axes_children };
end

