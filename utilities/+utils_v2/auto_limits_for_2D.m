function [my_xlim, my_ylim] = auto_limits_for_2D( ...
    every_x,      ...
    every_y,      ...
    x_prop_const, ...
    y_prop_const  ...
    )
%AUTO_LIMITS_FOR_2D
% Inputs
%   every_x, every_y  
%   ... vertical vectors 
%   ... they contain every x and every y coordinate
% 
%   x_prop_const, y_prop_const
%   ... proportionality constants
%   ... they express the proportionality of the plot "scope"
%       enlargement, relative to the delta between the farthest
%       data points on the given axes
% 
% Outputs
%   my_xlim, my_ylim
%   ... horizontal vectors
%   ... intended to be used with xlim, ylim
%   ... example:
%       [my_xlim, my_ylim] = utils_v2.auto_limits_for_2D(every_x, every_y, 0.1, 0.1);
%       xlim(my_xlim);
%       ylim(my_ylim);
%
% Recommendation
%   1. using the function 'cat' create a variable called 'every_x'
%      which will hold every data point for this axis
%   2. perform the same for 'every_y'
%   !. MAKE SURE that your 'every_x' and 'every_y' is a vector and vertical
%   3. call this function

    % every_x = cat(1, pTx, pAx);
    % every_y = cat(1, pTy, pAy);
    min_x = min(every_x);
    max_x = max(every_x);
    min_y = min(every_y);
    max_y = max(every_y);
    xlim_delta = max_x - min_x;
    ylim_delta = max_y - min_y;
    my_xlim_delta = xlim_delta * x_prop_const;
    my_ylim_delta = ylim_delta * y_prop_const;
    
    my_xlim = [min_x - my_xlim_delta, max_x + my_xlim_delta];
    my_ylim = [min_y - my_ylim_delta, max_y + my_ylim_delta];
%     xlim(my_xlim);
%     ylim(my_ylim);
end

