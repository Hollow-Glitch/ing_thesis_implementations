function lineBetweenTwoPoints(A, B)
%LINEBETWEENTWOPOINTS creates a line between A and B, prettier than 'line'
    line([A(1) B(1)], [A(2) B(2)]);
end
