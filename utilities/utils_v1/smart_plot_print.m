function smart_plot_print(fn)
% smart_plot_print creates an image file of the with the name 'fn'
% in the directory images of the current working directory

    %% safe checking
    if ~ischar(fn)
        disp("'fn' must be a string");
        return;
    end

    %% main work
    lid = '/images/'; % [l]ocal [i]mage [d]irectory
    wd = pwd; % current [w]orking [d]irectory
    op = strcat(wd, lid, fn, '.pdf'); % [o]utput [p]ath
    
    % https://www.mathworks.com/help/matlab/creating_plots/save-figure-with-minimal-white-space.html
    fig = gcf;
    fig.PaperPositionMode = 'auto';
    fig_pos = fig.PaperPosition;
    fig.PaperSize = [fig_pos(3) fig_pos(4)];
    print(op,'-dpdf','-r300');
end

