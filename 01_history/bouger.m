function bouger()
    x0 = 5;
    x = 0:0.01:x0;
    vM = 1;
    vP = 2;
    
    n = vM / vP;
    if n == 1
        disp('n=1');
        y = bouger_eq_n_eq_1(x, x0, vM, vP);
    else
        disp('n~=1');
        y = bouger_eq_n_lt_1(x, x0, vM, vP);
    end

%     elseif n<1
%         disp('n<1');
%         y = bouger_eq_n_lt_1(x, x0, vM, vP);
%     else
%         error('the n>1 case is meaningless');
%     end
    
    hold on;
    plot(x, y);
    xlabel('x');
    ylabel('y');
    axis square;
    xline(0);
    yline(0);
    xline(x0, '--');
    plot(x(end-1), y(end-1), 'x');

    %% create figure
    set_fig_style_bc_squareish();
    print_current_fig('bouger');
end


function y = bouger_eq_n_lt_1(x, x0, vM, vP)
    % vM ... merchant velocity
    % vP ... pirate velocity
    n = vM / vP;
%     if ~(n<1)
%         error('for this equation, it must be that n<1')
%     end
    term_1 = (n*x0) / (1-n^2);
    term_2 = (x0 - x) ./ 2;
    term_3 = ( ( 1-x./x0).^n   ) / (1+n);
    term_4 = ( ( 1-x./x0).^(-n)) / (1-n);
    
    y = term_1 + term_2 .* (term_3 - term_4);
end


function y = bouger_eq_n_eq_1(x, x0, vM, vP)
    n = vM / vP;
    if ~(n==1)
        error('for this equation, it must be that n==1')
    end
    y = (1/2)*x0.*(                        ...
        (1/2).*(1-x./x0).^2  -  log(1-x./x0)  ...
    ) - (1/4)*x0;
end



