function tractrix()
    y = 0:0.01:1;
    a = 1;
    x = tractrix_eqn(y, a);
    
    hold on;
    axis square;
    xline(0);
    yline(0);
    xlabel('x');
    ylabel('y');
    plot(x,y);

    %% create figure
    set_fig_style_bc_squareish();
    print_current_fig("tractrix_imagePrintingTest1_bc_squareish");
end


function xW = tractrix_eqn(yW, yWI)
    % % watch
    % yWI ... initial y of the watch
    % xWI ... is implicitly 0
    % yW  ... y position of the watch
    % xW  ... x position of the watch
    %
    % 
    % 
    xW = yWI .* log( (yWI+sqrt(yWI^2 - yW.^2))./(yW) ) - sqrt(yWI^2 - yW.^2);
end

