function sla__continuous_heading_angle_change()

    %% options
    time_step = 1 / 60;

    % Misc. options
    render_initial_DB_circle = true;

    % Figure & video options
    create_figure = false;
    create_video  = false;
    % !!! : figures and video at the same time can't be created

    %%
    base_fig_name = 'straight_line_attack__';
    video_name = base_fig_name;

    predetermined_end_time = 20;
    capture_radius = 1;

    %% Video: create video object
    if create_video
        vidObj = VideoWriter([video_name, '.avi']);
        open(vidObj);
    end
    
    %% scenario setup
    t = 0;
    p_EI = [0;0];
    p_PI = [200;0];
    v_E = 25;
    k = 2;
    v_P = k * v_E;
    angular__v_E = pi/10;

    %% render: setup
    clf;
    hold on;
    axis equal;
    xlabel('x');
    ylabel('y');

    %% render: positions
    plot_with_vectors(p_PI, 'r*');
    plot_with_vectors(p_PI, 'rd');
    plot_with_vectors(p_EI, 'b*');
    plot_with_vectors(p_EI, 'bs');

    %% render: DB-circles
    [center, radius] = DB_circle_center_and_radius(...
        p_PI(1), p_PI(2), p_EI(1), p_EI(2), k);
    col_circle(center(1), center(2), radius, 'k');

    %%  Limits for x,y which will be used for all figures so that the reducing
    %   dominance region of the evader can be seen.
    my_xlim = 1.05 .* xlim;
    my_ylim = 1.05 .* ylim;
    xlim(my_xlim);
    ylim(my_ylim);

    %% calculating potential capture point
%     points = circleLineIntersections(p_C(:, 2), r_C, p_EI, p_PI(:, 2));
%     plot_with_vectors(points(:, 1), 'kx');

    %% calculating heading vectors: evader
    uvec__v_E = (p_PI - p_EI) / norm(p_PI - p_EI);
    psi_EI = anti_clockwise_angle_from_x_of_vector(uvec__v_E); % initial heading angle of E

    %% calculating heading vectors: pursuers
    %  strat1
%     uvec__v_P = [];
%     uvec__v_P1 = uvec(p_T - p_PI(:, 1));

    %% Figure: create initial state figure
    if create_figure
        clf
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");
        xlim(my_xlim);
        ylim(my_ylim);

        uvec__v_E = get_directional_uvec(psi_EI);
        p_E = p_EI + v_E * time_step * uvec__v_E;
        col_circle(center(1), center(2), radius, 'k');
        points = circleLineIntersections(center, radius, p_EI, p_E);
        p_T = points(:, 2); % target point
        uvec__v_P = (p_T - p_PI) / norm(p_PI - p_T);

        plot_with_vectors(p_PI, 'r*');
        plot_with_vectors(p_PI, 'rd');
        plot_with_vectors(p_EI, 'b*');
        plot_with_vectors(p_EI, 'bs');
        plot_with_vectors(p_T , 'k*');

        % render: heading lines
        line_p1p2l_draw(p_PI, p_PI + uvec__v_P, norm(p_PI - p_T), 'k--');
        line_p1p2l_draw(p_EI, p_EI + uvec__v_E, norm(p_EI - p_T), 'k--');

        fig_name = sprintf([base_fig_name, 'initial_state_t=%.2f'], t);
        set_fig_style_bc_squareish();
        % set_fig_style_min_squareish();
        print_current_fig(fig_name, "outputType","pdf");
    end

    %% path collections
    col__p_P = [];
    col__p_E = [];

    %% game loop
    p_P = p_PI;
    p_E = p_EI;
    psi_E = psi_EI;

    t = 0;
    isPlaying = true;
    while isPlaying
        loop_start_time = tic;

        t = t + time_step;

        % update evader
        psi_E = psi_E + angular__v_E * time_step;

        uvec__v_E = get_directional_uvec(psi_E);
%         uvec__v_E = (p_P - p_E) / norm(p_P - p_E); % evader heading vector
        old__p_E = p_E;
        p_E = p_E + v_E * time_step * uvec__v_E;

        [center, radius] = DB_circle_center_and_radius(...
            p_P(1), p_P(2), p_E(1), p_E(2), k);
        points = circleLineIntersections(center, radius, old__p_E, p_E);
        p_T = points(:, 2); % target point

        % update pursuers
%         uvec__v_P = (p_E - p_P) / norm(p_P - p_E);
        uvec__v_P = (p_T - p_P) / norm(p_P - p_T);
        p_P = p_P + v_P * time_step * uvec__v_P;

        % add positions to path collections
        col__p_E = [col__p_E, p_E];
        col__p_P = [col__p_P, p_P];

        % render: setup frame
        clf;
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");
        xlim(my_xlim);
        ylim(my_ylim);

        % render: agent paths
        plot_with_vectors(col__p_E, 'b-');
        plot_with_vectors(col__p_P, 'r-');

        % render: heading lines
        line_p1p2l_draw(p_P, p_P + uvec__v_P, norm(p_P - p_T), 'k--');
        line_p1p2l_draw(p_E, p_E + uvec__v_E, norm(p_E - p_T), 'k--');

        % render: positions
        plot_with_vectors(p_PI, 'r*');
        plot_with_vectors(p_P , 'rd');
        plot_with_vectors(p_EI, 'b*');
        plot_with_vectors(p_E , 'bs');
        plot_with_vectors(p_T , 'k*');

        % render: DB-circles
        col_circle(center(1), center(2), radius, 'k');

        if render_initial_DB_circle
            [center, radius] = DB_circle_center_and_radius(...
                p_PI(1), p_PI(2), p_EI(1), p_EI(2), k);
            col_circle(center(1), center(2), radius, 'k--');
        end

        % !!! DRAW !!!
        elapsed_time = toc(loop_start_time);
%         pause(1/60);
        drawnow;

%         print_var(t);
%         print_var(elapsed_time);
%         psi_E

        % create media
        if create_figure
            fig_time_1 = 0;
            fig_time_2 = 1;
            fig_time_3 = 2;
            fig_epsilon = 0.01;
            if float_eq(t, fig_time_1, fig_epsilon) ...
            || float_eq(t, fig_time_2, fig_epsilon) ...
            || float_eq(t, fig_time_3, fig_epsilon) ...
            || norm(p_P - p_E) < capture_radius
                disp("Creating figure");

                fig_name = sprintf([base_fig_name, 't=%.2f'], t);
                set_fig_style_bc_squareish();
                % set_fig_style_min_squareish();
                print_current_fig(fig_name, "outputType","pdf");
            end
        end
        if create_video
            writeVideo(vidObj, getframe(gcf));
        end

        % termination condition
        if norm(p_P - p_E) < capture_radius
            disp('Collision detected!');
            isPlaying = false;
        end

        % manual interupt
        keyboard_inputs = get(gcf, 'CurrentCharacter');
        if ~isempty(keyboard_inputs) && ismember('q', keyboard_inputs)
            keyboard_inputs
            set(gcf, 'CurrentCharacter', '@');
            disp("You pressed 'q', closing the program.");
            isPlaying = false;
        end

        % predetermined interupt
        if predetermined_end_time < t
            disp('Predetermined interupt!');
            isPlaying = false;
        end
    end

    %% Video: close video object
    if create_video
        close(vidObj);
    end

    %% Figure: create final state figure
    if create_figure
        clf
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");
        xlim([-18.4225, 218.6564]);
        ylim([-74.7272, 112.2592]);

        plot_with_vectors(p_PI, 'r*');
        plot_with_vectors(p_P, 'rd');
        plot_with_vectors(p_EI, 'b*');
        plot_with_vectors(p_E, 'bs');

        plot_with_vectors(col__p_E, 'b-');
        plot_with_vectors(col__p_P, 'r-');

        fig_name = sprintf([base_fig_name, 'final_paths_t=%.2f'], t);
        set_fig_style_bc_squareish();
        % set_fig_style_min_squareish();
        print_current_fig(fig_name, "outputType","pdf");
    end

    % reset in case the simulation ended without manual interuption
    set(gcf, 'CurrentCharacter', '@');
end

