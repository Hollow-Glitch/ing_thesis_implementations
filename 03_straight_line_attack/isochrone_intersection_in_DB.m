function [outputArg1,outputArg2] = isochrone_intersection_in_DB(inputArg1,inputArg2)
    create_video  = true;
    base_name = 'isochrone_intersection_in_DB';
    t_end = 2.5;

    pPI = [-1; 1];
    vP  = 2;

    pEI = [ 1; -0.5];
    vE  = 1;
    
    [center, radius] = DB_circle_center_and_radius(...
        pPI(1), pPI(2), pEI(1), pEI(2), vP);


    %% Video: create video object
    if create_video
        video_name = base_name;
        vidObj = VideoWriter([video_name, '.avi']);
        open(vidObj);
    end

    %% animation
    for t = 0 : 1/30 : t_end

        clf;
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");
%         xlim(my_xlim);
%         ylim(my_ylim);

        plot_with_vectors(pEI, 'xb');
        col_circle(pEI(1), pEI(2), vE * t, 'b');

        plot_with_vectors(pPI, 'xr');
        col_circle(pPI(1), pPI(2), vP * t, 'r');

        % render: DB-circles
        col_circle(center(1), center(2), radius, 'k');

        drawnow;

        if create_video
            writeVideo(vidObj, getframe(gcf));
        end
    end

    %% Video: close video object
    if create_video
        close(vidObj);
    end
end

