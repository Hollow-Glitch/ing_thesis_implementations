function comparison_of_PPDB_and_DADB()
    clf;
    axis equal;
    hold on;

    points = get_PPDB();
    plot(points(1,:), points(2,:), 'Color', '#00b100');

    xAI = -1; yAI = 0; xTI = 0; yTI = 0; k = 1.2;
    [center, radius ] = DB_circle_center_and_radius(xAI, yAI, xTI, yTI, k);
    circle_points = arc2(center(1), center(2), radius, 0, 2*pi, 0.01);
    plot(circle_points(1,:), circle_points(2,:), "Color", '#7E2F8E');

    plot(xTI, yTI, 'bx');
    plot(xAI, yAI, 'rx');

    plot(    5, 0, '*', Color='#D95319');
    plot(-0.45, 0, '*', Color='#D95319');
    
    xline(0);
    yline(0);
    xlabel('x');
    ylabel('y');

    %% create figure
    set_fig_style_bc_squareish();
    print_current_fig("comparison_of_PPDB_and_DADB");
end

function points = get_PPDB()
%     clf;
    animationsOn = true;
    heading_angle_count = 40;

    %% setup
    % xT = 0, yT = 0;
    IC = [-1, 0];  % => xA = -1, yA = 0;
    vA1_propConst = 1.2;
    EP_sys_closed = @(t, sys_vec_in, angle) EP_sys( t, sys_vec_in, vA1_propConst, angle );

    capture_points = [];

    matFileName = "ppdb_save.mat";
    if isfile(matFileName)
        load(matFileName);
        return;
    end

    %% core
    for angle = 0:(2*pi)/heading_angle_count:2*pi
        % solve sys
        options = odeset('RelTol',1e-5,'AbsTol',1e-5);
        [t, sys_vec_out] = ode45(@(t, IN) EP_sys_closed(t, IN, angle), [0 8], IC, options);

        % unpack Attacker sys
        pAx1 = sys_vec_out(:, 1);
        pAy1 = sys_vec_out(:, 2);

        % calculate Target
        [pTx, pTy] = pT_func(t, 1, angle, 0, 0);

        % radia used to draw and check for collision
        common_radius = 0.05;
        evader_radius  = common_radius;
        pursuer_radius = common_radius;

%         % calculate attacker for verification purposes
%         [xA_ver, yA_ver] = pT_func(t, vA1_propConst, 0, -1, 0);
%         xAI = -1; yAI = 0; xTI = 0; yTI = 0; k = 1.2;
%         [center, radius ] = DB_circle_center_and_radius(xAI, yAI, xTI, yTI, k);

        %% Animation
        for i = 1:length(t)
            clf;
            hold on;
            axis equal;
%             xlim(my_xlim);
%             ylim(my_ylim);
            xline(0);
            yline(0);
%             title(t(i));
            xlabel('x');
            ylabel('y');
    
            if animationsOn
                % Target
                plot(pTx(1),   pTy(1),   'bo');  % start position indicator
                plot(pTx(1:i), pTy(1:i), 'b');   % path
                circle(pTx(i), pTy(i), evader_radius, 'b');  % current position
        
                % Attacker
                plot(pAx1(1),   pAy1(1),   'ro');  % start position indicator
                plot(pAx1(1:i), pAy1(1:i), 'r');   % path
                circle(pAx1(i), pAy1(i), pursuer_radius, 'r'); % current position
            end
    
            pT  = [pTx(i);  pTy(i)];
            pA1 = [pAx1(i); pAy1(i)];
            if norm(pT - pA1) <= 0.0001
                table(pA1, pT)
                disp("pT - pA1 = ");
                disp(pT - pA1);
                norm(pT - pA1)
                disp("A1-Booom");
                capture_points = [capture_points, pT];
                break
            end
    
            if animationsOn
                pause(0.1);
            end
        end
    end
    points = getClosedPolygonFromPointSet(capture_points(1,:), capture_points(2,:));
    save(matFileName);

end


function circle(x, y, radius, color)
    r_half = radius / 2;
    x = x - r_half;
    y = y - r_half;
    pos = [x y radius radius];
    rectangle('Position', pos, 'Curvature', 1, 'FaceColor', color);
end


function [xT, yT] = pT_func(t, vT_norm, angle, xTI, yTI)
    xT = vT_norm .* cos(angle) .* t + xTI;
    yT = vT_norm .* sin(angle) .* t + yTI;
end

%% EP_SYS
function sys_vec_out = EP_sys( ...
        t,                     ...
        sys_vec_in,            ...
        A1_velocityPropConst,  ...
        angle                  ...
    )
    % % unpack
    in_pAx1 = sys_vec_in(1);
    in_pAy1 = sys_vec_in(2);
    
    % % ------------------------------------------------------------
    [vTx, vTy] = pT_func(t, 1, angle, 0, 0);

    pA1_scalar_part =          ...
        A1_velocityPropConst   ...
        /                      ... 
        sqrt( (vTx - in_pAx1)^2 + (vTy - in_pAy1)^2 );
    vAx1 = pA1_scalar_part * (vTx - in_pAx1);
    vAy1 = pA1_scalar_part * (vTy - in_pAy1);

    
    % % ------------------------------------------------------------
    % % pack
    sys_vec_out = [
            vAx1;   ...
            vAy1;    ...
        ];
end
