function single_agent_isochrone()
    %% Options
    create_figure = false;
    create_video  = true;

    %% Setup
    base_name = 'basic_isochrone_single_agent';
    t_end = 3;

    p_EI = [0;0]; % m
    v_E  = 25;    % m/s

    yellow = '#EDB120';
    orange = '#D95319';
    red    = '#A2142F';

    %% create reference render for both figure and animation limits
    clf;
    hold on;
    axis equal;
    plot_with_vectors(p_EI, 'xb');
    function isochrone_with_annotation(t, color)
        text_uvec = get_directional_uvec(pi/4);
        radius = t * v_E;
        circ = param_circle(p_EI(1), p_EI(2), radius, 0:2*pi/100:2*pi);
        plot(circ(1,:), circ(2,:), Color=color);
        p_text = radius * text_uvec;
        text_1 = sprintf('$t=%d$s', t);
        % textArroundPoint(p_text, (radius/3) + text_uvec, text_1);
        textArroundPoint(p_text, 5+text_uvec, text_1);
    end
    isochrone_with_annotation(1/3*t_end, yellow);
    isochrone_with_annotation(2/3*t_end, orange);
    isochrone_with_annotation(t_end, red);

    %% create figure
    if create_figure
        fig_name = [base_name, '_t=1,2,3_v_e=25'];
        set_fig_style_bc_squareish();
        print_current_fig(fig_name, "outputType","pdf");
    end
    %% Video: create video object
    if create_video
        video_name = base_name;
        vidObj = VideoWriter([video_name, '.avi']);
        open(vidObj);
    end

    %% animation limits
    my_xlim = xlim * 1.1;
    my_ylim = ylim * 1.1;

    %% animation
    for t = 0 : 1/60 : t_end
        isochrone_radius = v_E * t;

        clf;
        hold on;
        axis equal;
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");
        xlim(my_xlim);
        ylim(my_ylim);

        plot_with_vectors(p_EI, 'xb');
        circle(p_EI(1), p_EI(2), isochrone_radius);

        drawnow;

        if create_video
            writeVideo(vidObj, getframe(gcf));
        end
    end

    %% Video: close video object
    if create_video
        close(vidObj);
    end
end

