function isochrone_based_capture()
    %% options
    create_figure = false;
    create_video  = false;
    run_animation = true;
    % !!! : figures and video at the same time can't be created

    % If animation is not running video can't be created, hence:
    create_video = run_animation && create_video;

    base_name = 'circle_1v1_with_collision_isochrone';

    %%
    % simulation variables
    t_step = 0.01;
    t_pause = 0.0001;

    % attacker
    pAI = [-0.5;  1];
    vAr = 1;  % == vA_norm
    
    % target
    pTI = pT(0);
    
    % collision position & time
    % since we have such a setup here, that we don't we have vTnorm, but
    % only the vAnorm, but the bellow function needs vTnorm and k, we have a little problem.
    % But we will solve it by using the relation that vAnorm = vTnorm * k
    % by setting k=1, and vTnorm=vAnorm, hence even though in the
    % equation which we have in the function we have vTnorm * k
    % it will be equivalent to vAnorm
    out = get_colision_time_and_point_using_isochrone(pAI(1), pAI(2), vAr, 1);
    tC = out.t_c;
    pC = out.p_c;
%     tC
%     % !!!!!!!!!!!!!!!!!!!!!!
%     % !!! pC experiments !!!
%     % !!!!!!!!!!!!!!!!!!!!!!
%     pC(2) = -pC(2);
    
    % attackers direction to reach collision position
    dAIC = pC - pAI; % vector from the attacker's initial position to the colision point
    vAa = anti_clockwise_angle_from_x_of_vector(dAIC);
    
%     % !!!!!!!!!!!!!!!!!!!!!!
%     % !!! tC experiments !!!
%     % !!!!!!!!!!!!!!!!!!!!!!
%     if false
%         tC = 5; % temporary tC
%     elseif false
%         tC = abs(tC);
%         tC
%     end
    tT = 0:t_step:tC;
    [xT, yT] = pT(tT);
    draw_target_path = @() plot(xT, yT, '--k');
    draw_collision_point = @() plot(pC(1), pC(2), 'kx');
    attacker_isochrone = @(t) arc2_draw(pAI(1), pAI(2), t*vAr, 0, 2*pi, 0.1, "decor",'k-');
    draw_attacker_path = @() plot([pAI(1), pC(1)],[pAI(2),pC(2)], '--k');
    
    function loopbody(t)
        clf;
        hold on;
        axis equal;
%         xline(0);
%         yline(0);
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");

        % animation computations
        [xT, yT] = pT(t);
        
        % animation drawings
        % - precomputed paths
        draw_target_path();
        draw_collision_point();
        draw_attacker_path();
        % - current moments
        plot(xT, yT, 'ob');
        tT = 0:t_step:t;
        [xT, yT] = pT(tT);
        plot(xT, yT, '-b');
        attacker_isochrone(t);
        [pAx, pAy] = entity_position(t, pAI(1), pAI(2), vAr, vAa);
        plot(pAx, pAy, 'ro');
        plot([pAI(1), pAx],[pAI(2), pAy], 'r-');

        plot(pAI(1), pAI(2), 'rx');
    end

    function create_figure_func(tC)
        loopbody(tC);

        % create figure
%         fig_name = sprintf('circle_1v1_with_collision_isochrone_tC=%f', tC);
        fig_name = sprintf([base_name, '_tC=%f'], tC);
        set_fig_style_bc_squareish();
        print_current_fig(fig_name, outputType="pdf");
    end
    if create_figure
        create_figure_func(tC*0.5);
        create_figure_func(tC*0.8);
    end

    %% create media
    if ~run_animation
        return;
    end
    if create_video
        vidObj = VideoWriter([base_name, '.avi']);
        open(vidObj);
    end

    %% game loop
    for t = 0:t_step:tC
        loopbody(t);
        
%         pause(t_pause);
        drawnow;

        if create_video
            writeVideo(vidObj, getframe(gcf));
        end
    end

    if create_video
        close(vidObj);
    end
end


% The in_... prefix is needed because we are creating symbolic variables
% with the same name.
% Here we are assuming that the position function of the target is 
% f(t) = (cos(t), sin(t))  .
function out = get_colision_time_and_point_using_isochrone(in_x_AI, in_y_AI, in_vTnorm, in_k)
    syms x_T y_T x_AI y_AI vTnorm k t;
    eq = (cos(t) - x_AI)^2 + (sin(t)-y_AI)^2 == (t * vTnorm * k)^2;
    input.x_AI   = in_x_AI;
    input.y_AI   = in_y_AI;
    input.vTnorm = in_vTnorm;
    input.k      = in_k;
    
    eq_s = subs(eq, input);
    t_c = vpasolve(eq_s, [0, Inf])
%     t_c
    % Matlab says that it cans solve this symbolicaly so we will use 
    % its numeric functions.
    out.t_c = t_c;
    out.p_c = [cos(t_c); sin(t_c)];
end


function [x, y] = pT(t)
    x = cos(t);
    y = sin(t);
end

% t   ... time
% xEI ... entity's initial x position
% yEI ... entity's initial y position
% vEr ... entity's velocity's radial  component
% vEa ... entity's velocity's angular component
function [pEx, pEy] = entity_position(t, xEI, yEI, vEr, vEa)
    pEx = vEr .* cos(vEa).*t + xEI;
    pEy = vEr .* sin(vEa).*t + yEI;
end