function sla__isochrone_DB_capture()
%     lets_test_the_discriminant()
%     circle_test();
%     return;


    configScenario.vT_norm = 1;
    configScenario.k = 2;

    configScenario.xAI = -1;
    configScenario.yAI = 1;
    configScenario.xTI = 1;
    configScenario.yTI = -1;

    % configScenario.vTa = 0;
    % configScenario.vTa = (1/4).*pi;
    % configScenario.vTa = (1/2).*pi;
    configScenario.vTa = pi;
    % configScenario.vTa = (3/2).*pi;
    % configScenario.vTa = 1.231503;
    % configScenario.vTa = 1.231503712340852;

    % friendly reminder, some things are only drawn if for ex.: D > 0; and etc.
    configShow.isochroneTar  = true;
    configShow.isochroneAtt  = true;
    configShow.appolonius    = true;
    configShow.tangentLines  = false;
    configShow.pathTar       = false;
    configShow.pathAtt       = false;
    configShow.intersections = false;
    configShow.weirdLine     = false;
    configShow.timeInTitle   = false;  % if animating, then set this to true
    configShow.annotationTOnRight = false;
    configShow.annotationAOnRight  = true;
    
    % just_show(vT_norm, vTa, xAI, yAI, xTI, yTI, k, 20, configShow);
    % animate(vT_norm, vTa, xAI, yAI, xTI, yTI, k, 20, configShow)
%     generate_plots(vT_norm, vTa, xAI, yAI, xTI, yTI, k, configShow)

%     just_show(configScenario, 3, configShow);
%     animate(configScenario, 3, configShow);
    timeArr = [2]; 
    genPredefOnes = true;
    generate_plots(configScenario, configShow, timeArr, genPredefOnes);
    if isempty(timeArr)
        disp('!WARNING!: length(timeArr) == 0; THIS MAY NOT BE INTENDED');
    end
end


function lets_test_the_discriminant()
    vT_norm = 1;
    k = 0.8;
    
    xAI = 3;
    yAI = 2;
    xTI = 5;
    yTI = -1;
%     vTa = 1.231503;
%     vTa = 1.231503712340852;
%     vTa_range = 1.225:0.00001:1.232;

    hold on;
    just_show(vT_norm, 1.2, xAI, yAI, xTI, yTI, k, 3);
    just_show(vT_norm, 1.21, xAI, yAI, xTI, yTI, k, 3);
    just_show(vT_norm, 1.22, xAI, yAI, xTI, yTI, k, 3);
    just_show(vT_norm, 1.23, xAI, yAI, xTI, yTI, k, 3);
    just_show(vT_norm, 1.2315, xAI, yAI, xTI, yTI, k, 3);
    just_show(vT_norm, 1.24, xAI, yAI, xTI, yTI, k, 3);
end

function just_show(configScenario, t, configShow)
    loopbody(configScenario, t, configShow)
end

function generate_plots(configScenario, configShow, timeArr, genPredefOnes)
    function letThePainterPaint(configScenario, configShow, t, namePart)
        loopbody(configScenario, t, configShow);

        name = sprintf('appolonius__t=%f__configShow=%d%d%d%d%d%d%d%d%d__%s', t, ...
            configShow.isochroneTar  ,  ...
            configShow.isochroneAtt  ,  ...
            configShow.appolonius    ,  ...
            configShow.tangentLines  ,  ...
            configShow.pathTar       ,  ...
            configShow.pathAtt       ,  ...
            configShow.intersections ,  ...
            configShow.weirdLine     ,  ...
            configShow.timeInTitle   ,  ...
            namePart                 );
        set_fig_style_bc_squareish();
        print_current_fig(name);
    end

%     for t = timeArr
%         clf;
%         letThePainterPaint(configScenario, configShow, t);
%     end
    
    if genPredefOnes
        configScenario.vT_norm = 1;
        configScenario.k = 3;
        configScenario.xAI = -1;
        configScenario.yAI = 1;
        configScenario.xTI = 1;
        configScenario.yTI = -1;
        configScenario.vTa = pi;
        clf; letThePainterPaint(configScenario, configShow, 0.8, 'dominance_boundary');
        clf; letThePainterPaint(configScenario, configShow, 1  , 'dominance_boundary');
        
        configScenario.vT_norm = 1;
        configScenario.xAI = -1;
        configScenario.yAI = 0;
        configScenario.xTI = 1;
        configScenario.yTI = 0;
        configScenario.vTa = pi;
        configScenario.k = 0.5;
        clf; letThePainterPaint(configScenario, configShow, 1.5, 'manyFaces_k=0.5');

        configScenario.k = 1;
        configShow.annotationTOnRight = true;
        configShow.annotationAOnRight = false;
        clf; letThePainterPaint(configScenario, configShow, 1.5, 'manyFaces_k=1');

        configScenario.vT_norm = 0.5;
        configScenario.k = 2;
        configShow.annotationTOnRight = false;
        configShow.annotationAOnRight = true;
        clf; letThePainterPaint(configScenario, configShow, 1.5, 'manyFaces_k=2');

        configScenario.vT_norm = 1;
        configScenario.k       = 1.2;
        configScenario.xAI     = 3;
        configScenario.yAI     = -2;
        configScenario.xTI     = 10;
        configScenario.yTI     = 1;
        configScenario.vTa     = (1/4).*pi;
        configShow.isochroneTar  = true;
        configShow.isochroneAtt  = true;
        configShow.appolonius    = true;
        configShow.tangentLines  = false;
        configShow.pathTar       = true;
        configShow.pathAtt       = true;
        configShow.intersections = true;
        configShow.weirdLine     = false;
        configShow.timeInTitle   = true;
        configShow.annotationTOnRight = false;
        configShow.annotationAOnRight = true;
        clf; letThePainterPaint(configScenario, configShow, 20, 'with_intersec');
    end
end

function animate(configScenario, t_end, configShow)
    for t = 0:0.1:t_end
        clf;
        hold on;
        axis equal;

        configShow.timeIn = true;
        
        loopbody(configScenario, t, configShow)
        
        pause(0.01);
    end
end

function loopbody(configScenario, t, configShow)
    hold on;
    axis equal;
    xlabel('x');
    ylabel('y');

    vT_norm = configScenario.vT_norm;
    k       = configScenario.k      ;
    xAI     = configScenario.xAI    ;
    yAI     = configScenario.yAI    ;
    xTI     = configScenario.xTI    ;
    yTI     = configScenario.yTI    ;
    vTa     = configScenario.vTa    ;

    if configShow.timeInTitle
        title(sprintf('t = %.2f s', t));
    end
    
    % comment out the below line if you don't want the line in the render
    if configShow.weirdLine
        draw_the_weird_line(vT_norm, xAI, yAI, xTI, yTI, k, t)
    end
    
    if configShow.tangentLines
        appolonius_circle_tangent_lines(xAI, yAI, xTI, yTI, k)
    end

    [intersec1,intersec2,D] = ...
        appolonius_target_intersections(vT_norm, vTa, xAI, yAI, xTI, yTI, k);
    
    if configShow.appolonius
        if k == 1
            length = 4;
            appolonius_line(xAI, yAI, xTI, yTI, length, configShow)
        else
            appolonius_circle(xAI, yAI, xTI, yTI, k, configShow)
        end
    end

    
    % !!!!!
    % !!!!! SHOULDN'T THE ANGLE BE CALCULATED ONLY ONCE???
    % !!!!!
    % attacker motion line
    if D > 0
        intersec1p = [intersec1.x, intersec1.y];
        intersec2p = [intersec2.x, intersec2.y];
        pAI = [xAI, yAI];
        d_AI_inter1 = intersec1p - pAI;
        d_AI_inter2 = intersec2p - pAI;
        
        if intersec1.t > 0 || intersec2.t > 0
            if intersec1.t > 0 && intersec2.t > 0
                if norm(d_AI_inter1) < norm(d_AI_inter2)
                    vAa = anti_clockwise_angle_from_x_of_vector(d_AI_inter1);
                    my_intersec = intersec1;
                else
                    vAa = anti_clockwise_angle_from_x_of_vector(d_AI_inter2);
                    my_intersec = intersec2;
                end
            elseif intersec1.t > 0
                vAa = anti_clockwise_angle_from_x_of_vector(d_AI_inter1);
                my_intersec = intersec1;
            elseif intersec2.t > 0
                vAa = anti_clockwise_angle_from_x_of_vector(d_AI_inter2);
                my_intersec = intersec2;
            end

            vAr = k .* vT_norm;
            if configShow.pathAtt
                % dashed lines from the initial positions to the intersec point
                plot([xAI, my_intersec.x], [yAI, my_intersec.y], '--')
                plot([xTI, my_intersec.x], [yTI, my_intersec.y], '--')
                entity_motion_line(t, xAI, yAI, vAr, vAa)
            end
        end
        
        if intersec1.t > 0 && configShow.intersections
            plot(intersec1.x, intersec1.y, '*b');
        end
        if intersec2.t > 0 && configShow.intersections
            plot(intersec2.x, intersec2.y, '*b');
        end
        
    end
    
    % target   motion line
    vTr = vT_norm;
    if configShow.pathTar
        entity_motion_line(t, xTI, yTI, vTr, vTa)
    end
    
    % isochrones
    if configShow.isochroneAtt
        circle(xAI, yAI, k .* vT_norm .* t);
    end
    if configShow.isochroneTar
        circle(xTI, yTI,      vT_norm .* t);
    end
end

% % ???
% % why did I think that I need this? plot([x1 x2], [y1 y2]) is not good
% enough?????
% line from p1 to p2
function line_between_two_points(p1, p2)
    % p1 should be a vector of equal length of that of p2
    d = p2 - p1;
    length = norm(d);
    angle = anti_clockwise_angle_from_x_of_vector(d);
    x = length .* cos(angle).*t + p1(1);
    y = length .* sin(angle).*t + p1(2);
end

function entity_motion_line(t, xEI, yEI, vEr, vEa)
    [pEx0, pEy0] = entity_position(0, xEI, yEI, vEr, vEa);
    [pEx, pEy] = entity_position(t, xEI, yEI, vEr, vEa);
    plot( [pEx0,pEx] , [pEy0,pEy] , '-o');
end

% t   ... time
% xEI ... entity's initial x position
% yEI ... entity's initial y position
% vEr ... entity's velocity's radial  component
% vEa ... entity's velocity's angular component
function [pEx, pEy] = entity_position(t, xEI, yEI, vEr, vEa)
    pEx = vEr .* cos(vEa).*t + xEI;
    pEy = vEr .* sin(vEa).*t + yEI;
end

% the intersection point or points of the appolonius circle
% and the targets position line
function [intersec1, intersec2, D] ...
    = appolonius_target_intersections(vT_norm, vTa, xAI, yAI, xTI, yTI, k)

    vTr = vT_norm;
    target_position = @(t) entity_position(t, xTI, yTI, vTr, vTa);

    xCA = (k^2 .* xTI - xAI) / (k^2-1);
    yCA = (k^2 .* yTI - yAI) / (k^2-1);
    rCA = (k .* sqrt( (xAI - xTI)^2 + (yAI - yTI)^2 )) / (1-k^2);

    mTx = vTr .* cos(vTa);
    mTy = vTr .* sin(vTa);
    a = mTx^2 + mTy^2;
    b = 2.*(mTx.*xTI + mTy.*yTI - xCA.*mTx - yCA.*mTy);
    c = (xCA - xTI)^2 + (yCA - yTI)^2 - rCA^2;
    
    D = b^2 - 4.*a.*c;
    t1 = (-b + sqrt(D)) / (2.*a);
    t2 = (-b - sqrt(D)) / (2.*a);

    [pTxt1, pTyt1] = target_position(t1);
    [pTxt2, pTyt2] = target_position(t2);
    
    intersec1 = struct;
    intersec1.t = t1;
    intersec1.x = pTxt1;
    intersec1.y = pTyt1;
    
    intersec2 = struct;
    intersec2.t = t2;
    intersec2.x = pTxt2;
    intersec2.y = pTyt2;

%     plot(pTxt1, pTyt1, '*b');
%     plot(pTxt2, pTyt2, '*b');
%     
%     t = -5:1:20;
%     [x,y] = target_position(t);
%     plot(x,y, '--');
end

function appolonius_circle_tangent_lines(xAI, yAI, xTI, yTI, k)
    if k > 1
        % [p]osition of the [C]circle [A]ppolonius
        pCA = [
            (k^2 .* xTI - xAI) / (k^2-1)  ,  (k^2 .* yTI - yAI) / (k^2-1)
        ];
        pAI = [xAI,  yAI];
        pTI = [xTI,  yTI];

        % distance between the attacker's initial position and the 
        % appolonius circle's center
        d_TI_CA = norm(pCA - pAI);
        d_AI_TI = norm(pTI - pAI);

        rCA = (k .* d_AI_TI) / (1-k^2);

        theta = asin( rCA / d_TI_CA );

        theta = abs(theta);

        % hehe :) I was right, this works
        add_this_angle = anti_clockwise_angle_from_x_of_vector(pTI - pAI);

        % let's try with the polar representation of a line
    %     theta_1 = theta + pi;
    %     theta_2 = -theta + pi;
        theta_1 = theta + add_this_angle;
        theta_2 = -theta + add_this_angle;

        l = 0:1:15;
        x = xAI + l .* cos(theta_1);
        y = yAI + l .* sin(theta_1);
        plot(x,y);

        x = xAI + l .* cos(theta_2);
        y = yAI + l .* sin(theta_2);
        plot(x,y);
    elseif k < 1
        % [p]osition of the [C]circle [A]ppolonius
        pCA = [
            (k^2 .* xTI - xAI) / (k^2-1)  ,  (k^2 .* yTI - yAI) / (k^2-1)
        ];
        pAI = [xAI,  yAI];
        pTI = [xTI,  yTI];

        % distance between the attacker's initial position and the 
        % appolonius circle's center
        d_TI_CA = norm(pCA - pTI);
        d_AI_TI = norm(pTI - pAI);

        rCA = (k .* d_AI_TI) / (1-k^2);

        theta = asin( rCA / d_TI_CA );

        theta = abs(theta);

        % hehe :) I was right, this works
        add_this_angle = anti_clockwise_angle_from_x_of_vector(pAI - pTI);

        % let's try with the polar representation of a line
    %     theta_1 = theta + pi;
    %     theta_2 = -theta + pi;
        theta_1 = theta + add_this_angle;
        theta_2 = -theta + add_this_angle;

        l = 0:1:15;
        
        x = xTI + l .* cos(theta_1);
        y = yTI + l .* sin(theta_1);
        plot(x,y);

        x = xTI + l .* cos(theta_2);
        y = yTI + l .* sin(theta_2);
        plot(x,y);
    end
end

% this is the line which meets the appolonius circle at a time t
% at the same place as the attacker's isochrone
function draw_the_weird_line(vT_norm, xAI, yAI, xTI, yTI, k, t)
    x = -5:0.1:10;
    xCA = (k^2 .* xTI - xAI) / (k^2-1);
    yCA = (k^2 .* yTI - yAI) / (k^2-1);
    
    rIA = t .* vT_norm .* k;
    rCA = (k .* sqrt( (xAI - xTI)^2 + (yAI - yTI)^2 )) / (1-k^2);
    
    y = (xCA^2 - xAI^2 + yCA^2 - yAI^2 + rIA^2 - rCA^2 - x.*(2.*xCA-2.*xAI)) ...
        / (2 .* yCA - 2 .* yAI);
    
    plot(x, y);
end

function appolonius_line(xAI, yAI, xTI, yTI, length, configShow)
    appolonius_line_clever(xAI, yAI, xTI, yTI, length, configShow);
%     appolonius_line_derived(xAI, yAI, xTI, yTI, length, configShow);
end

function appolonius_line_derived(xAI, yAI, xTI, yTI, length, configShow)
    a = 2.*xTI - 2.*xAI;
    b = 2.*yTI - 2.*yAI;
    c = xAI^2 + yAI^2 - yTI^2 - xTI^2;
    %    ax + by + c = 0
    %        -ax - c
    % ~> y = -------
    %           b
%     x = min([xAI, xTI]) : 0.1 : max([xAI, xTI]);
%     y = (-a.*x - c) / b;
    
    if     yTI == yAI        % vertical line
        % here we could use
        %   x = rcos(theta)
        %   y = rsin(theta)
        % or just
        x = (xTI^2 - xAI^2) / (2.*xTI - 2.*xAI);
        plot([x, x], [-length/2, length/2], '--r');
    elseif xTI == xAI
        y = (yTI^2 - yAI^2) / (2.*yTI - 2.*yAI);
        plot([-length/2, length/2],[y, y], '--r');
    else
%         x = min([xAI, xTI]) : 0.1 : max([xAI, xTI]);
        x1 = min([xAI, xTI]);
        x2 = max([xAI, xTI]);
        y = @(x)(-a.*x - c) / b;
        y1 = y(x1);
        y2 = y(x2);
        plot([x1, x2], [y1, y2], '--r');
    end
    
    pAI = [xAI; yAI];
    pTI = [xTI; yTI];
    if configShow.annotationTOnRight
        text(pTI(1), pTI(2), '$\leftarrow p_{EI}$','interpreter','latex', 'HorizontalAlignment','left');
    else
        text(pTI(1), pTI(2), '$p_{EI}\rightarrow$','interpreter','latex', 'HorizontalAlignment','right');
    end
    if configShow.annotationAOnRight
        text(pAI(1), pAI(2), '$\leftarrow p_{PI}$','interpreter','latex', 'HorizontalAlignment','left');
    else
        text(pAI(1), pAI(2), '$p_{PI}\rightarrow$','interpreter','latex', 'HorizontalAlignment','right');
    end
    plot(pTI(1), pTI(2), 'o'); 
    plot(pAI(1), pAI(2), 'o'); 

end

% TODO: refactor "appolonius" into dominance boundary or something like that
function appolonius_line_clever(xAI, yAI, xTI, yTI, length, configShow)
    pAI = [xAI; yAI];
    pTI = [xTI; yTI];
    pointBetween_pAI_and_pTI = (1/2).*(pAI + pTI);
    vAT = (pTI - pAI);
    vAI_unit = vAT / norm(vAT);
    
    linesDirection = [vAI_unit(2); -vAI_unit(1)]; 
%     length = 3; % TODO:extract
    
    startPoint = pointBetween_pAI_and_pTI + (length/2) .* linesDirection;
    endPoint   = pointBetween_pAI_and_pTI - (length/2) .* linesDirection;
    plot([startPoint(1), endPoint(1)], [startPoint(2), endPoint(2)], 'r');
    
    if configShow.annotationTOnRight
        text(pTI(1), pTI(2), '$\leftarrow p_{EI}$','interpreter','latex', 'HorizontalAlignment','left');
    else
        text(pTI(1), pTI(2), '$p_{EI}\rightarrow$','interpreter','latex', 'HorizontalAlignment','right');
    end
    if configShow.annotationAOnRight
        text(pAI(1), pAI(2), '$\leftarrow p_{PI}$','interpreter','latex', 'HorizontalAlignment','left');
    else
        text(pAI(1), pAI(2), '$p_{PI}\rightarrow$','interpreter','latex', 'HorizontalAlignment','right');
    end
    plot(pTI(1), pTI(2), 'o'); 
    plot(pAI(1), pAI(2), 'o'); 
end

function appolonius_circle(xAI, yAI, xTI, yTI, k, configShow)
    pTI = [xTI, yTI];
    
    pAI = [xAI, yAI];

    radius = (k.*sqrt( (xAI-xTI)^2 + (yAI-yTI)^2 )) / (1-k^2);
    radius = abs(radius);
    
    centerX = (k^2 .* xTI - xAI) / (k^2-1);
    centerY = (k^2 .* yTI - yAI) / (k^2-1);
    
    hold on;
    axis equal;
    plot(pTI(1), pTI(2), 'o'); 

    if configShow.annotationTOnRight
        text(pTI(1), pTI(2), '$p_{EI}\rightarrow$','interpreter','latex', 'HorizontalAlignment','right');
    else
        text(pTI(1), pTI(2), '$\leftarrow p_{EI}$','interpreter','latex', 'HorizontalAlignment','left');
    end
    if configShow.annotationAOnRight
        text(pAI(1), pAI(2), '$p_{PI}\rightarrow$','interpreter','latex', 'HorizontalAlignment','right');
    else
        text(pAI(1), pAI(2), '$\leftarrow p_{PI}$','interpreter','latex', 'HorizontalAlignment','left');
    end

    plot(pAI(1), pAI(2), 'o'); 
    plot(centerX, centerY, 'x');
    col_circle(centerX, centerY, radius, 'r');
end


function circle_test()
    axis equal;
    hold on;
    xline(0);
    yline(0);
    xline(1);
    xline(-1);
    yline(1);
    yline(-1);
    circle(0,0,1);
end

function circle(centerX, centerY, radius)
    stepped_col_circle(centerX, centerY, radius, 'k', 0.025);
end

function col_circle(centerX, centerY, radius, edge_color)
    stepped_col_circle(centerX, centerY, radius, edge_color, 0.01);
end

function stepped_col_circle(centerX, centerY, radius, edge_color, theta_step)
    theta = 0 : theta_step : 2*pi;
    out = param_circle(centerX, centerY, radius, theta);
    x = out(1,:);
    y = out(2,:);
    x = [x, x(1)];
    y = [y, y(1)];
%     plot(x,y, 'o'); % for testing
    plot(x,y, edge_color);
end

function out = param_circle(xc, yc, r, theta)
    x = xc + r .* cos(theta);
    y = yc + r .* sin(theta);
    
    out = [
        x;
        y
        ];
end
