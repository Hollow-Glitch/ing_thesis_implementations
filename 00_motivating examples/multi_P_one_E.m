function multi_P_one_E()
    clf;
    hold on;
    axis equal;
    xlim([-12,12]);
    ylim([-12,12]);
    xlabel('x');
    ylabel('y');

    pTI = [0;0];
    vT = 13;
    number_of_attackers = 5;

    xTI = pTI(1);
    yTI = pTI(2);
    % dbcc ... DB circle center
    % alpha ... the angle : dbcc, pTI, tangent_point
    % 2 * alpha ... the angle : dbcc1, pTI, dbcc2
    %           ... also the angle : pAI1, pTI, pAI2
    % 2 * numberOfPursuers * alpha = 2 * pi  =>
    alpha = (2 * pi) / (2 * number_of_attackers);
    proport_const = sin( pi / number_of_attackers);
    vA = vT * proport_const;
    
    % it seems that the radius of the circle on which the attackers lie
    % is free, rADC ... attacker distribution circle radius
    rADC = 10;
    pAI = [];
    for attacker_index = 1:1:number_of_attackers
        angle = 2 * alpha * attacker_index;
        pAI = [pAI, [ xTI + rADC * cos(angle); yTI + rADC * sin(angle)]];
    end

    function draw_stuff(pT, pA)
        xT = pT(1);
        yT = pT(2);
        plot_with_vectors(pT, 'sb');
%         col_circle(pTI(1), pTI(2), rADC, 'k--');
        for  attacker_index = 1:1:number_of_attackers
            plot_with_vectors(pA(:, attacker_index), 'rd'); 
            textArroundPoint(pA(:, attacker_index), [0.5;1.5], attacker_index);
        end
    end

    draw_stuff(pTI, pAI);

    %% create figure
    set_fig_style_bc_squareish();
    print_current_fig("motivator__multi_P_one_E");
end

