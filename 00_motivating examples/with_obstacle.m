function with_obstacle()
    pTI = [
        0;
        0
        ];
%     pAI = [
%          9;
%         10
%         ];
    pAI = [
         5;
         8
        ];
    f1 = [
         0;
        10
        ];
    f2 = [
        10;
         0
        ];

    hold on;
    axis equal;
    xlim([-2, 12]);
    ylim([-2, 12]);
    xlabel('x');
    ylabel('y');

    plot_with_vectors([f1,f2], '-k');
    plot_with_vectors(pTI, 'bs');
    plot_with_vectors(pAI, 'rd');

    %% create figure
    set_fig_style_bc_squareish();
    print_current_fig("motivator__with_obstacle");
end

