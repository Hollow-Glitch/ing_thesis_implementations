function an_evader_and_a_pursuer()
    pTI = [
        0;
        0
        ];
    pAI = [
         4;
         4
        ];

    hold on;
    axis equal;
    xlim([-1, 5]);
    ylim([-1, 5]);
    xlabel('x');
    ylabel('y');

    plot_with_vectors(pTI, 'bs');
    plot_with_vectors(pAI, 'rd');

    %% create figure
    set_fig_style_bc_squareish();
    print_current_fig("motivator__base");
end

