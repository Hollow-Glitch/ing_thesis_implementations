function circular_evader_path()
    pTI = [
        0;
        0
        ];
    pAI = [
         5;
         8
        ];

    hold on;
    axis equal;
    xlim([-5, 7]);
    ylim([-2, 10]);
    xlabel('x');
    ylabel('y');

    radius = 3;
    col_circle(pTI(1), pTI(2)+radius, radius, 'k--');
    plot_with_vectors(pTI, 'bs');
    plot_with_vectors(pAI, 'rd');

    %% create figure
    set_fig_style_bc_squareish();
    print_current_fig("motivator__parametric_curve");
end


