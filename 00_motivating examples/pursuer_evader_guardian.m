function pursuer_evader_guardian()
    pTI = [
        0;
        0
        ];
    pAI = [
         4;
         4
        ];
%     f1 = [
%          0;
%         10
%         ];
    pGI = [
         5;
         5
        ];

    hold on;
    axis equal;
    xlim([-1, 6]);
    ylim([-1, 6]);
    xlabel('x');
    ylabel('y');


    plot_with_vectors(pTI, 'bs');
    plot_with_vectors(pAI, 'rd');
    plot_with_vectors(pGI, 'b^');

    %% create figure
    set_fig_style_bc_squareish();
    print_current_fig("motivator__target_attacker_guardian");
end

