% Dominance Region Boundary hyperbola
% f1 ... vector 2D
% f2 ... vector 2D
% in order to define a hyperbola, it is sufficient to define: f1, f2, a
% but here I am already working on my special PEG hyperbola for obstacles
% define the hyperbolic angle phi as:
%     phi = -pi/2 : 0.01 : pi/2;
function drbh = hyperbola(f1, f2, a, phi)
    % m
    vec_f1f2_unit = (1/norm(f2 - f1)) .* (f2-f1);
    m = f1 + (norm(f2 - f1) / 2) .* vec_f1f2_unit; 
    
    % a, v1, v2
    c = norm(f2 - f1) / 2;
%     a = c / 2;
    v1 = m - a .* vec_f1f2_unit;
    v2 = m + a .* vec_f1f2_unit;
    
    b = sqrt(c^2 - a^2);  % b^2 = c^2 - a^2
    % e = c / a;
    
    f1f2angle = anti_clockwise_angle_from_x_of_vector(vec_f1f2_unit);
    
    % % x,y
    x_t = m(1);        % ... translation in the x direction
    y_t = m(2);        % ... translation in the y direction
    theta = f1f2angle; % ... hyperbola's rotation angle arround the origin
    % rotated -> translated - parametric equations of the hyperbola
    % with 6 variables: a, b, phi, theta, x_t, y_t
    x_r = a .* cosh(phi) .* cos(theta) - b .* sinh(phi) .* sin(theta) + x_t;
    y_r = a .* cosh(phi) .* sin(theta) + b .* sinh(phi) .* cos(theta) + y_t;
    x_l = -a .* cosh(phi) .* cos(theta) - b .* sinh(phi) .* sin(theta) + x_t;
    y_l = -a .* cosh(phi) .* sin(theta) + b .* sinh(phi) .* cos(theta) + y_t;
    
    drbh.f1 = f1;
    drbh.f2 = f2;
    drbh.m  = m;
    drbh.c  = c;
    drbh.a  = a;
    drbh.v1 = v1;
    drbh.v2 = v2;
%     drbh.b  = b;
    drbh.x_r = x_r;
    drbh.y_r = y_r;
    drbh.x_l = x_l;
    drbh.y_l = y_l;
end
