classdef spwo_player_regions
    %SPWO_PLAYER_REGIONS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        pEI
        f1
        f2
        hStr
    end
    
    methods
        function obj = spwo_player_regions(pEI, f1, f2)
            obj.f1 = f1;
            obj.f2 = f2;
            obj.pEI = pEI;
            pEI_obstacle_rel_pos = getPointLineRelPos(pEI, f1, f2);

            d_EI_f2_norm = norm(f2 - pEI);
            d_EI_f1_norm = norm(f1 - pEI);
        %     phi = -pi/2 : 0.01 : pi/2;
            phi = 0 : 0.01 : pi;
            if pEI_obstacle_rel_pos > 0 % left
                c = d_EI_f2_norm - d_EI_f1_norm;
                obj.hStr = hyperbola(f2, f1, c/2, phi);
                disp("left");
            elseif pEI_obstacle_rel_pos < 0 % right
                c = d_EI_f1_norm - d_EI_f2_norm;
                obj.hStr = hyperbola(f1, f2, c/2, phi);
                disp("right");
            else % on line
                c = d_EI_f1_norm - d_EI_f2_norm;
                obj.hStr = hyperbola(f1, f2, c/2, phi);
                disp("line");
            end

            % DON'T DELETE: The bellow is not needed.
            % the reason is that c will be sometimes negative sometimes
            % positive, it depends on the entitie's distance from the two points
            % and this negativity or positivity will flip the hyperbola in such
            % a way, that it will be oriented correctly without needing
            % the below flip
            %     if d_EI_f2_norm < d_EI_f1_norm
            %         disp("d_EI_f2_norm < d_EI_f1_norm");
            %         hStr.x_r = [];
            %         hStr.y_r = [];
            %     else
            %         disp("! d_EI_f2_norm < d_EI_f1_norm");
            %         hStr.x_l = [];
            %         hStr.y_l = [];
            %     end
            obj.hStr.x_r = [];
            obj.hStr.y_r = [];
        end
        
        function player_regions_draw(obj, color, line_length)
            hyperbola_draw(obj.hStr, color);
            line_p1p2l_draw(obj.pEI, obj.f1, line_length, [color, '--']);
            line_p1p2l_draw(obj.pEI, obj.f2, line_length, [color, '--']);
        end
    end
end

