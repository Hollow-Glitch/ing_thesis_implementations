function ODB_closed_form()
    hold on;
    axis equal;

%     f1 = [-5; 0];
%     f2 = [0; -5];
%     pTI = [0; 0];
%     pAI = [10; 10];
%     vT = 1;
%     vA = 2 * vT;

    f2 = [10; 0];
    f1 = [0; 10];
    pTI = [10; 10];
    pAI = [20; 20];
    vT = 1.5;
    vA = 2;
    
%      va = 1;
%     vb = 2;
%     d = 2;
%     tB = 0;
    tB = (1/vA) .* norm(pAI - f1); % this should be it, right?
%     tB = (1/vT) .* norm(pTI - f1);
    % d =  norm(f2 - f1); % this, why???
    
    vb = vA;
    va = vT;
    % | 
    theta = 0 : 0.001 : 2*pi;
    
    % ============================================================
    % % region types
    % 1. both players can reach with straight line paths
    % 2. one can reach with straight line path but the other has to travel arround
    % 3. both travel arround, players travel to different endpoints
    % 4. both travel arround, players travel to the same endpoint
    
    % % DB forms
    % limacon   : tB != 0,  d != 0
    % appolonius: tB == 0,  d != 0
    % circle    : tB != 0,  d == 0
    
    % % DB form to region type linking
    % limacon in: 2,3(if tB!=0)
    % appolonius in: 1,3...
    % circle at edge: 4
    % ============================================================
    
    vec_pTI_pAI = pAI - pTI;
    vec_pTI_pAI_angle = anti_clockwise_angle_from_x_of_vector(vec_pTI_pAI);
    
    vec_pAI_pTI = pTI - pAI;
    vec_pAI_pTI_angle = anti_clockwise_angle_from_x_of_vector(vec_pAI_pTI);

    vec_pTI_f1 = f1 - pTI;
    vec_pTI_f1_angle = anti_clockwise_angle_from_x_of_vector(vec_pTI_f1);
    
    vec_pTI_f2 = f2 - pTI;
    vec_pTI_f2_angle = anti_clockwise_angle_from_x_of_vector(vec_pTI_f2);
    
    vec_pAI_f1 = f1 - pAI;
    vec_pAI_f1_angle = anti_clockwise_angle_from_x_of_vector(vec_pAI_f1);
    
    vec_pAI_f2 = f2 - pAI;
    vec_pAI_f2_angle = anti_clockwise_angle_from_x_of_vector(vec_pAI_f2);
    
    vec_f1_pAI = pAI - f1;
    vec_f1_pAI_angle = anti_clockwise_angle_from_x_of_vector(vec_f1_pAI);
    
    vec_f2_pAI = pAI - f2;
    vec_f2_pAI_angle = anti_clockwise_angle_from_x_of_vector(vec_f2_pAI);

    % ------------------------------------------------------------
    % region 2 - limacon
    % ------------------
    tT = (1/vT) * norm(f1 - pTI);
    tA = (1/vA) * norm(f1 - pAI);
%     tB = tA - tT;
    tB = tT;
%     d = norm(f1 - pAI) - tT * vA;
    d = norm(f1 - pAI);
%     theta = 0 : 0.001 : vec_pAI_f1_angle;
%     [locus1, locus2] = fancy_DB(vT, vA, d, tB, theta,   f1,f2,pTI,pAI);
%     plot_with_vectors(locus1, 'b');
%     plot_with_vectors(locus2, 'b');

    % for f1
    % we have to do minus-es there because we are rotating the limacon
    % which is in standard position and orientation, hence we have to rotate
    % our angles back taken from the scenarios geometry so that they correspond
    % with the limacons standard position and orientation
    start_angle = vec_pTI_f1_angle - vec_pAI_f1_angle;
    end_angle   = vec_pAI_f1_angle - vec_pAI_f1_angle;
    theta = start_angle : 0.001 : end_angle;
%     theta = 0 : 0.001 : 2*pi;
    d = norm(f1 - pAI);
    vT, vA, d, tB
    [locus1, locus2] = fancy_DB(vT, vA, d, tB, theta,   f1,f2,pTI,pAI);
    l1f1 = rotate_vector(locus1, vec_f1_pAI_angle) + f1;
    l2f1 = rotate_vector(locus2, vec_f1_pAI_angle) + f1;
%     plot_with_vectors(l1f1, 'b--'); % not this
    plot_with_vectors(l2f1, 'b-');
    
    % for f2
    start_angle = vec_pAI_f2_angle - vec_pAI_f2_angle;
    end_angle   = vec_pTI_f2_angle - vec_pAI_f2_angle;
    theta = start_angle : 0.001 : end_angle;
    d = norm(f2 - pAI);
    [locus1, locus2] = fancy_DB(vT, vA, d, tB, theta,   f1,f2,pTI,pAI);
    l1f2 = rotate_vector(locus1, vec_f2_pAI_angle) + f2;
    l2f2 = rotate_vector(locus2, vec_f2_pAI_angle) + f2;
    % plot_with_vectors(l1f2, 'b--'); % not this
    plot_with_vectors(l2f2, 'b-');

    % ------------------------------------------------------------
    % % region 4 - circle
    % -------------------
    % this is simpler but it has manual values
%     tT = (1/vT) * norm(f1 - pTI);
%     tA = (1/vA) * norm(f1 - pAI);
%     tB = tA - tT;
%     d = 0;
%     % f1
%     start_angle = vec_pAI_f1_angle;
%     end_angle   = vec_pAI_f1_angle + pi/4;
%     theta = start_angle : 0.001 : end_angle;
%     [l1, l2] = fancy_DB(vT, vA, d, tB, theta,   f1,f2,pTI,pAI);
%     c1 = l1 + f1;
%     plot_with_vectors(c1, 'g');
%     % f2
%     start_angle = vec_pAI_f2_angle-pi/4;
%     end_angle   = vec_pAI_f2_angle;
%     theta = start_angle : 0.001 : end_angle;
%     [l1, l2] = fancy_DB(vT, vA, d, tB, theta,   f1,f2,pTI,pAI);
%     c3 = l1 + f2;
%     plot_with_vectors(c3, 'g');

    % This works while getting the intersection point with polyxpoly for
    % the regions where the boundary is a circle centered at an obstacle
    % edge.
    tT = (1/vT) * norm(f1 - pTI);
    tA = (1/vA) * norm(f1 - pAI);
    tB = tA - tT;
    d = 0;

    % First get the set of points c1 and c3 corresponding to vertices f1
    % and f3.
    % f1
    start_angle = vec_pAI_f1_angle;
    end_angle   = vec_pAI_f1_angle +pi;%+ pi/4;
    theta = start_angle : 0.001 : end_angle;
    [l11, l12] = fancy_DB(vT, vA, d, tB, theta,   f1,f2,pTI,pAI);
    c1 = l11 + f1;
%     plot_with_vectors(c1, 'g');
    % f2
    start_angle = vec_pAI_f2_angle -pi;%-pi/4;
    end_angle   = vec_pAI_f2_angle;
    theta = start_angle : 0.001 : end_angle;
    [l21, l22] = fancy_DB(vT, vA, d, tB, theta,   f1,f2,pTI,pAI);
    c3 = l21 + f2;
%     plot_with_vectors(c3, 'g');

    % Using c1 and c3 calculate the intersection point pX and the angles.
    [pXx, pXy] = polyxpoly(c1(1,:), c1(2,:), c3(1,:), c3(2,:), 'unique');
    pX = [pXx'; pXy'];
    pX = pX(:, 1);
    vec_f1_pX_angle = anti_clockwise_angle_from_x_of_vector(pX - f1);
    vec_f2_pX_angle = anti_clockwise_angle_from_x_of_vector(pX - f2);
    
    % Using the calculated angles reconfigure and render the correct
    % output.
    start_angle = vec_pAI_f1_angle;
    end_angle   = vec_f1_pX_angle;
    theta = start_angle : 0.001 : end_angle;
    [l11, l12] = fancy_DB(vT, vA, d, tB, theta,   f1,f2,pTI,pAI);
    c1 = l11 + f1;
    plot_with_vectors(c1, 'g');
    % f2
    start_angle = vec_f2_pX_angle;
    end_angle   = vec_pAI_f2_angle;
    theta = start_angle : 0.001 : end_angle;
    [l21, l22] = fancy_DB(vT, vA, d, tB, theta,   f1,f2,pTI,pAI);
    c3 = l21 + f2;
    plot_with_vectors(c3, 'g');

    % ------------------------------------------------------------
    % % region 1 appolonius
    % ---------------------
    k = vA / vT;
%     appolonius_circle(pAI(1), pAI(2), pTI(1), pTI(2), k)
    radius = appolonius_circle_radius(pAI(1), pAI(2), pTI(1), pTI(2), k);
    [centerX, centerY] = appolonius_circle_center(pAI(1), pAI(2), pTI(1), pTI(2), k);
    cApp = [centerX; centerY];
%     plot_with_vectors(cApp, 'k*');
%     plot_with_vectors(l2f1(:,1), 'k*');
%     plot_with_vectors(l2f2(:,end), 'k*');
    angle_start = anti_clockwise_angle_from_x_of_vector(l2f2(:,end) - cApp);
    angle_end   = anti_clockwise_angle_from_x_of_vector(l2f1(:,1) - cApp);
    arc2_draw(centerX, centerY, radius, angle_start, angle_end, 0.01, 'decor', 'r-')
    
    % ------------------------------------------------------------
    % % remaining renderings
    plot(f1(1), f1(2), 'kx');  % f1
    plot(f2(1), f2(2), 'k*');  % f2
    plot([f1(1), f2(1)], [f1(2), f2(2)], 'k');  % obstacle
    plot(pTI(1), pTI(2), 'bs');  % TI
    plot(pAI(1), pAI(2), 'rd');  % AI
    draw_line_p1p2l(pAI, f1, 70, 'k--');
    draw_line_p1p2l(pTI, f1, 60, 'k--');
    draw_line_p1p2l(pAI, f2, 70, 'k--');
    draw_line_p1p2l(pTI, f2, 60, 'k--');
    draw_line_p1p2l(pAI, pTI, 70, 'k--');

    xlim([-27.6062   27.3938]);
    ylim([-28.3195   26.6804]);
    xlabel('x');
    ylabel('y');
    set_fig_style_min_squareish();
    print_current_fig("spwo_scen3_closed_form");
end

function [locus1, locus2] = fancy_DB(va, vb, d, tB, theta,   f1,f2,pTI,pAI)
    disp("fancy_DB");
    gamma = vb / va;
    a = gamma^2 - 1;
    b = 2 .* (d .* cos(theta) - gamma^2 .* va .* tB);
    c = gamma.^2 * va^2 * tB^2 - d^2;
    
    D = b.^2 - 4*a*c;
    % TODO: 
    % this check doesn't work, and using abs(sqrt(D)) causes only problems
    if D == 0
        disp("D == 0");
    elseif D < 0
        disp("D < 0");
    else % D > 0
        disp("D > 0");
%         r_1 = (-b + abs(sqrt(D))) / (2*a);
%         r_2 = (-b - abs(sqrt(D))) / (2*a);
        r_1 = (-b + sqrt(D)) / (2*a);
        r_2 = (-b - sqrt(D)) / (2*a);
    end
    x_1 = r_1 .* cos(theta);
    y_1 = r_1 .* sin(theta);
    x_2 = r_2 .* cos(theta);
    y_2 = r_2 .* sin(theta);
    
    locus1 = [x_1; y_1];
    locus2 = [x_2; y_2];
end


% ------------------------------------------------------------
% utilities
% ---------

function plot_with_vectors(vec, decor)
    plot(vec(1,:), vec(2,:), decor);
end

function radius = appolonius_circle_radius(xAI, yAI, xTI, yTI, k)
    radius = (k.*sqrt( (xAI-xTI)^2 + (yAI-yTI)^2 )) / (1-k^2);
    radius = abs(radius);
end

function [centerX, centerY] = appolonius_circle_center(xAI, yAI, xTI, yTI, k)
    centerX = (k^2 .* xTI - xAI) / (k^2-1);
    centerY = (k^2 .* yTI - yAI) / (k^2-1);
end

% function appolonius_circle(xAI, yAI, xTI, yTI, k, configShow)
function appolonius_circle(xAI, yAI, xTI, yTI, k)
    radius = appolonius_circle_radius(xAI, yAI, xTI, yTI, k);
    [centerX, centerY] = appolonius_circle_center(xAI, yAI, xTI, yTI, k);
    
    pTI = [xTI, yTI];    
    pAI = [xAI, yAI];

    hold on;
    axis equal;
    plot(pTI(1), pTI(2), 'o');

    plot(pAI(1), pAI(2), 'o'); 
    plot(centerX, centerY, 'x');
    col_circle(centerX, centerY, radius, 'r');
end

function circle(centerX, centerY, radius)
    stepped_col_circle(centerX, centerY, radius, 'k', 0.025);
end

function col_circle(centerX, centerY, radius, edge_color)
    stepped_col_circle(centerX, centerY, radius, edge_color, 0.01);
end

function stepped_col_circle(centerX, centerY, radius, edge_color, theta_step)
    theta = 0 : theta_step : 2*pi;
    out = param_circle(centerX, centerY, radius, theta);
    x = out(1,:);
    y = out(2,:);
    x = [x, x(1)];
    y = [y, y(1)];
%     plot(x,y, 'o'); % for testing
    plot(x,y, edge_color);
end

function out = param_circle(xc, yc, r, theta)
    x = xc + r .* cos(theta);
    y = yc + r .* sin(theta);
    
    out = [
        x;
        y
        ];
end

% x_in ... horizontal array
% y_in ... horiznotal array
% angle ... scalar angle in radians
%
% you can test this with:
%     poly_x = [1, 0, -1];
%     poly_y = [0, 1,  0];
%     plot(poly_x, poly_y, 'b');
%     [poly_xr, poly_yr] = rotate_points(poly_x, poly_y, pi/4);
%     plot(poly_xr, poly_yr, 'r');

function vec_out = rotate_vector(vec, angle)
    [x_out, y_out] = rotate_points(vec(1,:), vec(2,:), angle);
    vec_out = [
        x_out;
        y_out
    ];
end

function [x_out, y_out] = rotate_points(x_in, y_in, angle)
    % x_in ... array of x coordinates
    % y_in ... array of y coordinates
    % x_out ... array of rotated x coordinates
    % y_out ... array of rotated y coordinates
    R = [
        cos(angle), -sin(angle);
        sin(angle),  cos(angle)
    ];
    vec = [
        x_in;
        y_in
    ];
    rotated_vec = R * vec;
    x_out = rotated_vec(1,:);
    y_out = rotated_vec(2,:);
end

function draw_line_p1p2l(p1, p2, length, decor)
    [x,y] = line_p1p2l(p1,p2,length);
    plot([p1(1), x],[p1(2), y],decor);
end

function [x,y] = line_p1p2l(p1, p2, length)
    angle = anti_clockwise_angle_from_x_of_vector(p2-p1);
    [x, y] = line_p(p1, angle, length);
end

% draw a line that starts from `p`, goes in the direction given with
% `angle` and has length `length`, and line decoration is given with
% `decor`
function draw_line_p(p, angle, length, decor)
    [x,y] = line_p(p, angle, length);
    plot([p(1), x],[p(2), y],decor);
end

function [x, y] = line_p(p, angle, length)
    x = length .* cos(angle) + p(1);
    y = length .* sin(angle) + p(2);
end

function angle = anti_clockwise_angle_from_x_of_vector(vec)
    function angle = vec_angle_from_x(vec)
        x_unit = [1 0];
        angle = acos(...
                dot(vec, x_unit) / (norm(vec) * norm(x_unit) )...
            );
    end

    if vec(2) < 0
        angle = 2*pi - vec_angle_from_x(vec);
    else
        angle = vec_angle_from_x(vec);
    end
end