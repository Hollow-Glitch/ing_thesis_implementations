function single_agent_isochrone()
    %% options
    create_video  = false;
    create_figure = false;
    % In the case of this program, both of the above can be active at the
    % same time.

    %%
    single_agent_isochrone_anim(create_video);
    single_agent_isochrone_figures(create_figure);
end


function single_agent_isochrone_anim(create_video)
    f1 = [0;10];
    f2 = [10;0];
    pTI = [-1.5;6];
    vTnorm = 1;

    target_regions = spwo_player_regions(pTI, f1, f2);

    %% Video: create video object
    if create_video
        video_name = 'single_agent_isochrone';
        vidObj = VideoWriter([video_name, '.avi']);
        open(vidObj);
    end

    %% game loop
    for t = 0:0.1:20
        [xTc, yTc] = spwo_func_get_iso(f1,f2,vTnorm,pTI,t);

        clf;
        hold on;
        axis equal;
%         xlim([-7.6458   20.6733]);
%         ylim([-5.8152   16.5205]);
        xlim([-32.1124   34.6626]);
        ylim([-19.0799   33.5865]);
        xlabel('x');
        ylabel('y');
        title(sprintf('$t=%.2fsec$', t), Interpreter="latex");

        line_p1p2_draw(f1, f2, 'k-');
        plot_with_vectors(pTI, 'bs');
        target_regions.player_regions_draw('b', 40);
        plot(xTc, yTc, '-b');

%         pause(1/60);
        drawnow;

        if create_video
            writeVideo(vidObj, getframe(gcf));
        end
    end

    if create_video
        close(vidObj);
    end
end


function single_agent_isochrone_figures(create_figure)
    f1 = [0;10];
    f2 = [10;0];
    pTI = [-1.5;6];
    vTnorm = 1;
    
    clf;
    hold on;
    axis equal;
    xlim([-7.6458   20.6733]);
    ylim([-5.8152   16.5205]);
    line_p1p2_draw(f1, f2, 'k-');
    xlabel('x');
    ylabel('y');

    plot_with_vectors(pTI, 'bs');
    target_regions = spwo_player_regions(pTI, f1, f2);
    target_regions.player_regions_draw('b', 40);
    
    for t = 0:2:100
        [xTc, yTc] = spwo_func_get_iso(f1,f2,vTnorm,pTI,t);
        plot(xTc, yTc, ':b');
    end

    textArroundPoint( [-1.5;    -2], [0 ;-1], '$1$');
    textArroundPoint( [17  ;  0.34], [0.8;0], '$2$');
    textArroundPoint( [13.7; 10.63], [0.8;0], '$3$');

    if create_figure
        set_fig_style_bc_squareish();
        print_current_fig("obstacle_isochrone_t=0_2_100");
    end
end