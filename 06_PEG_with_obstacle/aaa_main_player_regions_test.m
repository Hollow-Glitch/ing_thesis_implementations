function aaa_main_player_regions_test()
    f1 = [-5; 0];
    f2 = [0; -5];

    hold on;
    axis equal;
    plot_with_vectors([f1, f2], 'ko-');

    line_length = 40;
    function set_limits(xmin, xmax, ymin, ymax)
        xlim([xmin, xmax]);
        ylim([ymin, ymax]);
    end
    my_limits = @() set_limits(-40, 10, -40, 10);
    my_limits();

%     f1 = ginput(1); f1 = f1'; f1
%     f2 = ginput(1); f2 = f2'; f2
%     pTI = [ 5; 0];
%     pAI = [-10; -11];
    pTI = ginput(1); pTI = pTI'; % pTI
    pAI = ginput(1); pAI = pAI'; % pAI
    vT = 1;
    vA = 2 * vT;

%     hold on;
%     axis equal;
%     plot_with_vectors([f1, f2], 'ko-');
% 
%     line_length = 40;
%     function set_limits(xmin, xmax, ymin, ymax)
%         xlim([xmin, xmax]);
%         ylim([ymin, ymax]);
%     end
%     my_limits = @() set_limits(-40, 10, -40, 10);
    set(gcf, 'Position',  [100, 100, 1200, 400]);

    subplot(1, 3, 1);
    hold on;
    axis square;
    my_limits();
    textArroundPoint(f1, [ 0; 3], '$f_1$');
    textArroundPoint(f2, [ 3; 0], '$f_2$');
    plot_with_vectors(pAI, 'rx');
    plot_with_vectors(pTI, 'bx');
    plot_with_vectors([f1, f2], 'ko-');
    player_regions_draw(pAI, f1, f2, 'r', line_length);

    subplot(1, 3, 2);
    hold on;
    axis square;
    my_limits();
    textArroundPoint(f1, [ 0; 3], '$f_1$');
    textArroundPoint(f2, [ 3; 0], '$f_2$');
    plot_with_vectors(pAI, 'rx');
    plot_with_vectors(pTI, 'bx');
    plot_with_vectors([f1, f2], 'ko-');
    player_regions_draw(pTI, f1, f2, 'b', line_length);
    
    subplot(1, 3, 3);
    hold on;
    axis square;
    my_limits();
    textArroundPoint(f1, [ 0; 3], '$f_1$');
    textArroundPoint(f2, [ 3; 0], '$f_2$');
    plot_with_vectors(pAI, 'rx');
    plot_with_vectors(pTI, 'bx');
    plot_with_vectors([f1, f2], 'ko-');
    players_regions_draw(pAI, pTI, f1, f2, line_length);
end

function players_regions_draw(pAI, pTI, f1, f2, line_length)
    player_regions_draw(pAI, f1, f2, 'r', line_length);
    player_regions_draw(pTI, f1, f2, 'b', line_length);
end

function player_regions_draw(pEI, f1, f2, color, line_length)
    line_p1p2l_draw(pEI, f1, line_length, [color, '--']);
    line_p1p2l_draw(pEI, f2, line_length, [color, '--']);

    % we need to know on which side of the obstacle the player is
    % hence in order to use the cross product to determine this
    % we first need to determine the sequence of our points by which
    % we determine the orientation of the area of the triangle
    % they are describing, let this seq. be: f1 -> f2 -> pEI
    % u = f2 - f1
    % w = pEI - f2
    % | i    j    k|
    % | u_x  w_x  0|
    % | u_y  w_y  0|
    % k(u_x*w_y - w_x*u_y) = signedDoubleArea
    % we drop the unit vector k since we only need a scalar
    % if signedArea is <0 then we are on one side, if >0 on the other
    % and if =0 then pEI is just a multiple of the vector (f2-f1)
    % i.e. lies on the same line.
    % The reason for the name "signedDoubleArea" is that actually
    % determinant determines the area of the paralelogram with sides
    % u, w, and hence in order to get the area of the triangle, we would
    % need to divide it by two, i.e. signedArea = signedDoubleArea / 2;
    % area >0 ... we are on the left  side
    % area =0 ... we are on the same  line
    % area <0 ... we are on the right side
    u = f2 - f1;
    w = pEI - f2;
    signedDoubleArea = u(1)*w(2) - w(1)*u(2);

    d_EI_f2_norm = norm(f2 - pEI);
    d_EI_f1_norm = norm(f1 - pEI);
%     phi = -pi/2 : 0.01 : pi/2;
    phi = 0 : 0.01 : pi;
    if signedDoubleArea > 0 % left
        c = d_EI_f2_norm - d_EI_f1_norm;
        hStr = hyperbola(f2, f1, c/2, phi);
        disp("left");
    elseif signedDoubleArea < 0 % right
        c = d_EI_f1_norm - d_EI_f2_norm;
        hStr = hyperbola(f1, f2, c/2, phi);
        disp("right");
    else % on line
        c = d_EI_f1_norm - d_EI_f2_norm;
        hStr = hyperbola(f1, f2, c/2, phi);
        disp("line");
    end

    % DON'T DELETE: The bellow is not needed.
    % the reason is that c will be sometimes negative sometimes
    % positive, it depends on the entitie's distance from the two points
    % and this negativity or positivity will flip the hyperbola in such
    % a way, that it will be oriented correctly without needing
    % the below flip
%     if d_EI_f2_norm < d_EI_f1_norm
%         disp("d_EI_f2_norm < d_EI_f1_norm");
%         hStr.x_r = [];
%         hStr.y_r = [];
%     else
%         disp("! d_EI_f2_norm < d_EI_f1_norm");
%         hStr.x_l = [];
%         hStr.y_l = [];
%     end
    hStr.x_r = [];
    hStr.y_r = [];
    hyperbola_draw(hStr, color);
    disp("");
end

