% hStr ... hyperbola struct
function hyperbola_draw(hStr, color)
    hold on;
%     xlim([-2, 2]);
%     ylim([-2, 2]);
%     xline(0);
%     yline(0);
    
    if false
        plot(hStr.f1(1), hStr.f1(2), 'kx');
        plot(hStr.f2(1), hStr.f2(2), 'rx');
        plot(hStr.m(1),  hStr.m(2),  'r*');
        plot(hStr.v1(1), hStr.v1(2), 'ro');
        plot(hStr.v2(1), hStr.v2(2), 'ro');
    end
    plot(hStr.x_r,   hStr.y_r,   [color, '--']);
    plot(hStr.x_l,   hStr.y_l,   [color, '--']);
end
