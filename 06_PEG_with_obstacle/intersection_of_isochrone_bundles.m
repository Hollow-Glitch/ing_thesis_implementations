function intersection_of_isochrone_bundles()
    % spwo_player_isochrone2_test()

    clf;
    spwo_scen3_just_some();

    clf;
    spwo_scen1();
    clf;
    spwo_scen2();
    clf;
    spwo_scen3();
end


% ============================================================
% SCENARIOS
% =========

function spwo_scen3_just_some()
    scenarioName = "spwo_scen3";
    if ~isfile(strcat(scenarioName, '.mat'))
        error("Animation_and_generation must be run first.");
    end
    load(strcat(scenarioName, '.mat'));

    hold on;
    axis equal;
%     xlim([-27.6062   27.3938]);
%     ylim([-28.3195   26.6804]);
    xlim([-25, 60]);
    ylim([-25, 60]);
    xlabel('x');
    ylabel('x');

    plot_with_vectors(pAI, 'dr');
    attacker_regions = spwo_player_regions(pAI, f1, f2);
    attacker_regions.player_regions_draw('r', 80);

    plot_with_vectors(pTI, 'bo');
    target_regions = spwo_player_regions(pTI, f1, f2);
    target_regions.player_regions_draw('b', 80);
    
    plot([f1(1), f2(1)], [f1(2), f2(2)], 'k-', 'LineWidth',1);   

    xi = [];
    yi = [];
    t_seq = [15, 20];
%     for t = 0:10:30
    for t = t_seq
        [xTc, yTc] = spwo_func_get_iso(f1,f2,vTnorm,pTI,t);
        plot(xTc, yTc, ':b');

        [xAc, yAc] = spwo_func_get_iso(f1,f2,vAnorm,pAI,t);
        plot(xAc, yAc, ':r');
        
        [xi_current, yi_current] = polyxpoly(xTc, yTc, xAc, yAc);
        xi = [xi, xi_current'];
        yi = [yi, yi_current'];
    end
    
    points = getClosedPolygonFromPointSet(xi,yi);
    plot(points(1,:), points(2,:), '*k');

%     set_fig_style_min_squareish();
    set_fig_style_bc_squareish();
    print_current_fig("spwo_scen3_just_some");
end


function spwo_scen1()
    st = dbstack;
    namestr = st.name;
    scenarioName = namestr;

    if false
        animation_and_generation(scenarioName, false, 20, 0.1, ...
            'f1',     [ 0;10],        ...
            'f2',     [10; 0],        ...
            'pTI',    [ 0; 0],        ...
            'vTnorm',       1,        ...
            'pAI',    [ 9;10],        ...
            'vAnorm',       2         ...
        );
    end

    % ----------------------------------------
    % figure
    if ~isfile(strcat(scenarioName, '.mat'))
        error("Animation_and_generation must be run first.");
    end
    load(strcat(scenarioName, '.mat'));
    
    t = 10;
    
    hold on;
    axis equal;
    xlim([-14.7637   22.8020]);
    ylim([-15.4592   22.1065]);
    xlabel('x');
    ylabel('x');
    
%     textArroundPoint(f1, [0;-2], '$f_1$');
%     textArroundPoint(f2, [0;-2], '$f_2$');
    
    plot_with_vectors(pAI, 'dr');
    attacker_regions = spwo_player_regions(pAI, f1, f2);
    attacker_regions.player_regions_draw('r', 40);

    plot_with_vectors(pTI, 'bo');
    target_regions = spwo_player_regions(pTI, f1, f2);
    target_regions.player_regions_draw('b', 40);
    
    plot([f1(1), f2(1)], [f1(2), f2(2)], 'k-', 'LineWidth',1);
    
    plot(points(1,:), points(2,:), 'Color', '#00b100');
    
    xi = [];
    yi = [];
    for t = 0:1:30
        [xTc, yTc] = spwo_func_get_iso(f1,f2,vTnorm,pTI,t);
        plot(xTc, yTc, ':b');

        [xAc, yAc] = spwo_func_get_iso(f1,f2,vAnorm,pAI,t);
        plot(xAc, yAc, ':r');
        
        [xi_current, yi_current] = polyxpoly(xTc, yTc, xAc, yAc);
        xi = [xi, xi_current'];
        yi = [yi, yi_current'];
    end
    
    points = getClosedPolygonFromPointSet(xi,yi);
    plot(points(1,:), points(2,:), '*k');
    
    set_fig_style_min_squareish();
    print_current_fig(scenarioName);
end


function spwo_scen2()
    st = dbstack;
    namestr = st.name;
    scenarioName = namestr;

    if false
        animation_and_generation(scenarioName, false, 20,0.1, ...
            'f1',     [ 0;10],        ...
            'f2',     [10; 0],        ...
            'pTI',    [ 0; 0],        ...
            'vTnorm',       1,        ...
            'pAI',    [ 3;8],        ...
            'vAnorm',       2         ...
        );
    end

    % ----------------------------------------
    % figure
    if ~isfile(strcat(scenarioName, '.mat'))
        error("Animation_and_generation must be run first.");
    end
    load(strcat(scenarioName, '.mat'));
    
    t = 10;
    
    hold on;
    axis equal;
    xlim([-14.7637   22.8020]);
    ylim([-15.4592   22.1065]);
    xlabel('x');
    ylabel('x');
    
%     textArroundPoint(f1, [0;-2], '$f_1$');
%     textArroundPoint(f2, [0;-2], '$f_2$');
    
    plot_with_vectors(pAI, 'dr');
    attacker_regions = spwo_player_regions(pAI, f1, f2);
    attacker_regions.player_regions_draw('r', 40);

    plot_with_vectors(pTI, 'bo');
    target_regions = spwo_player_regions(pTI, f1, f2);
    target_regions.player_regions_draw('b', 40);
    
    plot([f1(1), f2(1)], [f1(2), f2(2)], 'k-', 'LineWidth',1);
    
    plot(points(1,:), points(2,:), 'Color', '#00b100');
    
    xi = [];
    yi = [];
    for t = 0:1:30;
        [xTc, yTc] = spwo_func_get_iso(f1,f2,vTnorm,pTI,t);
        plot(xTc, yTc, ':b');

        [xAc, yAc] = spwo_func_get_iso(f1,f2,vAnorm,pAI,t);
        plot(xAc, yAc, ':r');
        
        [xi_current, yi_current] = polyxpoly(xTc, yTc, xAc, yAc);
        xi = [xi, xi_current'];
        yi = [yi, yi_current'];
    end
    
    points = getClosedPolygonFromPointSet(xi,yi);
    plot(points(1,:), points(2,:), '*k');
    
    set_fig_style_min_squareish();
    print_current_fig(scenarioName);
end


function spwo_scen3()
    st = dbstack;
    namestr = st.name;
    scenarioName = namestr;

    if false
        animation_and_generation(scenarioName, false, 40, 0.001, ...
            'f1',     [ 0; 10],        ...
            'f2',     [10;  0],        ...
            'pTI',    [10; 10],        ...
            'vTnorm',      1.5,        ...
            'pAI',    [ 20;20],        ...
            'vAnorm',       2         ...
        );
    end

    % ----------------------------------------
    % figure
    if ~isfile(strcat(scenarioName, '.mat'))
        error("Animation_and_generation must be run first.");
    end
    load(strcat(scenarioName, '.mat'));
    
    t = 10;
    
    hold on;
    axis equal;
    xlim([-27.6062   27.3938]);
    ylim([-28.3195   26.6804]);
    xlabel('x');
    ylabel('x');
    
%     textArroundPoint(f1, [0;-2], '$f_1$');
%     textArroundPoint(f2, [0;-2], '$f_2$');
    
    plot_with_vectors(pAI, 'dr');
    attacker_regions = spwo_player_regions(pAI, f1, f2);
    attacker_regions.player_regions_draw('r', 80);

    plot_with_vectors(pTI, 'bo');
    target_regions = spwo_player_regions(pTI, f1, f2);
    target_regions.player_regions_draw('b', 80);
    
    plot([f1(1), f2(1)], [f1(2), f2(2)], 'k-', 'LineWidth',1);
    
    plot(points(1,:), points(2,:), 'Color', '#00b100');
    
    xi = [];
    yi = [];
    for t = 0:1:30;
        [xTc, yTc] = spwo_func_get_iso(f1,f2,vTnorm,pTI,t);
        plot(xTc, yTc, ':b');

        [xAc, yAc] = spwo_func_get_iso(f1,f2,vAnorm,pAI,t);
        plot(xAc, yAc, ':r');
        
        [xi_current, yi_current] = polyxpoly(xTc, yTc, xAc, yAc);
        xi = [xi, xi_current'];
        yi = [yi, yi_current'];
    end
    
    points = getClosedPolygonFromPointSet(xi,yi);
    plot(points(1,:), points(2,:), '*k');
    
    set_fig_style_min_squareish();
    print_current_fig(scenarioName);
end

% ============================================================
% CORE
% ====


function animation_and_generation(scenarioName, RECORD, endTime, timeStep, args)
    arguments
        scenarioName
        RECORD
        endTime
        timeStep
        args.f1      (2,1)
        args.f2      (2,1)
        args.pTI     (2,1)
        args.vTnorm  (1,1)
        args.pAI     (2,1)
        args.vAnorm  (1,1)
    end
    videoName = strcat(scenarioName, '.avi');

    f1      = args.f1;
    f2      = args.f2;
    pTI     = args.pTI;
    vTnorm  = args.vTnorm;
    pAI     = args.pAI;
    vAnorm  = args.vAnorm;

%     isoT = spwo_player_isochrone2(f1,f2,vTnorm,pTI);
%     t = 3;
%     isoT.calculate_curve(obj, t);
    hold on;
    axis equal;
    xlim([-20,20]);
    ylim([-20,20]);
    
    textArroundPoint(f1, [0;-2], '$f_1$');
    textArroundPoint(f2, [0;-2], '$f_2$');
    
%     plot_with_vectors(pAI, 'dr');
%     attacker_regions = spwo_player_regions(pAI, f1, f2);
%     attacker_regions.player_regions_draw('r', 20);

    plot_with_vectors(pTI, 'bo');
    target_regions = spwo_player_regions(pTI, f1, f2);
    target_regions.player_regions_draw('b', 20);
    
    plot_with_vectors([f1,f2],'k-');
    
    %% isochrone
    matFileName = strcat(scenarioName, '.mat');
    if isfile(matFileName)
        CURRENT_RECORD = RECORD;
        load(matFileName);
        RECORD = CURRENT_RECORD;
        disp('IF SOMETHINGS DOES NOT WORK CORRECTLY THEN KEEP IN MIND THAT IT MAY BE CAUSE BY THE LOAD FUNCTION OVERWRITING OTHER STUFF.');
    else
        xi = [];
        yi = [];
    end
%     t = 19;
    if RECORD == true
        disp(['Recording of:',videoName,' has been started.']);
        vidObj = VideoWriter(videoName);
        open(vidObj);
    else
        disp(['No recording.']);
    end
    
    for t = 0:timeStep:endTime
        clf;
        hold on;
        axis equal;
%         set(gca, 'color', '#E0C9A6');
        title(t);
        
        plot_with_vectors([f1,f2],'k-');

        [xTc, yTc] = spwo_func_get_iso(f1,f2,vTnorm,pTI,t);
        plot(xTc, yTc, '--b');
        
        [xAc, yAc] = spwo_func_get_iso(f1,f2,vAnorm,pAI,t);
        plot(xAc, yAc, '--r');
        
        if isfile(matFileName)
            plot(points(1,:), points(2,:), '--', 'Color', '#00b100', 'LineWidth',2);
        else
            [xi_current, yi_current] = polyxpoly(xTc, yTc, xAc, yAc);
            xi = [xi, xi_current'];
            yi = [yi, yi_current'];
            plot(xi, yi, '.g');
        end

%         disp(t);
        if RECORD
            writeVideo(vidObj, getframe(gcf));
        end
        disp(t);
%         pause(0.1);
    end
    
    points = getClosedPolygonFromPointSet(xi,yi);
    plot(points(1,:), points(2,:), 'Color', '#00b100');
    
    if ~ isfile(matFileName)
        save(matFileName);
    end
    if RECORD
        close(vidObj);
        disp(['Wrote video into: ',videoName,'.']);
    end
end
